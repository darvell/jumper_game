package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;

public class ItemInfo extends Group {

    private TextureRegion corner;
    private TextureRegion border;
    private TextureRegion borderLand;
    private TextureRegion body;

    private final ItemInfo itemInfo;

    private Label label;

    private UIActor closeBtn;


    public ItemInfo(Assets assets) {
        itemInfo = this;
        setWidth(133f);
        setHeight(100f);

        corner = assets.getUITexture(Assets.UI_INFO_PANEL_CORNER);
        border = assets.getUITexture(Assets.UI_INFO_PANEL_BORDER);
        borderLand = assets.getUITexture(Assets.UI_INFO_PANEL_BORDER_LAND);
        body = assets.getUITexture(Assets.UI_INFO_PANEL_BODY);

        closeBtn = new UIActor();
        closeBtn.setTextureRegion(assets.getUITexture(Assets.UI_INFO_PANEL_CLOSE_BTN));
        closeBtn.setX(getX() + getWidth() - 12f);
        closeBtn.setY(getY() + 5f);
        addActor(closeBtn);
        closeBtn.toFront();

        closeBtn.addListener(new ClickListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                itemInfo.setVisible(false);
            }
        });

        Skin skin = new Skin(Gdx.files.internal("data/style.json"));
        label = new Label("12", skin);

        addActor(label);
//        label.setDebug(true);


        setText("Трава с края земли\nили нет\nили да");

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        batch.draw(corner, getX(), (getY() + getHeight()) - corner.getRegionHeight());
        batch.draw(border, getX() + corner.getRegionWidth(), (getY() + getHeight()) - border.getRegionHeight(),
                getWidth() - corner.getRegionWidth() * 2, border.getRegionHeight());
        corner.flip(true, false);

        batch.draw(corner, getX() + getWidth() - corner.getRegionWidth(), (getY() + getHeight()) - corner.getRegionHeight());
        corner.flip(true, false);
        batch.draw(borderLand, getX() + getWidth() - borderLand.getRegionWidth(), getY()+corner.getRegionHeight(),
                borderLand.getRegionWidth(),getHeight() - corner.getRegionHeight() * 2);
        corner.flip(true, true);

        batch.draw(corner, getX() + getWidth() - corner.getRegionWidth(), getY());
        border.flip(false, true);
        batch.draw(border, getX() + corner.getRegionWidth(), getY(),
                getWidth() - corner.getRegionWidth() * 2, border.getRegionHeight());
        border.flip(false, true);

        corner.flip(true, false);
        batch.draw(corner, getX(), getY());
        corner.flip(false, true);

        borderLand.flip(true, false);
        batch.draw(borderLand, getX(), getY() + corner.getRegionHeight(),
                borderLand.getRegionWidth(),getHeight() - corner.getRegionHeight() * 2);
        borderLand.flip(true, false);

        batch.draw(body, getX()+corner.getRegionHeight(), getY()+corner.getRegionHeight(),
                getWidth() - corner.getRegionWidth() * 2, getHeight() - corner.getRegionHeight() *2);

        super.draw(batch, parentAlpha);
    }


    public void setText(String text){
        label.setFontScale(1f, 1f);
        label.setText(text);
        label.setFontScale(0.3f, 0.3f);
        label.setHeight(label.getPrefHeight());
        label.setX(getX() + 3f);
        label.setY(getY() + getHeight() - label.getPrefHeight() - 6f);
    }
}
