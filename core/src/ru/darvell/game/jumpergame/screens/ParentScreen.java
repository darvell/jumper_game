package ru.darvell.game.jumpergame.screens;

import ru.darvell.game.jumpergame.stages.GamePlayStage;
import ru.darvell.game.jumpergame.stages.UIStage;
import ru.darvell.game.jumpergame.utils.Levels;
import ru.darvell.game.jumpergame.utils.Saver;

public interface ParentScreen {

    UIStage getUIStage();
    GamePlayStage getGamePlayStage();
    void reset();
    Levels gerCurrentLevel();
    void changeLevel(Levels levels);

    void changeInputToUIOnly();
    void resetInput();

    Saver getSaver();
}
