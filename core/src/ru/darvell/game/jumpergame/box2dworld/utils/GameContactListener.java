package ru.darvell.game.jumpergame.box2dworld.utils;


import com.badlogic.gdx.physics.box2d.*;
import ru.darvell.game.jumpergame.box2dworld.*;
import ru.darvell.game.jumpergame.box2dworld.fixtures.FixtureUtils;
import ru.darvell.game.jumpergame.utils.Constants;


public class GameContactListener implements ContactListener {

    public static final String LOG_TAG = "GameContactListener";

    @Override
    public void beginContact(Contact contact) {
        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        //Player и Block
        if ((FixtureUtils.fixtureIsPlayerGroundSensor(contact.getFixtureA()) && BodyUtils.bodyIsBlock(b))
                || ((FixtureUtils.fixtureIsPlayerGroundSensor(contact.getFixtureB()) && BodyUtils.bodyIsBlock(a)))) {

            BlockUserData blockUserData = (BlockUserData) (BodyUtils.bodyIsBlock(a) ? a.getUserData() : b.getUserData());

            if (!blockUserData.isHided()) {
                Fixture fixture = FixtureUtils.fixtureIsPlayerGroundSensor(contact.getFixtureA()) ? contact.getFixtureA() : contact.getFixtureB();
                Body body = fixture.getBody();
                PlayerUserData playerUserData = (PlayerUserData) body.getUserData();
//                contact.setFriction(playerUserData.getFriction());

                Body blockBody = BodyUtils.bodyIsBlock(a) ? a : b;
//                playerUserData.setBlockId();
                playerUserData.setLanded(((BlockUserData) blockBody.getUserData()).getBlockId());

                if (body.getLinearVelocity().y < Constants.PLAYER_MAX_LANDING_VELOCITY) {
                    playerUserData.setFellDown(true);
                }
            }
        } else if ((FixtureUtils.fixtureIsPlayerLandingSensor(contact.getFixtureA()) && BodyUtils.bodyIsBlock(b))
                || ((FixtureUtils.fixtureIsPlayerLandingSensor(contact.getFixtureB()) && BodyUtils.bodyIsBlock(a)))) {
            Fixture fixture = FixtureUtils.fixtureIsPlayerLandingSensor(contact.getFixtureA()) ? contact.getFixtureA() : contact.getFixtureB();
            Body body = fixture.getBody();
            PlayerUserData playerUserData = (PlayerUserData) body.getUserData();
            playerUserData.setNeedLandingAnimation(true);

        } else if ((BodyUtils.bodyIsSideSensor(a) && BodyUtils.bodyIsBlock(b))
                || (BodyUtils.bodyIsSideSensor(b) && BodyUtils.bodyIsBlock(a))) {
            Body blockBody = BodyUtils.bodyIsBlock(b) ? b : a;
            BlockUserData blockUserData = (BlockUserData) blockBody.getUserData();
            if (!blockUserData.isCollideWithBlock()) {
                blockUserData.setCollideWithBlock(true);
            }

        } else if (BodyUtils.bodyIsBlock(a) && BodyUtils.bodyIsBlock(b)) {
            if (BodyUtils.bodyIsMoveBlock(a) || BodyUtils.bodyIsMoveBlock(b)) {
                BlockUserData userData = (BlockUserData) (BodyUtils.bodyIsMoveBlock(a) ? a.getUserData() : b.getUserData());
                userData.setCollideWithBlock(true);
            }
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsBlock(b))
                || (BodyUtils.bodyIsPlayer(b) && BodyUtils.bodyIsBlock(a))) {
            Body playerBody = BodyUtils.bodyIsPlayer(a) ? a : b;
            PlayerUserData playerUserData = (PlayerUserData) playerBody.getUserData();
            contact.setFriction(playerUserData.getFriction());
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsItemOnField(b)) ||
                (BodyUtils.bodyIsPlayer(b)) && BodyUtils.bodyIsItemOnField(a)) {

            ItemOnFieldUserData itemOnFieldUserData = (ItemOnFieldUserData) (BodyUtils.bodyIsItemOnField(a) ? a.getUserData() : b.getUserData());
            if (!itemOnFieldUserData.isPickedUp()) {
                itemOnFieldUserData.setPickedUp(true);
//                itemOnFieldUserData.setNeedDelete(true);
            }
        }

        //Goblin LANDING ON BLOCK

        else if ((FixtureUtils.fixtureIsGoblinGroundLandingSensor(contact.getFixtureA()) && BodyUtils.bodyIsBlock(b))
                || ((FixtureUtils.fixtureIsGoblinGroundLandingSensor(contact.getFixtureB()) && BodyUtils.bodyIsBlock(a)))) {
            Fixture fixture = FixtureUtils.fixtureIsGoblinGroundLandingSensor(contact.getFixtureA()) ? contact.getFixtureA() : contact.getFixtureB();
            Body body = fixture.getBody();
            GoblinUserData goblinUserData = (GoblinUserData) body.getUserData();
            goblinUserData.setLanded(true);
            Body blockBody = BodyUtils.bodyIsBlock(a) ? a : b;
            BlockUserData blockUserData = (BlockUserData) blockBody.getUserData();
            blockUserData.setShaked(true);
////            System.out.println(body.getLinearVelocity().y);
//            if (body.getLinearVelocity().y < Constants.PLAYER_MAX_LANDING_VELOCITY){
//                playerUserData.setFellDown(true);
//            }
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsGoblin(b)) || (BodyUtils.bodyIsPlayer(b) && BodyUtils.bodyIsGoblin(a))) {
//            Object ud = a.getUserData() instanceof PlayerUserData ? a.getUserData() : b.getUserData();
//            ((PlayerUserData) ud).setBadTouched(true);

        } else if ((BodyUtils.bodyIsPlayer(a) && (BodyUtils.bodyIsFallSensor(b) || BodyUtils.bodyIsStormSensor(b))) ||
                (BodyUtils.bodyIsPlayer(b) && (BodyUtils.bodyIsFallSensor(a) || BodyUtils.bodyIsStormSensor(a)))) {
            Body playerBody = BodyUtils.bodyIsPlayer(a) ? a : b;
            PlayerUserData playerUserData = (PlayerUserData) playerBody.getUserData();
            if (!playerUserData.isDeepFall()) {
                playerUserData.setDeepFall(true);
            }
        } else if (BodyUtils.bodyIsSnowBall(a) || BodyUtils.bodyIsSnowBall(b)){
            SnowBallUserData snowBallUserData = (SnowBallUserData) (BodyUtils.bodyIsSnowBall(a) ? a : b).getUserData();
            Body bodyToCheck = BodyUtils.bodyIsSnowBall(a) ? b : a;

            if (!snowBallUserData.isNeedDelete()){
                if (BodyUtils.bodyIsPlayer(bodyToCheck)){
                    ((PlayerUserData)bodyToCheck.getUserData()).setBadTouched(true);
                    snowBallUserData.setNeedDelete(true);
                }
            }
        }
    }

    @Override
    public void endContact(Contact contact) {

        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        if ((FixtureUtils.fixtureIsPlayerGroundSensor(contact.getFixtureA()) && BodyUtils.bodyIsBlock(b))
                || ((FixtureUtils.fixtureIsPlayerGroundSensor(contact.getFixtureB()) && BodyUtils.bodyIsBlock(a)))) {
            Fixture fixture = FixtureUtils.fixtureIsPlayerGroundSensor(contact.getFixtureA()) ? contact.getFixtureA() : contact.getFixtureB();

            BlockUserData blockUserData = (BlockUserData) (BodyUtils.bodyIsBlock(a) ? a.getUserData() : b.getUserData());
            if (!blockUserData.isHided()) {
                Body body = fixture.getBody();
                PlayerUserData playerUserData = (PlayerUserData) body.getUserData();
//            playerUserData.setBlockId(-1);
                playerUserData.setNotLanded();
            }

        } else if ((FixtureUtils.fixtureIsGoblinGroundLandingSensor(contact.getFixtureA()) && BodyUtils.bodyIsBlock(b)) ||
                (FixtureUtils.fixtureIsGoblinGroundLandingSensor(contact.getFixtureB()) && BodyUtils.bodyIsBlock(a))) {
            Fixture fixture = FixtureUtils.fixtureIsGoblinGroundLandingSensor(contact.getFixtureA()) ? contact.getFixtureA() : contact.getFixtureB();

            Body body = fixture.getBody();
            GoblinUserData goblinUserData = (GoblinUserData) body.getUserData();
            goblinUserData.setLanded(false);

        } else if ((FixtureUtils.fixtureIsGoblinLeftEndGroundSensor(contact.getFixtureA()) && BodyUtils.bodyIsBlock(b))
                || ((FixtureUtils.fixtureIsGoblinLeftEndGroundSensor(contact.getFixtureB()) && BodyUtils.bodyIsBlock(a)))) {
            Fixture fixture = FixtureUtils.fixtureIsGoblinLeftEndGroundSensor(contact.getFixtureA()) ? contact.getFixtureA() : contact.getFixtureB();
            Body body = fixture.getBody();
            GoblinUserData goblinUserData = (GoblinUserData) body.getUserData();
            goblinUserData.setReachedEndOfPlatform(true);

        } else if ((FixtureUtils.fixtureIsGoblinRightEndGroundSensor(contact.getFixtureA()) && BodyUtils.bodyIsBlock(b))
                || ((FixtureUtils.fixtureIsGoblinRightEndGroundSensor(contact.getFixtureB()) && BodyUtils.bodyIsBlock(a)))) {
            Fixture fixture = FixtureUtils.fixtureIsGoblinRightEndGroundSensor(contact.getFixtureA()) ? contact.getFixtureA() : contact.getFixtureB();
            Body body = fixture.getBody();
            GoblinUserData goblinUserData = (GoblinUserData) body.getUserData();
            goblinUserData.setReachedEndOfPlatform(true);
        } else if (BodyUtils.bodyIsPike(a) || BodyUtils.bodyIsPike(b)) {
            Body pikeBody = BodyUtils.bodyIsPike(a) ? a : b;
            Body bodyToCheck = BodyUtils.bodyIsPike(a) ? b : a;

            PikeUserData pikeUserData = (PikeUserData) pikeBody.getUserData();

            if (BodyUtils.bodyIsSnowBall(bodyToCheck)) {
                SnowBallUserData snowBallUserData = (SnowBallUserData) bodyToCheck.getUserData();
                pikeUserData.setNeedDelete(true);
                snowBallUserData.setNeedDelete(true);
            } else if (BodyUtils.bodyIsSnowman(bodyToCheck) && !((SnowmanUserData)bodyToCheck.getUserData()).isNeedDelete()){
                pikeUserData.setNeedDelete(true);
                ((SnowmanUserData)bodyToCheck.getUserData()).setNeedDelete(true);
            }
        } else if (BodyUtils.bodyIsSnowBullet(a) || BodyUtils.bodyIsSnowBullet(b)){
            SnowBulletUserData bulletUserDate = (SnowBulletUserData) (BodyUtils.bodyIsSnowBullet(a) ? a : b).getUserData();
            Body bodyToCheck = BodyUtils.bodyIsSnowBullet(a) ? b : a;
            if (BodyUtils.bodyIsPlayer(bodyToCheck) && !bulletUserDate.isNeedDelete()){
                ((PlayerUserData)bodyToCheck.getUserData()).setBadTouched(true);
                bulletUserDate.setNeedDelete(true);
            }
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        if (BodyUtils.bodyIsBlock(a) || BodyUtils.bodyIsBlock(b)) {
            Body blockBody = BodyUtils.bodyIsBlock(a) ? a : b;
            BlockUserData blockUserData = (BlockUserData) blockBody.getUserData();

            if (blockUserData.isHided() || blockUserData.isFalling()) {
                contact.setEnabled(false);
            }

            if (BodyUtils.bodyIsFlyingBottle(a) || BodyUtils.bodyIsFlyingBottle(b)) {
                contact.setEnabled(false);
            }

            if (BodyUtils.bodyIsPike(a) || BodyUtils.bodyIsPike(b)) {
                contact.setEnabled(false);
            }

            if (BodyUtils.bodyIsTeleportBomb(a) || BodyUtils.bodyIsTeleportBomb(b)) {
                if (((BlockUserData) blockBody.getUserData()).getBlockId() == 0) {
                    contact.setEnabled(true);
                } else {
                    Body bodyToCheck = BodyUtils.bodyIsTeleportBomb(a) ? a : b;
                    contact.setEnabled(((TeleportBombUserData) bodyToCheck.getUserData()).isActivated());
                }
            }

            if (BodyUtils.bodyIsPlayer(a) || BodyUtils.bodyIsPlayer(b)) {
                Body playerBody = BodyUtils.bodyIsPlayer(a) ? a : b;
                PlayerUserData playerUserData = (PlayerUserData) playerBody.getUserData();
                contact.setFriction(playerUserData.getFriction());
            }
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsBlock(b))
                || (BodyUtils.bodyIsPlayer(b) && BodyUtils.bodyIsBlock(a))) {

        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsItemOnField(b)) ||
                (BodyUtils.bodyIsPlayer(b) && BodyUtils.bodyIsItemOnField(a))) {
            contact.setEnabled(false);
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsGoblin(b))
                || (BodyUtils.bodyIsPlayer(b) && BodyUtils.bodyIsGoblin(a))) {
            contact.setEnabled(false);
        } else if ((BodyUtils.bodyIsItemOnField(a) && BodyUtils.bodyIsGoblin(b))
                || (BodyUtils.bodyIsItemOnField(b) && BodyUtils.bodyIsGoblin(a))) {
            contact.setEnabled(false);
        } else if (BodyUtils.bodyIsFlyingBottle(b) || BodyUtils.bodyIsFlyingBottle(a)) {
            Body bodyToCheck = BodyUtils.bodyIsFlyingBottle(a) ? b : a;
            if (BodyUtils.bodyIsFallSensor(bodyToCheck)) {
                contact.setEnabled(true);
            } else {
                contact.setEnabled(false);
            }
        } else if (BodyUtils.bodyIsTeleportBomb(a) || BodyUtils.bodyIsTeleportBomb(b)) {
            contact.setEnabled(false);
        } else if (BodyUtils.bodyIsPike(a) || BodyUtils.bodyIsPike(b)) {
            Body bodyToCheck = BodyUtils.bodyIsPike(a) ? b : a;
            contact.setEnabled(false);
//            if (BodyUtils.bodyIsSnowBall(bodyToCheck)){
//                contact.setEnabled(false);
//            }
//            if (BodyUtils.bodyIsPlayer(bodyToCheck)){
//                contact.setEnabled(false);
//            }
        } else if (BodyUtils.bodyIsSnowman(a) || BodyUtils.bodyIsSnowman(b)){
            contact.setEnabled(false);
        } else if (BodyUtils.bodyIsSnowBall(a) || BodyUtils.bodyIsSnowBall(b)){
            Body bodyToCheck = BodyUtils.bodyIsSnowBall(a) ? b : a;
            if (!BodyUtils.bodyIsBlock(bodyToCheck)){
                contact.setEnabled(false);
            }
        } else if (BodyUtils.bodyIsSnowBullet(a) || BodyUtils.bodyIsSnowBullet(b)){
            Body bodyToCheck = BodyUtils.bodyIsSnowBullet(a) ? b : a;
            contact.setEnabled(false);
//            if (BodyUtils.bodyIsPlayer(bodyToCheck)){
//                contact.setEnabled(true);
//            } else {
//                contact.setEnabled(false);
//            }
        } else if (BodyUtils.bodyIsSideSensor(a) || BodyUtils.bodyIsSideSensor(b)){
            Body bodyToCheck = BodyUtils.bodyIsSideSensor(a) ? b : a;
            if (BodyUtils.bodyIsGoblin(bodyToCheck) ||
                    BodyUtils.bodyIsSnowBall(bodyToCheck) || BodyUtils.bodyIsSnowBullet(bodyToCheck)
                    ||BodyUtils.bodyIsSideSensor(bodyToCheck)
            ) {

                contact.setEnabled(false);
            }
        }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

}
