package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

import static ru.darvell.game.jumpergame.box2dworld.UserDataType.BLOCK;

public class BlockUserData extends UserData {

    private boolean shaked;
    private boolean falling = false;
    private float shakeTime = 0;
    private int blockId = 0;
    private boolean hided = false;
    private boolean collideWithBlock = false;
    private boolean initialState = true;
    private boolean aiOnPause = false;

//    protected BlockLengthTypes blockLengthType;

    public BlockUserData(float width) {
        super();
//        blockLengthType = lengthTypes;
        userDataType = BLOCK;
        this.width = width;
        height = Constants.BLOCK_HEIGHT;
        shaked = false;
    }

    public BlockUserData(float width, UserDataType type){
        this(width);
        userDataType = type;
    }

    public boolean isShaked() {
        return shaked;
    }

    public void setShaked(boolean shaked) {
        this.shaked = shaked;
        if (shaked){
            if (initialState){
                initialState = false;
            } else {
                shakeTime = 2f;
            }
        }
    }

    public float getShakeTime() {
        return shakeTime;
    }

    public void setShakeTime(float shakeTime) {
        this.shakeTime = shakeTime;
    }

    public int getBlockId() {
        return blockId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }

    public boolean isHided() {
        return hided;
    }

    public void setHided(boolean hided) {
        this.hided = hided;
    }

    public boolean isFalling() {
        return falling;
    }

    public void setFalling(boolean falling) {
        this.falling = falling;
    }

    public boolean isCollideWithBlock() {
        return collideWithBlock;
    }

    public void setCollideWithBlock(boolean collideWithBlock) {
        if (isAiOnPause()){
            if (!collideWithBlock){
                this.collideWithBlock = false;
            }
        } else {
            this.collideWithBlock = collideWithBlock;
        }
    }

    public boolean isAiOnPause() {
        return aiOnPause;
    }

    public void setAiOnPause(boolean aiOnPause) {
        this.aiOnPause = aiOnPause;
    }
}
