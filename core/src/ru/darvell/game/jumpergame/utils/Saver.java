package ru.darvell.game.jumpergame.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Json;
import ru.darvell.game.jumpergame.entitys.utils.PlayerPouch;
import ru.darvell.game.jumpergame.entitys.utils.SpaceShipInventory;

import java.util.HashMap;
import java.util.Map;


public class Saver {

    private final String PREFS = "GAME_PREFS";
    private final Preferences preferences;

    public static final String POUCH = "POUCH";
    public static final String SPACE_SHIP = "SPACE_SHIP";
    public static final String LEVEL = "CURR_LEVEL";

    public Saver() {
        preferences = Gdx.app.getPreferences(PREFS);
    }

    public void saveData(PlayerPouch playerPouch, SpaceShipInventory spaceShipInventory, Levels level){
        Json json = new Json();

        preferences.putString(POUCH, json.toJson(playerPouch, PlayerPouch.class));

        if (spaceShipInventory != null){
            preferences.putString(SPACE_SHIP, json.toJson(spaceShipInventory, SpaceShipInventory.class));
        }

        preferences.putInteger(LEVEL, level.ordinal());
        preferences.flush();
    }

    public void resetData(){
        preferences.clear();
    }

    public Map<String, Object> loadData(){
        Map<String, Object> result = new HashMap<String, Object>();

        Json json = new Json();
        PlayerPouch pouch = json.fromJson(PlayerPouch.class, preferences.getString(POUCH));
        result.put(POUCH, pouch);

        SpaceShipInventory spaceShipInventory = json.fromJson(SpaceShipInventory.class, preferences.getString(SPACE_SHIP));
        result.put(SPACE_SHIP, spaceShipInventory);

        result.put(LEVEL, Levels.values()[preferences.getInteger(LEVEL)]);

        return result;
    }

}
