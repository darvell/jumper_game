package ru.darvell.game.jumpergame.stages;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import ru.darvell.game.jumpergame.screens.ParentScreen;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;

public abstract class NonGameStage extends Stage {

    protected float viewPortWidth;
    protected float viewPortHeight;

    protected ParentScreen parentScreen;
    protected Assets assets;
    protected UIActor background;

    public NonGameStage(float width, float height, Assets assets, final ParentScreen parentScreen) {
        super(new FillViewport(width, height));
        viewPortWidth = width;
        viewPortHeight = height;

        this.parentScreen = parentScreen;
        this.assets = assets;
        initBackground();
    }

    protected void initBackground(){
        background = new UIActor();
        setBackgroundTexture(background);
        background.setX(0);
        background.setY(viewPortHeight / 2 - background.getHeight() / 2);

        addActor(background);
    }

    abstract void setBackgroundTexture(UIActor background);


}
