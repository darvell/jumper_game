package ru.darvell.game.jumpergame.desktop;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class MyPacker {
    public static void main (String[] args) throws Exception {
        TexturePacker.process("game_assets/texture_atlases/level_one/to_map"
                , "android/assets/data/level_one/", "atlas");
        TexturePacker.process("game_assets/texture_atlases/level_two/to_map"
                , "android/assets/data/level_two/", "atlas");
        TexturePacker.process("game_assets/texture_atlases/level_three/to_map"
                , "android/assets/data/level_three/", "atlas");
        TexturePacker.process("game_assets/texture_atlases/level_four/to_map"
                , "android/assets/data/level_four/", "atlas");
        TexturePacker.process("game_assets/texture_atlases/level_five/to_map"
                , "android/assets/data/level_five/", "atlas");
        TexturePacker.process("game_assets/texture_atlases/level_six/to_map"
                , "android/assets/data/level_six/", "atlas");

        TexturePacker.process("game_assets/texture_atlases/hero"
                , "android/assets/data/hero/", "hero");

        TexturePacker.process("game_assets/texture_atlases/ui/non_localed"
                , "android/assets/data/ui/", "ui");

        TexturePacker.process("game_assets/texture_atlases/ui/localed/ru_RU"
                , "android/assets/data/ui/localed/ru_RU", "ui");

        TexturePacker.process("game_assets/texture_atlases/ui/localed/en_US"
                , "android/assets/data/ui/localed/en_US", "ui");

//        TexturePacker.process("game_assets/texture_atlases/ui"
//                , "android/assets/data/ui/", "ui");

        Runtime runtime = Runtime.getRuntime();

        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_two/first_layer_background.png android/assets/data/level_two/first_layer_background_%d.png");
        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_two/second_layer_background.png android/assets/data/level_two/second_layer_background_%d.png");


        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_three/first_layer_background.png android/assets/data/level_three/first_layer_background_%d.png");
        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_three/second_layer_background.png android/assets/data/level_three/second_layer_background_%d.png");
        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_three/third_layer_background.png android/assets/data/level_three/third_layer_background_%d.png");

        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_four/first_layer_background.png android/assets/data/level_four/first_layer_background_%d.png");

        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_five/first_layer_background.png android/assets/data/level_five/first_layer_background_%d.png");
        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_five/second_layer_background.png android/assets/data/level_five/second_layer_background_%d.png");
        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_five/third_layer_background.png android/assets/data/level_five/third_layer_background_%d.png");

        runtime.exec("convert -crop 135x1024 game_assets/texture_atlases/level_six/first_layer_background.png android/assets/data/level_six/first_layer_background_%d.png");
    }
}