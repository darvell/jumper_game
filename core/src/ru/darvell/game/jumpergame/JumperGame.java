package ru.darvell.game.jumpergame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.BufferUtils;
import ru.darvell.game.jumpergame.screens.GamePlayScreen;
import ru.darvell.game.jumpergame.screens.UiScreen;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.GdxLog;
import ru.darvell.game.jumpergame.utils.Levels;
import ru.darvell.game.jumpergame.utils.Saver;

import java.nio.IntBuffer;

public class JumperGame extends Game {

    private Screen currentScreen;
    private Assets assets;
    private Saver saver;

    @Override
    public void create() {

        IntBuffer intBuffer = BufferUtils.newIntBuffer(16);
        Gdx.gl20.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, intBuffer);
        System.out.println(intBuffer.get());
        saver = new Saver();
        saver.loadData();

        GdxLog.DEBUG = true;

//        changeLevel(LEVEL_ONE);
        changeToMainMenu();
    }

    public void changeToMainMenu(){
        if (assets != null && assets.getLoadedAssets() > 0) {
            assets.dispose();
        }
        assets = new Assets();
        assets.load();
        assets.finishLoading();
        currentScreen = new UiScreen(assets, this, saver);
        setScreen(currentScreen);
    }

    public void changeLevel(Levels level){
        if (assets != null && assets.getLoadedAssets() > 0) {
            assets.dispose();
        }
        assets = new Assets();
        assets.setCurrentLevel(level);
        assets.load();
        assets.finishLoading();
        currentScreen = new GamePlayScreen(assets, level, saver, this);
        setScreen(currentScreen);
    }

    @Override
    public void dispose() {
        super.dispose();
        assets.dispose();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        currentScreen.resize(width, height);
    }
}
