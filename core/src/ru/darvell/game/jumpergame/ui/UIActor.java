package ru.darvell.game.jumpergame.ui;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class UIActor extends Actor {

    protected TextureRegion textureRegion;
    protected Animation animation;

    private boolean animated = false;
    protected float stateTime = 0f;

    public UIActor(TextureRegion textureRegion){
        this.textureRegion = textureRegion;
        setWidth(textureRegion.getRegionWidth());
        setHeight(textureRegion.getRegionHeight());
    }

    public UIActor(){
    }

    public UIActor(Animation animation){
        this.animation = animation;
        setWidth(((TextureRegion)animation.getKeyFrames()[0]).getRegionWidth());
        setHeight(((TextureRegion)animation.getKeyFrames()[0]).getRegionHeight());
        animated = true;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        stateTime += delta;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (animated) {
            textureRegion = (TextureRegion) animation.getKeyFrame(stateTime, true);
        }
        if (textureRegion != null) {
            batch.draw(textureRegion, getX(), getY(), getWidth(), getHeight());
        }
    }

    public void setTextureRegion(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
        setWidth(textureRegion.getRegionWidth());
        setHeight(textureRegion.getRegionHeight());
    }
}
