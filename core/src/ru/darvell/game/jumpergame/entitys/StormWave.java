package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Group;
import ru.darvell.game.jumpergame.box2dworld.StormWaveUserData;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;

public class StormWave extends Group {

    private UIActor waveUp;
    private UIActor waveDown;
    private Body body;
    private boolean active;
    private boolean falling;

    private float fallTimeOut = 0;

    private float screenHeight;
    private float velocity = 2.5f;

    public StormWave(Body body, Assets assets, float screenHeight, float screenWidth) {
        this.body = body;
        this.screenHeight = screenHeight;
        waveUp = new UIActor(assets.getForAllAnimation(Assets.VAWE, 0.1f));
        waveDown = new UIActor(assets.getUITexture(Assets.WAVE_COLOR));

        waveDown.setHeight(screenHeight);
        waveDown.setWidth(135f);

        setWidth(waveDown.getWidth());
        setHeight(waveUp.getHeight() + waveDown.getHeight());
        waveDown.setY(0);
        waveUp.setY(waveDown.getY() + waveDown.getHeight());

        setX(0);

        addActor(waveUp);
        addActor(waveDown);
        setActive(false);

    }

    @Override
    public void act(float delta) {
        waveUp.act(delta);
        updateWavePositions();
        if (isActive()) {
            int direction;
            if (fallTimeOut > 0){
                fallTimeOut -= delta;
                direction = -2;
            } else {
                fallTimeOut = 0;
                direction = 1;
            }

            body.setLinearVelocity(0, velocity * direction);
        } else {
            body.setLinearVelocity(0, 0);
        }
    }

    private void updateWavePositions(){
        if (body != null) {
            setY(((body.getPosition().y + Constants.FALL_SENSOR_HEIGHT / 2) * Constants.WORLD_TO_SCREEN) - getHeight() + 70f);
        }
    }

    protected StormWaveUserData getUserData() throws NullPointerException {
        return body != null ? (StormWaveUserData) body.getUserData() : null;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void initFalling(){
        fallTimeOut = 5f;
    }
}
