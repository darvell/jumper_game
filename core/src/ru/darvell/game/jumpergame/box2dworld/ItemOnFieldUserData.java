package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

public class ItemOnFieldUserData extends UserData {

    private boolean pickedUp = false;

    public ItemOnFieldUserData(UserDataType type) {
        this.userDataType = type;
        height = Constants.STONE_HEIGHT;
        width = Constants.STONE_HEIGHT;
    }

    public boolean isPickedUp() {
        return pickedUp;
    }

    public void setPickedUp(boolean pickedUp) {
        this.pickedUp = pickedUp;
    }
}
