package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.UserData;
import ru.darvell.game.jumpergame.utils.Constants;

public abstract class GameActor extends SimpleActor {

    protected Body body;
    protected UserData userData;
    private boolean active = false;

    public GameActor(Body body) {
        this.body = body;
        this.userData = (UserData) body.getUserData();
    }

    protected abstract UserData getUserData() throws NullPointerException;

    @Override
    public void act(float delta) {
        super.act(delta);
        if ((body != null) && (body.getUserData() != null)){
            updatePosition();
        }
    }

    protected void updatePosition() {
        setX(transformToScreen(body.getPosition().x - userData.getWidth() / 2));
        setY(transformToScreen(body.getPosition().y - userData.getHeight() / 2 + userData.getyOffsetForDrawing()));
        setWidth(transformToScreen(userData.getWidth()));
        setHeight(transformToScreen(userData.getHeight()));
    }

    protected float transformToScreen(float n) {
        return Constants.WORLD_TO_SCREEN * n;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isNeedDelete(){
        if (body != null && userData != null && userData.isNeedDelete()){
            return true;
        }
        return false;
    }

    public void setNeedDelete(){
        if (body != null && userData != null && !userData.isNeedDelete()){
            userData.setNeedDelete(true);
        }
    }

    public Body getBody(){
        return body;
    }

    public Vector2 getCenterOrig(){
        Vector2 v = new Vector2();
        if (body != null) {
            v.set(body.getPosition());
        }
        return v;
    }
}
