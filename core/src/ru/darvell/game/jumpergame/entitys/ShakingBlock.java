package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.entitys.interfaces.ShakeListener;

public class ShakingBlock extends Block {

    private ShakeListener shakeListener;
    private int shakeDirection = -1;
    private float shakeVelocity = 1f;

    public ShakingBlock(Body body, float drawingHeight, TextureRegion texture, ShakeListener shakeListener) {
        super(body, drawingHeight, texture);
        this.shakeListener = shakeListener;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (getUserData() != null) {
            if (getUserData().isShaked() && getUserData().getShakeTime() > 0) {

                shakeListener.blockShaked(getUserData().getBlockId());

                body.setLinearVelocity(new Vector2(0, shakeVelocity * shakeDirection));
                if (body.getPosition().y < originY - 0.08f) {
                    shakeDirection = 1;
                } else if (body.getPosition().y > originY + 0.08f) {
                    shakeDirection = -1;
                }
                getUserData().setShakeTime(getUserData().getShakeTime() - delta);
            }
            if (getUserData().isShaked() && getUserData().getShakeTime() <= 0) {
                getUserData().setShaked(false);
                body.setLinearVelocity(new Vector2(0, 0));
                shakeDirection = -1;
            }
        }
    }
}
