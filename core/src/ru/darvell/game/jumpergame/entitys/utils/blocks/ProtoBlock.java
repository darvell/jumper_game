package ru.darvell.game.jumpergame.entitys.utils.blocks;

public class ProtoBlock {
    private BlockTypes type;
    private float startX;
    private boolean movable = false;
    private boolean soul = false;

    public ProtoBlock() {
    }

    public ProtoBlock(BlockTypes type, float startX) {
        this.type = type;
        this.startX = startX;
    }

    public ProtoBlock(BlockTypes type, float startX, boolean movable) {
        this.type = type;
        this.startX = startX;
        this.movable = movable;
        this.soul = !movable;
    }

    public BlockTypes getType() {
        return type;
    }

    public float getStartX() {
        return startX;
    }

    public void setType(BlockTypes type) {
        this.type = type;
    }

    public void setStartX(float startX) {
        this.startX = startX;
    }

    public boolean isMovable() {
        return movable;
    }

    public boolean isSoul() {
        return soul;
    }
}
