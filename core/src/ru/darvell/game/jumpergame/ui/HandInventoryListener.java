package ru.darvell.game.jumpergame.ui;

public interface HandInventoryListener {
    void onHandInventoryClick(HandInventoryType inventoryType, HandInventory handInventory);
}
