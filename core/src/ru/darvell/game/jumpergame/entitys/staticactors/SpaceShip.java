package ru.darvell.game.jumpergame.entitys.staticactors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.darvell.game.jumpergame.utils.Assets;

public class SpaceShip extends Actor {

    private TextureRegion texture;

    public SpaceShip(Assets assets, float x, float y, final InGameObjectsClickListener clickListener){
        texture = (TextureRegion) assets.getWorldAnimation(Assets.SPACESHIP, 0)
                .getKeyFrame(0f);
        setX(x - texture.getRegionWidth() / 2f);
        setY(y);
        setWidth(texture.getRegionWidth());
        setHeight(texture.getRegionHeight());
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                clickListener.spaceShipCliked();
                return true;
            }
        });
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY());
    }
}
