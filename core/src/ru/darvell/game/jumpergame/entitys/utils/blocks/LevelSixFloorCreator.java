package ru.darvell.game.jumpergame.entitys.utils.blocks;

import com.badlogic.gdx.physics.box2d.World;
import ru.darvell.game.jumpergame.utils.Assets;

import static ru.darvell.game.jumpergame.entitys.utils.blocks.BlockTypes.*;

public class LevelSixFloorCreator extends LevelOneFloorCreator {

    public LevelSixFloorCreator(World world, Assets assets) {
        super(world, assets);
    }

    @Override
    void initPatterns() {
        generatorPatterns.add(new FloorPattern(1, new ProtoBlock(BLOCK_FOUR, 0.1f)));
        generatorPatterns.add(new FloorPattern(2, new ProtoBlock(BLOCK_FOUR, 6f)));
        generatorPatterns.add(new FloorPattern(3, new ProtoBlock(BLOCK_FOUR, 3f)));

        generatorPatterns.add(new FloorPattern(4, new ProtoBlock(BLOCK_FOUR, 0.1f), new ProtoBlock(BLOCK_TWO, 9.6f)));
        generatorPatterns.add(new FloorPattern(5, new ProtoBlock(BLOCK_TWO, 0.1f), new ProtoBlock(BLOCK_FOUR, 6f)));

        generatorPatterns.add(new FloorPattern(6, new ProtoBlock(BLOCK_FOUR, 0.1f), new ProtoBlock(BLOCK_ONE, 9.3f)));
        generatorPatterns.add(new FloorPattern(7, new ProtoBlock(BLOCK_ONE, 1.3f), new ProtoBlock(BLOCK_FOUR, 6f)));
// Three
        generatorPatterns.add(new FloorPattern(8, new ProtoBlock(BLOCK_THREE, 6f)));
        generatorPatterns.add(new FloorPattern(9, new ProtoBlock(BLOCK_THREE, 2f)));

        generatorPatterns.add(new FloorPattern(10, new ProtoBlock(BLOCK_THREE, 0.1f)));
        generatorPatterns.add(new FloorPattern(11, new ProtoBlock(BLOCK_THREE, 8.1f)));

        generatorPatterns.add(new FloorPattern(12, new ProtoBlock(BLOCK_THREE, 0.1f), new ProtoBlock(BLOCK_THREE, 8.1f)));
        generatorPatterns.add(new FloorPattern(13, new ProtoBlock(BLOCK_THREE, 0.1f), new ProtoBlock(BLOCK_TWO, 8.6f)));
        generatorPatterns.add(new FloorPattern(14, new ProtoBlock(BLOCK_TWO, 2f), new ProtoBlock(BLOCK_THREE, 8f)));

        generatorPatterns.add(new FloorPattern(15, new ProtoBlock(BLOCK_THREE, 0.1f), new ProtoBlock(BLOCK_ONE, 8.6f)));
        generatorPatterns.add(new FloorPattern(16, new ProtoBlock(BLOCK_THREE, 0.1f), new ProtoBlock(BLOCK_ONE, 7.5f)));

        generatorPatterns.add(new FloorPattern(17, new ProtoBlock(BLOCK_ONE, 2f), new ProtoBlock(BLOCK_THREE, 8.1f)));
        generatorPatterns.add(new FloorPattern(18, new ProtoBlock(BLOCK_ONE, 3f), new ProtoBlock(BLOCK_THREE, 8.1f)));

// Two
        generatorPatterns.add(new FloorPattern(19, new ProtoBlock(BLOCK_TWO, 2f), new ProtoBlock(BLOCK_ONE, 9f)));
        generatorPatterns.add(new FloorPattern(20, new ProtoBlock(BLOCK_ONE, 2f), new ProtoBlock(BLOCK_TWO, 8.1f)));

        generatorPatterns.add(new FloorPattern(21, new ProtoBlock(BLOCK_TWO, 0.1f), new ProtoBlock(BLOCK_ONE, 6f), new ProtoBlock(BLOCK_ONE, 10.6f)));
        generatorPatterns.add(new FloorPattern(22, new ProtoBlock(BLOCK_ONE, 0.1f), new ProtoBlock(BLOCK_ONE, 5f), new ProtoBlock(BLOCK_TWO, 9.6f)));
        generatorPatterns.add(new FloorPattern(23, new ProtoBlock(BLOCK_ONE, 0.1f), new ProtoBlock(BLOCK_TWO, 5f), new ProtoBlock(BLOCK_ONE, 10.6f)));
// One
        generatorPatterns.add(new FloorPattern(24, new ProtoBlock(BLOCK_ONE, 0.1f), new ProtoBlock(BLOCK_ONE, 5.5f), new ProtoBlock(BLOCK_ONE, 10.6f)));
        generatorPatterns.add(new FloorPattern(25, new ProtoBlock(BLOCK_ONE, 1.5f), new ProtoBlock(BLOCK_ONE, 7f)));
        generatorPatterns.add(new FloorPattern(26, new ProtoBlock(BLOCK_ONE, 3.5f), new ProtoBlock(BLOCK_ONE, 9f)));

        generatorPatterns.add(new FloorPattern(27, new ProtoBlock(BLOCK_ONE, 10f)));

        generatorPatterns.add(new FloorPattern(28, new ProtoBlock(BLOCK_ONE, 2f)));
        generatorPatterns.add(new FloorPattern(29, new ProtoBlock(BLOCK_ONE, 5.6f)));


    }
}
