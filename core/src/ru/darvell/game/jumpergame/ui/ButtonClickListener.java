package ru.darvell.game.jumpergame.ui;

public interface ButtonClickListener {

    void onButtonClick(ButtonType buttonType);
    void onButtonUp(ButtonType buttonType);

}
