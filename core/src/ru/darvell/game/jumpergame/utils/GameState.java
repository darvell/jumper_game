package ru.darvell.game.jumpergame.utils;

public enum GameState {

    LOADING,
    PLAYING,
    PAUSE
}
