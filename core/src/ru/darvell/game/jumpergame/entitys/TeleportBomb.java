package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.TeleportBombUserData;
import ru.darvell.game.jumpergame.utils.Assets;

public class TeleportBomb extends GameActor {

    private TextureRegion textureRegion;
    private float activateTimeout;
    private float stateTime = 0;

    public TeleportBomb(Body body, Assets assets) {
        super(body);
        textureRegion = assets.getUITexture(Assets.UI_TELEPORT_BOMB);
    }

    @Override
    protected TeleportBombUserData getUserData() throws NullPointerException {
        return body != null ? (TeleportBombUserData) body.getUserData() : null;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (body != null){
            batch.draw(textureRegion, getX(), getY(),
                    getWidth() / 2, getHeight() / 2,
                    getWidth(), getHeight()
                    , 1, 1,
                    body.getAngle() * 100);
        }
    }

    public void throwUp(int multiplier, int direction) {
        if (body != null){
            Vector2 power = new Vector2(0, getUserData().velocity * multiplier * direction);
            Vector2 v2Center = new Vector2(body.getWorldCenter());
            body.applyLinearImpulse(power, v2Center, true);
            body.applyAngularImpulse(1f, true);
            activateTimeout = multiplier * 0.2f;

        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        stateTime += delta;
        if (body != null) {
            Vector2 tmpVel = body.getLinearVelocity();
            tmpVel.x = 0;
            body.setLinearVelocity(tmpVel);
            if (stateTime > activateTimeout) {
                getUserData().setActivated(true);
            }
        }
    }

    public boolean isActivated(){
        if (getUserData() != null) {
            return getUserData().activated;
        }
        return false;
    }

}
