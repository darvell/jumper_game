package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.PlayerUserData;
import ru.darvell.game.jumpergame.entitys.interfaces.Controllable;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;
import ru.darvell.game.jumpergame.utils.GdxLog;

public class Player extends GameActor implements Controllable {

    public static final String LOG_TAG = "Player";

    private boolean moving = false;
    private int moveDirection = 1;
    private Vector2 velocity = new Vector2();

    private Animation stayRight;
    private Animation stayLeft;
    private Animation runRight;
    private Animation runLeft;
    private Animation jumpRight;
    private Animation jumpLeft;
    private Animation inAirRight;
    private Animation inAirLeft;
    private Animation landingRight;
    private Animation landingLeft;
    private Animation bamRight;
    private Animation bamLeft;
    private float stateTime;

    private boolean jumpingInit = false;
    private boolean jumpingAnimFinish = true;

    private boolean doLandingAnimation = false;
    private boolean iceLevel = false;

    private int blinkingCount = 0;
    private boolean skipFrame;
    private int framesToSkip = 0;

    private int lifes;
    private int yellowStoneCount;
    private int blueStoneCount;
    private boolean inBoots = false;
    private boolean hasLight = false;
    private float inBirdaminsTimeLeft = 0f;

    public Player(Body body, Assets assets) {
        super(body);
        stayRight = assets.getForAllAnimation(Assets.HERO_STAY_RIGHT, 0.1f);
        stayLeft = assets.getForAllAnimation(Assets.HERO_STAY_LEFT, 0.1f);
        runRight = assets.getForAllAnimation(Assets.HERO_RUN_RIGHT, 0.08f);
        runLeft = assets.getForAllAnimation(Assets.HERO_RUN_LEFT, 0.08f);
        jumpRight = assets.getForAllAnimation(Assets.HERO_JUMP_RIGHT, 0.1f);
        jumpLeft = assets.getForAllAnimation(Assets.HERO_JUMP_LEFT, 0.1f);
        inAirRight = assets.getForAllAnimation(Assets.HERO_JUMP_IN_AIR_RIGHT, 0.4f);
        inAirLeft = assets.getForAllAnimation(Assets.HERO_JUMP_IN_AIR_LEFT, 0.4f);
        landingRight = assets.getForAllAnimation(Assets.HERO_LANDING_RIGHT, 0.1f);
        landingLeft = assets.getForAllAnimation(Assets.HERO_LANDING_LEFT, 0.1f);
        bamRight = assets.getForAllAnimation(Assets.HERO_BAM_RIGHT, 0.3f);
        bamLeft = assets.getForAllAnimation(Assets.HERO_BAM_LEFT, 0.3f);
        lifes = Constants.PLAYER_MAX_LIFE_COUNT;
        yellowStoneCount = 0;
        blueStoneCount = 0;
    }

    public void initBlinking() {
        if (blinkingCount == 0) {
            blinkingCount = Constants.PLAYER_BLINKING_COUNT;
            skipFrame = true;
            framesToSkip = Constants.PLAYER_BLINKING_SKIP_FRAMES_COUNT;
        }
    }

    @Override
    protected PlayerUserData getUserData() {
        if (body != null && body.getUserData() != null) {
            return (PlayerUserData) body.getUserData();
        }
        return null;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (getUserData() != null) {
            if (isBirdamins()) {
                inBirdaminsTimeLeft -= delta;
                if (inBirdaminsTimeLeft <= 0) {
                    GdxLog.d(LOG_TAG, "Birdamins in over");
                }
            }

            if (iceLevel) {
                updateFriction(0.4f);
            }

            if (moving) {
                if (!iceLevel) {
                    if (isInBoots()) {
                        updateFriction(5f);
                    } else {
                        updateFriction(0f);
                    }
                }

                if (moveDirection == 1) {
                    if (body.getLinearVelocity().x < (Constants.PLAYER_MAX_VILOCITY + (isBirdamins() ? Constants.PLAYER_MAX_VILOCITY_BADDITIONAL : 0))) {
                        getBody().applyLinearImpulse(new Vector2(moveDirection * getUserData().getSpeed(), 0), getBody().getWorldCenter(), true);
                    }
                } else {
                    if (body.getLinearVelocity().x > (Constants.PLAYER_MAX_VILOCITY + (isBirdamins() ? Constants.PLAYER_MAX_VILOCITY_BADDITIONAL : 0)) * -1) {
                        getBody().applyLinearImpulse(new Vector2(moveDirection * getUserData().getSpeed(), 0), getBody().getWorldCenter(), true);
                    }
                }

            } else {
                if (!iceLevel) {
                    updateFriction(1000f);
                } else {
                    if (isInBoots()) {
                        updateFriction(1000f);
                    }
                }

                if (!getUserData().isLanded()) {
                    Vector2 tmpVel = body.getLinearVelocity();
                    tmpVel.x = 0;
                    body.setLinearVelocity(tmpVel);
                }
            }


            if (isAlive()) {
                if (jumpingInit) {
                    if (jumpingAnimFinish) {
                        jump();
                        jumpingInit = false;
                        stateTime = 0;
                    }
                }

                if (getUserData().isNeedLandingAnimation()) {
                    if (getUserData().isLanded()) {
                        doLandingAnimation = true;
                        stateTime = 0;
                    }
                    getUserData().setNeedLandingAnimation(false);
                }


                if (getUserData().isFellDown()) {
                    getUserData().setFellDown(false);
                    if (!isBirdamins()) {
                        hit();
                    }
                }

                if (getUserData().isBadTouched()) {
                    getUserData().setBadTouched(false);
                    hit();
                }
            }
        }
    }

    @Override
    protected void updatePosition() {
        float rightOffset = 0;
        if (moveDirection > 0) {
            rightOffset = .1f;
        }
        setX(transformToScreen(body.getPosition().x - Constants.PLAYER_DRAWING_WIDTH / 2 + rightOffset));
        setY(transformToScreen(body.getPosition().y - Constants.PLAYER_DRAWING_HEIGHT / 2));
        setWidth(transformToScreen(Constants.PLAYER_DRAWING_WIDTH));
        setHeight(transformToScreen(Constants.PLAYER_DRAWING_HEIGHT));
    }

    public Body getBody() {
        return body;
    }

    private void processStonesTouches() {
        if (getUserData().isTouchBlueStone()) {
            blueStoneCount++;
            getUserData().setTouchBlueStone(false);
        } else if (getUserData().isTouchYellowStone()) {
            yellowStoneCount++;
            getUserData().setTouchYellowStone(false);
            System.out.println("Collect yellow stone");
            System.out.println(yellowStoneCount);
        }
    }

    public void jump() {
        try {
            if (getUserData().isLanded()) {
//                body.applyLinearImpulse(getUserData().getJumpingLinearImpulse(), body.getWorldCenter(), true);
                velocity.y = getUserData().getJumpVelocity() + (isBirdamins() ? Constants.PLAYER_JUMP_VELOCITY_BADDITIONAL : 0);
                body.setLinearVelocity(velocity);
                getUserData().setBeenJumped(true);
            } else if (getUserData().hasExtraJump() && getUserData().isBeenJumped()) {
                getUserData().setExtraJump(false);
                velocity.y = getUserData().getExtraJumpVelocity() + (isBirdamins() ? Constants.PLAYER_EXTRA_JUMP_VELOCITY_BADDITIONAL : 0);
                body.setLinearVelocity(velocity);
//                body.applyLinearImpulse(getUserData().getExtraJumpingLinearImpulse(), body.getWorldCenter(), true);
            }
        } catch (NullPointerException e) {

        }
    }

    @Override
    public void initJumping() {
        if (isAlive()) {
            jumpingInit = true;
            jumpingAnimFinish = false;
            stateTime = 0f;
        }
    }

    @Override
    public void initMoveRight() {
        if (isAlive()) {
            try {
                moving = true;
                moveDirection = 1;
                velocity.x = moveDirection * getUserData().getSpeed();
            } catch (NullPointerException e) {

            }
        }
    }

    @Override
    public void initMoveLeft() {
        if (isAlive()) {
            try {
                moving = true;
                moveDirection = -1;
                velocity.x = moveDirection * getUserData().getSpeed();

            } catch (NullPointerException e) {

            }
        }
    }

    @Override
    public void stopMovingRight() {
        if (moveDirection == 1) {
            moving = false;
            velocity.x = 0;
        }
    }

    @Override
    public void stopMovingLeft() {
        if (moveDirection == -1) {
            moving = false;
            velocity.x = 0;
        }
    }

    public void updateFriction(float fric) {
        if (fric > 0 && !getUserData().isLanded()) {
            fric = 0;
        }
        getUserData().setFriction(fric);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        stateTime += Gdx.graphics.getDeltaTime();
        TextureRegion textureToDraw;

        if (blinkingCount > 0) {
            if (skipFrame) {
                if (framesToSkip == 0) {
                    skipFrame = false;
                    blinkingCount--;
                } else {
                    framesToSkip--;
                    return;
                }
            }
        }
        if (!isAlive()) {
            if (moveDirection > 0) {
                textureToDraw = (TextureRegion) bamRight.getKeyFrame(stateTime, false);
            } else {
                textureToDraw = (TextureRegion) bamLeft.getKeyFrame(stateTime, false);
            }
        } else {
            if (jumpingInit) {
                if (moveDirection > 0) {
                    textureToDraw = (TextureRegion) jumpRight.getKeyFrame(stateTime, false);
                    jumpingAnimFinish = jumpRight.isAnimationFinished(stateTime);
                } else {
                    textureToDraw = (TextureRegion) jumpLeft.getKeyFrame(stateTime, false);
                    jumpingAnimFinish = jumpLeft.isAnimationFinished(stateTime);
                }
            } else if (doLandingAnimation) {
                if (moveDirection > 0) {
                    textureToDraw = (TextureRegion) landingRight.getKeyFrame(stateTime, false);
                    doLandingAnimation = !landingRight.isAnimationFinished(stateTime);
                } else {
                    textureToDraw = (TextureRegion) landingLeft.getKeyFrame(stateTime, false);
                    doLandingAnimation = !landingLeft.isAnimationFinished(stateTime);
                }
            } else if (getUserData() != null && !getUserData().isLanded()) {
                if (moveDirection > 0) {
                    textureToDraw = (TextureRegion) inAirRight.getKeyFrame(stateTime, false);
                } else {
                    textureToDraw = (TextureRegion) inAirLeft.getKeyFrame(stateTime, false);
                }
            } else if (moving) {
                if (moveDirection > 0) {
                    textureToDraw = (TextureRegion) runRight.getKeyFrame(stateTime, true);
                } else {
                    textureToDraw = (TextureRegion) runLeft.getKeyFrame(stateTime, true);
                }

            } else {
                if (moveDirection > 0) {
                    textureToDraw = (TextureRegion) stayRight.getKeyFrame(1, false);
                } else {
                    textureToDraw = (TextureRegion) stayLeft.getKeyFrame(1, false);
                }
            }
        }
        batch.draw(textureToDraw, getX(), getY(), getWidth(), getHeight());
        skipFrame = !skipFrame;
        framesToSkip = Constants.PLAYER_BLINKING_SKIP_FRAMES_COUNT;
    }

    public int getBlockStanding() {
        return getUserData() != null ? getUserData().getBlockId() : -1;
    }

    public int getLastBlockStanding() {
        return getUserData() != null ? getUserData().getLastBlockId() : -1;
    }

    public void hit() {
        if (blinkingCount == 0 && isAlive()) {
            if (lifes > 1) {
                lifes--;
                initBlinking();
            } else {
                lifes--;
            }
        }
        if (!isAlive()) {
            stateTime = 0f;
            stopMovingLeft();
            stopMovingRight();
        }
    }

    public int getLifes() {
        return lifes;
    }

    public boolean isAlive() {
        return lifes > 0;
    }

    public void moveToBlock(Block block) {
        if (getUserData() != null) {
            getBody().setTransform(block.getPositionForGeneration().x, block.getPositionForGeneration().y + getUserData().getHeight() / 2, 0);
        }
    }

    public void moveToPosition(Vector2 position) {
        if (getUserData() != null) {
            getBody().setTransform(position.x, position.y + getUserData().getHeight() / 2, 0);
        }
    }

    public boolean isDeepFall() {
        if (getUserData() != null) {
            return getUserData().isDeepFall();
        }
        return false;
    }

    public void resetDeepFall() {
        if (getUserData() != null) {
            getUserData().setDeepFall(false);
        }
        hit();
    }

    public boolean bamAnimationIsFinish() {
        return moveDirection > 1 ? bamRight.isAnimationFinished(stateTime) : bamLeft.isAnimationFinished(stateTime);
    }

    public void setIceLevel(boolean iceLevel) {
        this.iceLevel = iceLevel;
    }

    public boolean isInBoots() {
        return inBoots;
    }

    public void setInBoots(boolean inBoots) {
        this.inBoots = inBoots;
    }

    public boolean isHasLight() {
        return hasLight;
    }

    public void setHasLight(boolean hasLight) {
        this.hasLight = hasLight;
    }

    public void eatBirdamins() {
        inBirdaminsTimeLeft = Constants.PLAYER_MAX_BIRDAMINS_TIME;
    }

    public boolean isBirdamins() {
        return inBirdaminsTimeLeft > 0;
    }

    public Vector2 getOriginCenter(){
        Vector2 vec = new Vector2();
        if (getBody() != null){
            vec.set(getBody().getPosition());
        }
        return vec;
    }
}
