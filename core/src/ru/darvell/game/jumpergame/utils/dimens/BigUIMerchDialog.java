package ru.darvell.game.jumpergame.utils.dimens;

import com.badlogic.gdx.math.Vector2;
import ru.darvell.game.jumpergame.utils.DimensForDialogWithAction;

public class BigUIMerchDialog extends DimensForDialogWithAction {

    public BigUIMerchDialog() {

        PANEL_HORIZONTAL_OFFSET = 23f;
        PANEL_VERTICAL_OFFSET = 84f;

        CELL_HORIZONTAL_OFFSET = 1f;
        CELL_VERTICAL_OFFSET = -82;

        DOOR_HORIZONTAL_OFFSET = 0;
        DOOR_VERTICAL_OFFSET = -113;

        DIALOG_CELL_DIMENS = 25f;
        DIALOG_CELL_SPACE_FOR_POUCH = 2f;
        DIALOG_CELL_SPACE_FOR_MERCH = 5f;

        CELL_MERCH_HORIZONTAL_OFFSET = 10f;
        CELL_MERCH_VERTICAL_OFFSET = 8f;

        panels.add(new PanelData(new Vector2(23f, -50f), "test"));
        panels.add(new PanelData(new Vector2(23f, -115f), "test"));
    }

}
