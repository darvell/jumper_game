package ru.darvell.game.jumpergame.entitys.interfaces;

public interface Controllable {
    void initJumping();
    void initMoveRight();
    void initMoveLeft();
    void stopMovingRight();
    void stopMovingLeft();
}
