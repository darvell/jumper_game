package ru.darvell.game.jumpergame.entitys.utils.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.entitys.Block;
import ru.darvell.game.jumpergame.entitys.interfaces.ShakeListener;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;

import static ru.darvell.game.jumpergame.entitys.utils.blocks.BlockTypes.BLOCK_MAX;
import static ru.darvell.game.jumpergame.entitys.utils.blocks.BlockTypes.BLOCK_THREE;

public abstract class FloorCreator {

    protected Array<FloorPattern> generatorPatterns = new Array<FloorPattern>();
    protected World world;
    protected Assets assets;
    protected ShakeListener shakeListener;
    protected BlockFabrick blockFabrick;

    protected int lastRndIndex = 0;
    protected float floorPosY = 0;
    protected int lastBlockId = 0;

    private Array<Block> blocks;

    public FloorCreator(World world, Assets assets) {
        this.world = world;
        this.assets = assets;
        this.blockFabrick = new BlockFabrick(world);
        blocks = new Array<Block>();
    }

    public void setShakeListener(ShakeListener shakeListener) {
        this.shakeListener = shakeListener;
    }

    abstract void initPatterns();

    public void generateLevelBlocks(ShakeListener shakeListener, float maxHeight) {
        createStartBlock();
        int countBlocks = (int) (maxHeight / (Constants.BLOCK_MIN_Y_OFFSET * Constants.WORLD_TO_SCREEN));
        countBlocks -= 1;
        for (int i = 0; i < countBlocks; i++) {
            generateFloor();
        }
        createFinishBlock();
    }

    private void generateFloor() {
        int rndIndex;
        do {
            rndIndex = MathUtils.random(0, generatorPatterns.size - 1);
        } while (
                generatorPatterns.get(rndIndex).notCorrectFloor(generatorPatterns.get(lastRndIndex).getId())
//                && lastRndIndex == rndIndex
                        && generatorPatterns.size > 1

        );
        FloorPattern pattern = generatorPatterns.get(rndIndex);
        lastRndIndex = rndIndex;

        for (ProtoBlock protoBlock : pattern.getBlocks()) {

            Block block = createBlock(protoBlock, floorPosY);
            blocks.add(block);
            block.setProtoBlock(protoBlock);

        }
        floorPosY += Constants.BLOCK_MIN_Y_OFFSET;
    }

    abstract Block createBlock(ProtoBlock protoBlock, float floorPosY);

    private Block createStartBlock() {
        Block block = createSimpleBlock(assets.getBlockTexture(BLOCK_MAX), new ProtoBlock(BLOCK_MAX, 0f));
        blocks.add(block);
        return block;
    }

    private Block createFinishBlock() {
        Block block = createSimpleBlock(assets.getBlockTexture(BLOCK_THREE), new ProtoBlock(BLOCK_THREE, 0f));
        blocks.add(block);
        return block;
    }

    private Block createSimpleBlock(TextureRegion textureRegion, ProtoBlock protoBlock) {
        Vector2 position = new Vector2(0f, floorPosY);
        Block block = blockFabrick.getSimpleBlock(position, textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), lastBlockId++, textureRegion);
        block.setProtoBlock(protoBlock);

        floorPosY += Constants.BLOCK_MIN_Y_OFFSET;
        return block;
    }

    public Array<Block> getPreparedBlocks() {
        return blocks;
    }
}
