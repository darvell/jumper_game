package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.darvell.game.jumpergame.entitys.utils.DvigClickListener;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;

public class Dvig extends SimpleActor {

    private Animation animation;
    private float stateTime = 0f;
    private float yOffset;
    private final Dvig self;

    private boolean inShowLevel = false;
    private boolean needShowBall = false;

    public Dvig(Assets assets, final DvigClickListener clickListener, boolean inSnowLevel) {
        self = this;
        animation = assets.getForAllAnimation(Assets.DVIG, 0.1f);
        setWidth(Constants.DVIG_DRAWING_WIDTH);
        setHeight(Constants.DVIG_DRAWING_HEIGHT);
        yOffset = Constants.DVIG_Y_OFFSET;
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                clickListener.dvigClicked(self);
                return true;
            }
        });
        this.inShowLevel = inSnowLevel;

    }

    public Dvig(Assets assets, final DvigClickListener clickListener) {
        this(assets, clickListener, false);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        TextureRegion textureRegion = (TextureRegion) animation.getKeyFrame(stateTime, true);

        batch.draw(textureRegion, getX(), getY(),
                getWidth() / 2, getHeight() / 2,
                getWidth(), getHeight()
                , 1, 1,
                getRotation());
    }

    @Override
    public void act(float delta) {
        stateTime += delta;

    }

    @Override
    public float getY() {
        return super.getY() + yOffset;
    }


}
