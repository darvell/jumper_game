package ru.darvell.game.jumpergame.entitys.utils.blocks;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.entitys.Block;
import ru.darvell.game.jumpergame.entitys.FallenBlock;
import ru.darvell.game.jumpergame.entitys.interfaces.ShakeListener;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;
import ru.darvell.game.jumpergame.utils.GdxLog;
import ru.darvell.game.jumpergame.utils.Levels;

public class WorldBlockMaster{

    public static final String LOG_TAG = "BLOCK_MASTER";

    private Assets assets;
    private World world;
    private Stage stage;
    private FloorCreator floorCreator;
    private ShakeListener shakeListener;
    private float maxCameraPosition;

    private Array<Block> blocks;

    public WorldBlockMaster(World world, Assets assets, Stage stage, Levels level, ShakeListener shakeListener, float maxCameraPosition) {
        long t0 = System.nanoTime();
        this.world = world;
        this.assets = assets;
        this.stage = stage;
        this.shakeListener = shakeListener;
        blocks = new Array<Block>();
        this.maxCameraPosition = maxCameraPosition;
        initFloorCreator(level);
        generateLevel();
        GdxLog.d(LOG_TAG, "Generated for : " + ((double)((System.nanoTime() - t0)) / 1000000) + "ms");
    }

    private void initFloorCreator(Levels levels){
        switch (levels){
            case LEVEL_ONE:floorCreator = new LevelOneFloorCreator(world, assets);
                break;
            case LEVEL_TWO:floorCreator = new LevelTwoFloorCreator(world, assets);
                break;
            case LEVEL_FOUR:floorCreator = new LevelFourCreator(world, assets);
                break;
            case LEVEL_FIVE:floorCreator = new LevelFiveFloorCreator(world, assets);
                break;
            case LEVEL_THREE:floorCreator = new LevelThreeFloorCreator(world, assets);
                break;
            case LEVEL_SIX:floorCreator = new LevelSixFloorCreator(world, assets);
                break;
        }
    }

    private void generateLevel(){
        floorCreator.initPatterns();
        floorCreator.setShakeListener(shakeListener);
        floorCreator.generateLevelBlocks(shakeListener, maxCameraPosition);
        blocks = floorCreator.getPreparedBlocks();
        for (Block block:blocks){
            stage.addActor(block);
        }
    }

    public Array<Block> getBlocksFromMobs() {
        Array<Block> blocksForMobs = new Array<Block>();
        for (int i = 1; i < blocks.size - 1; i++){
            Block block = blocks.get(i);
            if (block.getProtoBlock().getType() == BlockTypes.BLOCK_THREE || block.getProtoBlock().getType() == BlockTypes.BLOCK_FOUR){
                blocksForMobs.add(block);
            }
        }
        return blocksForMobs;
    }

    public Block getFirstBlock(){
        return blocks.get(0);
    }

    public Block getLastBlock(){
        return blocks.get(blocks.size - 1);
    }

    public Block getBlockById(int index){
        for (Block block: blocks){
            if (block.getId() == index){
                return block;
            }
        }
        return null;
    }

    public Block getNotFallenBlock(int fromId){
        for (int i=++fromId; i < blocks.size; i++){
            if (!(blocks.get(i) instanceof FallenBlock)){
                return blocks.get(i);
            } else {
                if (!((FallenBlock)blocks.get(i)).isReturning()){
                    return blocks.get(i);
                }
            }
        }
        return blocks.get(blocks.size - 1);
    }




    public void allBlockToFront() {
        for (Block block : blocks) {
            block.toFront();
        }
    }

    public void reactivateBlocks(float cameraTop, float cameraButton) {
        for (Block block: blocks) {
            if ((block.getCenterOrig().y <  (cameraTop + 20f) / Constants.WORLD_TO_SCREEN)
                    && (block.getCenterOrig().y >  (cameraButton - 20f) / Constants.WORLD_TO_SCREEN)) {
                block.setActive(true);
            } else {
                if (block.isActive()) {
                    block.setActive(false);
                }
            }
        }
    }
}
