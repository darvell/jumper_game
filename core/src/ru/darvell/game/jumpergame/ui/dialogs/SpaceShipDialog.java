package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.entitys.utils.SpaceShipInventory;
import ru.darvell.game.jumpergame.ui.InventoryCell;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.ui.TextPanel;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.UISpaceShipDialog;
import ru.darvell.game.jumpergame.utils.dimens.BigUISpaceShipDialog;
import ru.darvell.game.jumpergame.utils.dimens.SmallUISpaceShipDialog;

import static ru.darvell.game.jumpergame.ui.InventoryCellType.POUCH;
import static ru.darvell.game.jumpergame.ui.InventoryCellType.SHIP;


public class SpaceShipDialog extends MyDialog {


    private SpaceShipInventory spaceShipInventory;
    private TextPanel textPanelCollection;
    private TextPanel textPanelTools;
    private TextPanel textPanelInventory;
    private UIActor background;
    private Array<InventoryCell> collectionCells = new Array<InventoryCell>(5);
    private Array<InventoryCell> toolsCells = new Array<InventoryCell>(8);
    private Array<InventoryCell> pouchCells;
    private UISpaceShipDialog dimens;


    public SpaceShipDialog(Assets assets, float screenHeight, DialogListener dialogListener) {
        super(assets, screenHeight, dialogListener);
        if (screenHeight < 240f) {
            dimens = new SmallUISpaceShipDialog();
        } else {
            dimens = new BigUISpaceShipDialog();
        }
    }


    public void setSpaceShipInventory(SpaceShipInventory spaceShipInventory) {
        this.spaceShipInventory = spaceShipInventory;
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            initPanels();
            initCollectionCells();
            initToolsCells();
            initPouchCells();
            initDragAndDrop();
        } else {
            if (spaceShipInventory != null && playerPouch != null) {
                for (Item item : spaceShipInventory.getItems()) {
                    item.remove();
                }
                for (Item item : playerPouch.getItems()) {
                    item.remove();
                }
                for (InventoryCell cell : pouchCells) {
                    cell.remove();
                }
                dragAndDrop.clear();
            }
        }
        super.setVisible(visible);
    }

    private void initPanels() {
        if (textPanelCollection == null) {
            textPanelCollection = new TextPanel(assets.getUITexture(Assets.UI_TEXT_PANEL_LONG),
                    dimens.DIALOG_PANEL_HORIZONTAL_OFFSET,
                    getHeight() / 2 + dimens.DIALOG_PANEL_COLLECTION_VERTICAL_OFFSET);
            addActor(textPanelCollection);

            textPanelTools = new TextPanel(assets.getUITexture(Assets.UI_TEXT_PANEL_LONG),
                    dimens.DIALOG_PANEL_HORIZONTAL_OFFSET,
                    getHeight() / 2 + dimens.DIALOG_PANEL_TOOLS_VERTICAL_OFFSET);
            addActor(textPanelTools);

            textPanelInventory = new TextPanel(assets.getUITexture(Assets.UI_TEXT_PANEL_LONG),
                    dimens.DIALOG_PANEL_HORIZONTAL_OFFSET,
                    getHeight() / 2 + dimens.DIALOG_PANEL_INVENTORY_VERTICAL_OFFSET);
            addActor(textPanelInventory);
        }
    }


    private void initCollectionCells() {
        float startX = dimens.DIALOG_CELL_START_OFFSET;
        if (collectionCells.size == 0) {
            for (int i = 0; i < 5; i++) {
                InventoryCell cell = new InventoryCell(assets.getUITexture(Assets.UI_EMPTY_CELL), startX,
                        getHeight() / 2 + dimens.DIALOG_CELL_COLLECTION_VERTICAL_OFFSET,
                            dimens.DIALOG_CELL_DIMENS,
                        SHIP, assets.getFont(Assets.FONT_ZX_5_WHITE), this);
                collectionCells.add(cell);
                addActor(cell);
                startX += (dimens.DIALOG_CELL_DIMENS + dimens.DIALOG_CELL_SPACE);
            }
        }

        if (spaceShipInventory.getCollectionItems().get(0)) {
            collectionCells.get(0).setTextureRegion(assets.getUITexture(Assets.UI_COL_ITEM1));
        }
        if (spaceShipInventory.getCollectionItems().get(1)) {
            collectionCells.get(1).setTextureRegion(assets.getUITexture(Assets.UI_COL_ITEM2));
        }
        if (spaceShipInventory.getCollectionItems().get(2)) {
            collectionCells.get(2).setTextureRegion(assets.getUITexture(Assets.UI_COL_ITEM3));
        }
        if (spaceShipInventory.getCollectionItems().get(3)) {
            collectionCells.get(3).setTextureRegion(assets.getUITexture(Assets.UI_COL_ITEM4));
        }
        if (spaceShipInventory.getCollectionItems().get(4)) {
            collectionCells.get(4).setTextureRegion(assets.getUITexture(Assets.UI_COL_ITEM5));
        }
    }

    private void initToolsCells() {
        if (toolsCells.size == 0) {
            float startX = dimens.DIALOG_CELL_START_OFFSET;
            float posY = dimens.DIALOG_CELL_TOOLS_VERTICAL_OFFSET;
            for (int i = 0; i < 8; i++) {
                InventoryCell cell = new InventoryCell(assets.getUITexture(Assets.UI_EMPTY_CELL), startX,
                        getHeight() / 2 + posY,
                        dimens.DIALOG_CELL_DIMENS,
                        SHIP, assets.getFont(Assets.FONT_ZX_5_WHITE), this);
                toolsCells.add(cell);
                addActor(cell);
                startX += (dimens.DIALOG_CELL_DIMENS + dimens.DIALOG_CELL_SPACE);

                if (i == 4) {
                    startX = toolsCells.get(1).getX();
                    posY = dimens.DIALOG_CELL_TOOLS_VERTICAL_OFFSET_LINE_TWO;
                }
            }
        }

        fillCells(spaceShipInventory.getItems(), toolsCells, dimens.DIALOG_CELL_DIMENS);
    }

    private void initPouchCells() {
        float startX = dimens.DIALOG_CELL_START_OFFSET;
        pouchCells = new Array<InventoryCell>();
        for (int i = 0; i < playerPouch.getPouchCurrentCapacity(); i++) {
            InventoryCell cell = new InventoryCell(assets.getUITexture(Assets.UI_EMPTY_CELL), startX,
                    getHeight() / 2 + dimens.DIALOG_CELL_INVENTORY_VERTICAL_OFFSET,
                    dimens.DIALOG_CELL_DIMENS,
                    POUCH, assets.getFont(Assets.FONT_ZX_5_WHITE), this);
            pouchCells.add(cell);
            addActor(cell);
            startX += (dimens.DIALOG_CELL_DIMENS + dimens.DIALOG_CELL_SPACE);
        }

        fillCells(playerPouch.getItems(), pouchCells, dimens.DIALOG_CELL_DIMENS);
    }



    private void initDragAndDrop() {
        for (InventoryCell inventoryCell : toolsCells) {
            dragAndDrop.addSource(new MySource(inventoryCell));
            dragAndDrop.addTarget(new MyTarget(inventoryCell));
        }

        for (InventoryCell inventoryCell : pouchCells) {
            dragAndDrop.addSource(new MySource(inventoryCell));
            dragAndDrop.addTarget(new MyTarget(inventoryCell));
        }
    }



    class MyTarget extends DragAndDrop.Target {

        public MyTarget(Actor actor) {
            super(actor);
        }

        @Override
        public boolean drag(DragAndDrop.Source source, Payload payload, float x, float y, int pointer) {
            return true;
        }

        @Override
        public void drop(DragAndDrop.Source source, Payload payload, float x, float y, int pointer) {
            InventoryCell sourceInventoryCell = ((InventoryCell) source.getActor());
            InventoryCell targetInventoryCell = ((InventoryCell) getActor());
            if (targetInventoryCell.isEmpty()) {

                if (sourceInventoryCell.getCellType() == SHIP && targetInventoryCell.getCellType() == POUCH) {
                    if (playerPouch.addItem(sourceInventoryCell.getItem()) != null) {
                        spaceShipInventory.removeItem(sourceInventoryCell.getItem());
                        sourceInventoryCell.setItem(null);
                        targetInventoryCell.setItem((Item) payload.getDragActor());
                    }
                } else if (sourceInventoryCell.getCellType() == POUCH && targetInventoryCell.getCellType() == SHIP) {
                    if (sourceInventoryCell.getItem().isTool()) {
                        spaceShipInventory.addItem(sourceInventoryCell.getItem());
                        playerPouch.removeItem((Item) payload.getDragActor());
                        sourceInventoryCell.setItem(null);
                        targetInventoryCell.setItem((Item) payload.getDragActor());

                    }
                }
            }
        }
    }
}
