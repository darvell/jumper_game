package ru.darvell.game.jumpergame.entitys.utils.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import ru.darvell.game.jumpergame.entitys.FallenBlock;
import ru.darvell.game.jumpergame.box2dworld.BlockUserData;
import ru.darvell.game.jumpergame.box2dworld.utils.WorldUtils;
import ru.darvell.game.jumpergame.entitys.Block;
import ru.darvell.game.jumpergame.entitys.MoveBlock;
import ru.darvell.game.jumpergame.entitys.ShakingBlock;
import ru.darvell.game.jumpergame.entitys.interfaces.ShakeListener;
import ru.darvell.game.jumpergame.utils.Constants;

public class BlockFabrick {

    private World world;

    public BlockFabrick(World world) {
        this.world = world;
    }

    private Body prepareBody(Vector2 position, float width, int blockId) {
        calculateRightBlockPosition(position, width, Constants.BLOCK_HEIGHT);
        Body blockBody = WorldUtils.createDefaultBlock(world, position, width);
        ((BlockUserData) blockBody.getUserData()).setBlockId(blockId);
        return blockBody;
    }

    private Body prepareMoveBody(Vector2 position, float width, int blockId) {
        calculateRightBlockPosition(position, width, Constants.BLOCK_HEIGHT);
        Body blockBody = WorldUtils.createMovableBlock(world, position, width);
        ((BlockUserData) blockBody.getUserData()).setBlockId(blockId);
        return blockBody;
    }

    private Body prepareBodyWithSoul(Vector2 position, float width, int blockId) {
        calculateRightBlockPosition(position, width, Constants.BLOCK_HEIGHT);
        Body blockBody = WorldUtils.createBlockWithSoul(world, position, width);
        ((BlockUserData) blockBody.getUserData()).setBlockId(blockId);
        return blockBody;
    }

    public Block getShakedBlock(Vector2 position, float width, float height, int blockId, ShakeListener shakeListener, TextureRegion textureRegion) {

        float newBlockWidth = width / 10f;
        float newBlockHeight = height / 10f;

        Body blockBody = prepareBody(position, newBlockWidth, blockId);
        return new ShakingBlock(blockBody, newBlockHeight, textureRegion, shakeListener);
    }

    public Block getFallBlock(Vector2 position, float width, float height, int blockId, TextureRegion textureRegion){
        float newBlockWidth = width / 10f;
        float newBlockHeight = height / 10f;

        Body blockBody = prepareBody(position, newBlockWidth, blockId);
        return new FallenBlock(blockBody, newBlockHeight, textureRegion);
    }

    public Block getMoveBlock(Vector2 position, float width, float height, int blockId, TextureRegion textureRegion, float moveSpeed){
        float newBlockWidth = width / 10f;
        float newBlockHeight = height / 10f;
        Body blockBody = prepareMoveBody(position, newBlockWidth, blockId);
        return new MoveBlock(blockBody, newBlockHeight, textureRegion, moveSpeed);
    }

    public Block getSoulBlock(Vector2 position, float width, float height, int blockId, TextureRegion textureRegion){
        float newBlockWidth = width / 10f;
        float newBlockHeight = height / 10f;
        Body blockBody = prepareBodyWithSoul(position, newBlockWidth, blockId);
        return new Block(blockBody, newBlockHeight, textureRegion);
    }

    public Block getSimpleBlock(Vector2 position, float width, float height, int blockId, TextureRegion textureRegion){

        float newBlockWidth = width / 10f;
        float newBlockHeight = height / 10f;

        Body blockBody = prepareBody(position, newBlockWidth, blockId);

        return new Block(blockBody, newBlockHeight, textureRegion);
    }

    private Vector2 calculateRightBlockPosition(Vector2 position, float width, float height) {
        position.x += width / 2;
        position.y += height / 2;
        return position;
    }

}
