package ru.darvell.game.jumpergame.box2dworld.fixtures;

public enum FixtureDataType {
    PLAYER_GROUND_SENSOR,
    PLAYER_LANDING_SENSOR,
    GOBLIN_LEFT_END_GROUND_SENSOR,
    GOBLIN_RIGHT_END_GROUND_SENSOR,
    GOBLIN_GROUND_LANDED_SENSOR

}
