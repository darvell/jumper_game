package ru.darvell.game.jumpergame.utils.dimens;

import ru.darvell.game.jumpergame.utils.UISpaceShipDialog;

public class SmallUISpaceShipDialog extends UISpaceShipDialog {

    public SmallUISpaceShipDialog() {
         DIALOG_PANEL_HORIZONTAL_OFFSET = 23f;
         DIALOG_PANEL_COLLECTION_VERTICAL_OFFSET = 72f;

         DIALOG_PANEL_TOOLS_VERTICAL_OFFSET = 24f;
         DIALOG_PANEL_INVENTORY_VERTICAL_OFFSET = -55f;

         DIALOG_CELL_DIMENS = 25f;
         DIALOG_CELL_START_OFFSET = 1f;
         DIALOG_CELL_SPACE = 2f;
         DIALOG_CELL_COLLECTION_VERTICAL_OFFSET = 44f;
         DIALOG_CELL_TOOLS_VERTICAL_OFFSET = -5f;
         DIALOG_CELL_TOOLS_VERTICAL_OFFSET_LINE_TWO = -34f;
         DIALOG_CELL_INVENTORY_VERTICAL_OFFSET = -83f;
    }
}
