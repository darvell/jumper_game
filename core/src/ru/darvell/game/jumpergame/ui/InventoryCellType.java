package ru.darvell.game.jumpergame.ui;

public enum InventoryCellType {
    SHIP,
    POUCH,
    WHOLE
}
