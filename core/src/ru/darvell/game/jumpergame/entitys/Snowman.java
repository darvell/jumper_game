package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.SnowmanUserData;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;

public class Snowman extends GameActor {

    private Animation stayRightAnimation;
    private Animation stayLeftAnimation;
    private Animation fireRightAnimation;
    private Animation fireLeftAnimation;

    private boolean firing = true;
    private boolean needFire;
    private int direction = 1;
    private float stateTime;

    private boolean needShowBall = false;
    private float spawnSnowBallTimeOut = Constants.SNOWMAN_SNOWBAL_TIMEOUT_MAX;
    private float spawnSnowBallTimeOutLeft;

    private boolean fireAnimationFinish = false;

    private float fireTimeOutMax = Constants.SNOWMAN_FIRE_TIMEOUT_MAX;
    private float fireTimeoutLeft = fireTimeOutMax;

    public Snowman(Body body, Assets assets) {
        super(body);
        stayLeftAnimation = assets.getForAllAnimation(Assets.SNOWMAN_STAY_LEFT, 0.1f);
        stayRightAnimation = assets.getForAllAnimation(Assets.SNOWMAN_STAY_RIGHT, 0.1f);
        fireRightAnimation = assets.getForAllAnimation(Assets.SNOWMAN_FIRE_RIGHT, 0.1f);
        fireLeftAnimation = assets.getForAllAnimation(Assets.SNOWMAN_FIRE_LEFT, 0.1f);
        spawnSnowBallTimeOutLeft = spawnSnowBallTimeOut;

    }

    @Override
    protected SnowmanUserData getUserData() throws NullPointerException {
        return body != null ? (SnowmanUserData) body.getUserData() : null;
    }

    @Override
    public void act(float delta) {
        if (isActive()) {
            super.act(delta);
            stateTime += delta;
            if (fireTimeoutLeft < 0) {
                setNeedFire(true);
            } else {
                fireTimeoutLeft -= delta;
            }

            if (isFiring() && fireAnimationFinish) {
                setFiring(false);
                fireAnimationFinish = false;
                fireTimeoutLeft = fireTimeOutMax;
                stateTime = 0;
            }

            if (!needShowBall) {
                spawnSnowBallTimeOutLeft -= delta;
                if (spawnSnowBallTimeOutLeft <= 0) {
                    needShowBall = true;
                    spawnSnowBallTimeOutLeft = spawnSnowBallTimeOut;
                }
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        TextureRegion textureRegion;
        if (isFiring()){
            if (direction > 0) {
                textureRegion = (TextureRegion) fireRightAnimation.getKeyFrame(stateTime, false);
                fireAnimationFinish = fireRightAnimation.isAnimationFinished(stateTime);
            } else{
                textureRegion = (TextureRegion) fireLeftAnimation.getKeyFrame(stateTime, false);
                fireAnimationFinish = fireLeftAnimation.isAnimationFinished(stateTime);
            }
        } else {
            if (direction > 0) {
                textureRegion = (TextureRegion) stayRightAnimation.getKeyFrame(stateTime);
            } else{
                textureRegion = (TextureRegion) stayLeftAnimation.getKeyFrame(stateTime);
            }
        }
        float xPosition = direction<0 ? getX() - 11f: getX();
        batch.draw(textureRegion, xPosition, getY());
    }

    public boolean isFiring() {
        return firing;
    }

    public void setFiring(boolean firing) {
        this.firing = firing;
    }

    public boolean isNeedFire() {
        return needFire;
    }

    public void setNeedFire(boolean needFire) {
        if (isNeedFire() && needFire == false){
            setFiring(true);
        }
        this.needFire = needFire;
    }

    public boolean isNeedShowBall() {
        return needShowBall;
    }

    public void setNeedShowBall(boolean needShowBall) {
        this.needShowBall = needShowBall;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public Vector2 getPositionForBullet(){
        if (getUserData() != null){
            return new Vector2(
                    body.getPosition().x +((direction == 1) ? 1.6f : -1.6f)
                    ,body.getPosition().y + getUserData().getHeight() / 2 - 0.6f);
        } else {
            return new Vector2();
        }
    }
}
