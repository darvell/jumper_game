package ru.darvell.game.jumpergame.entitys.utils.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import ru.darvell.game.jumpergame.entitys.Block;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;

import static ru.darvell.game.jumpergame.entitys.utils.blocks.BlockTypes.*;

public class LevelFourCreator extends FloorCreator{

    public LevelFourCreator(World world, Assets assets) {
        super(world, assets);
    }

    @Override
    void initPatterns() {
        generatorPatterns.add(new FloorPattern(1, new ProtoBlock(BLOCK_TWO, 0.1f), new ProtoBlock(BLOCK_TWO, 5.3f), new ProtoBlock(BLOCK_TWO, 10.2f)));
        generatorPatterns.add(new FloorPattern(2, new ProtoBlock(BLOCK_TWO, 2.6f), new ProtoBlock(BLOCK_TWO, 8f)));
        generatorPatterns.add(new FloorPattern(3, new ProtoBlock(BLOCK_TWO, 0.1f), new ProtoBlock(BLOCK_TWO, 10.2f)));

        generatorPatterns.add(new FloorPattern(4, new ProtoBlock(BLOCK_THREE, 1.5f)));
        generatorPatterns.add(new FloorPattern(5, new ProtoBlock(BLOCK_THREE, 4.3f)));
        generatorPatterns.add(new FloorPattern(6, new ProtoBlock(BLOCK_THREE, 6.3f)));

        generatorPatterns.add(new FloorPattern(7, new ProtoBlock(BLOCK_THREE, 0.1f), new ProtoBlock(BLOCK_TWO, 9f)));
        generatorPatterns.add(new FloorPattern(8, new ProtoBlock(BLOCK_TWO, 2.5f), new ProtoBlock(BLOCK_THREE, 7.5f)));

        generatorPatterns.add(new FloorPattern(9, new ProtoBlock(BLOCK_FOUR, 2.5f)));

        generatorPatterns.add(new FloorPattern(10, new ProtoBlock(BLOCK_TWO, 0.1f), new ProtoBlock(BLOCK_FOUR, 5f)));
        generatorPatterns.add(new FloorPattern(11, new ProtoBlock(BLOCK_FOUR, 0.1f), new ProtoBlock(BLOCK_TWO, 10.2f)));
//        generatorPatterns.add(new FloorPattern(new ProtoBlock(BLOCK_FOUR, 0.1f, false), new ProtoBlock(BLOCK_TWO, 10.2f, true)));

        generatorPatterns.add(new FloorPattern(12, new ProtoBlock(BLOCK_TWO, 2.6f, true)));
        generatorPatterns.add(new FloorPattern(13, new ProtoBlock(BLOCK_TWO, 2.6f, true)));
        generatorPatterns.add(new FloorPattern(14, new ProtoBlock(BLOCK_TWO, 2.6f, true)));
        generatorPatterns.add(new FloorPattern(15, new ProtoBlock(BLOCK_THREE, 4.3f, true)));
        generatorPatterns.add(new FloorPattern(16, new ProtoBlock(BLOCK_THREE, 4.3f, true)));
    }

    @Override
    Block createBlock(ProtoBlock protoBlock, float floorPosY) {
        TextureRegion textureRegion = assets.getBlockTexture(protoBlock.getType());
        Vector2 position = new Vector2(protoBlock.getStartX(), floorPosY);
//        int rnd = 2;
        Block block;
        if (protoBlock.isMovable()) {
            float rnd = MathUtils.random(Constants.BLOCK_MIN_MOVE_SPEED, Constants.BLOCK_MAX_MOVE_SPEED);
            block = blockFabrick.getMoveBlock(position, textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), lastBlockId++, textureRegion, rnd);
        } else if (protoBlock.isSoul()){
            block = blockFabrick.getSoulBlock(position, textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), lastBlockId++, textureRegion);
        } else {
            block = blockFabrick.getShakedBlock(position, textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), lastBlockId++, shakeListener, textureRegion);
        }
        return block;
    }


}
