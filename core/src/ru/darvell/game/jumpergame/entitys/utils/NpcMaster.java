package ru.darvell.game.jumpergame.entitys.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.box2dworld.utils.WorldUtils;
import ru.darvell.game.jumpergame.entitys.*;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;

import java.util.Iterator;

public class NpcMaster {

    private Array<GameActor> npcs = new Array<GameActor>();
    private Array<ItemOnField> itemsOnField = new Array<ItemOnField>();
    private Array<SimpleActor> decorators = new Array<SimpleActor>();
    private Array<Pike> pikes = new Array<Pike>();
    private Array<SnowBall> snowBalls = new Array<SnowBall>();
    private Array<SnowBullet> snowBullets = new Array<SnowBullet>();
    private Array<Dvig> dvigs = new Array<Dvig>();
    private Pike pike;
    private World world;
    private Assets assets;
    private TeleportBomb teleportBomb;
    private Stage stage;

    private boolean needTeleport = false;
    private Vector2 teleportCoords;

    public NpcMaster(World world, Assets assets, Stage stage) {
        this.world = world;
        this.assets = assets;
        this.stage = stage;
    }

    public void update() {
        if (teleportBomb != null && teleportBomb.isActivated()) {
            needTeleport = true;
            teleportCoords = teleportBomb.getCenterOrig();
            teleportBomb.setNeedDelete();
        }
        for (Dvig dvig : dvigs) {

        }
        for (GameActor actor: npcs){
            if (actor instanceof Snowman){
                Snowman snowman = (Snowman)actor;
                if (snowman.isNeedFire()){
                    snowman.setNeedFire(false);
                    spawnSnowBullet(snowman);
                }
                if (snowman.isNeedShowBall()){
                    spawnSnowBall(snowman.getCenter().scl(0.1f), snowman.getDirection());
                    snowman.setNeedShowBall(false);
                }
            }
        }

    }

    public Dvig spawnDvig(Block block, DvigClickListener clickListener, boolean isSnowLevel) {
        Dvig dvig = new Dvig(assets, clickListener, isSnowLevel);
        block.getPositionForGeneration();
        dvig.setX(block.getPositionForGeneration().x * Constants.WORLD_TO_SCREEN - dvig.getWidth() / 2);
        dvig.setY(block.getPositionForGeneration().y * Constants.WORLD_TO_SCREEN);
        dvigs.add(dvig);
        return dvig;
    }


    public FlyingBottle throwBottle(Player player) {
        FlyingBottle bottle = new FlyingBottle(WorldUtils.createBottleBody(world, player.getOriginCenter()), assets);
        bottle.throwUp();
        return bottle;
    }

    public Snowman spawnSnowman(Block block){

        Snowman snowman = new Snowman(WorldUtils.createSnowmanBody(world, block.getPositionForGeneration()), assets);
        if (block.getPositionForGeneration().x > Constants.MAX_BOX2D_WORLD_WIDTH / 2){
            snowman.setDirection(-1);
        } else {
            snowman.setDirection(1);
        }
        npcs.add(snowman);
        return snowman;
    }

    public void spawnSnowmanCrack(Snowman snowman){
        SnowmanCrack snowmanCrack = new SnowmanCrack(assets, snowman.getDirection());
        snowmanCrack.setX(snowman.getX());
        snowmanCrack.setY(snowman.getY());
        stage.addActor(snowmanCrack);

    }

    public FlyingBottle throwBottle(Player player, Vector2 power) {
        FlyingBottle bottle = new FlyingBottle(WorldUtils.createBottleBody(world, player.getOriginCenter()), assets);
        bottle.throwUp(power);
        return bottle;
    }

    public TeleportBomb throwTeleportBomb(Player player, int multiplier, int direction) {
        if (teleportBomb == null) {
            teleportBomb = new TeleportBomb(WorldUtils.createTeleportBombBody(world, player.getOriginCenter()), assets);
            teleportBomb.throwUp(multiplier, direction);
            return teleportBomb;
        }
        return null;
    }

    public Pike throwPike(Player player, Vector2 point) {
        Pike pike = new Pike(assets, WorldUtils.createPikeBody(world, player.getOriginCenter()));
        pike.throwUp(point.sub(player.getOriginCenter()));
        pikes.add(pike);
        return pike;
    }

    public SnowBall spawnSnowBall(Vector2 position, int direction) {
        SnowBall snowBall = new SnowBall(WorldUtils.createSnowBallBody(world, position), assets);
        snowBall.setDirection(direction);
        snowBall.throwBall();
        snowBalls.add(snowBall);
        stage.addActor(snowBall);
        return snowBall;
    }

    public SnowBullet spawnSnowBullet(Snowman snowman){
        SnowBullet snowBullet = new SnowBullet(assets, WorldUtils.createSnowBulletBody(world, snowman.getPositionForBullet()), snowman.getDirection());
        snowBullets.add(snowBullet);
        stage.addActor(snowBullet);
        return snowBullet;
    }

    public void spawnSnowBallCrack(float xCenter, float y) {
        SnowballCrack snowballCrack = new SnowballCrack(assets);
        snowballCrack.setX(xCenter);
        snowballCrack.setY(y);
        stage.addActor(snowballCrack);
    }

    public void addGameDecorator(SimpleActor decorator) {
        decorators.add(decorator);
    }

    public ItemOnField spawnItemOnField(Block block, Item item) {
        ItemOnField itemOnField = new ItemOnField(WorldUtils.createItemOnField(world, block.getPositionForGeneration()), item);
        itemsOnField.add(itemOnField);
        return itemOnField;
    }


    public Goblin spawnGoblin(Block block) {
        Goblin goblin = new Goblin(WorldUtils.createGoblinBody(world, block.getPositionForGeneration()), assets);
        npcs.add(goblin);
        return goblin;
    }

    public void reactivateNpc(float cameraTop, float cameraButton) {
        for (GameActor npc : npcs) {
            if ((npc.getY() < cameraTop + Constants.CAMERA_ACTIVE_VIEW_OFFSET) && (npc.getY() > cameraButton - Constants.CAMERA_ACTIVE_VIEW_OFFSET)) {
                npc.setActive(true);
            } else {
                if (npc.isActive()) {
                    npc.setActive(false);
                }
            }
        }
    }

    public void doDeleteOperations(){
        deleteMarkedNpcs();
        deleteMarkedTeleportBomb();
        deleteMarkedItemsOnField();
        deleteMarkedPikes();
        deleteMarkedSnowBalls();
    }

    private boolean deleteGameActor(GameActor gameActor){
         boolean result = gameActor.isNeedDelete();
         if (result){
             gameActor.remove();
             if (gameActor.getBody() != null) {
                 world.destroyBody(gameActor.getBody());
             }
         }
         return result;
    }



    public void deleteMarkedPikes(){
        Iterator<Pike> pikeIterator = pikes.iterator();
        while (pikeIterator.hasNext()){
            Pike pike = pikeIterator.next();
            if (pike.isNeedDelete()){
                deleteGameActor(pike);
                pikeIterator.remove();
            }
        }
    }


    public void deleteMarkedSnowBalls(){
        Iterator<SnowBall> iterator = snowBalls.iterator();
        while (iterator.hasNext()){
            SnowBall snowBall = iterator.next();
            float x = snowBall.getX() - snowBall.getWidth() / 2;
            float y = snowBall.getY();
            if (snowBall.isNeedDelete()){
                deleteGameActor(snowBall);
                iterator.remove();
                spawnSnowBallCrack(x, y);
            }
        }
    }

    public void deleteMarkedTeleportBomb() {
        if (teleportBomb != null && teleportBomb.isNeedDelete()) {
            teleportBomb.remove();
            if (teleportBomb.getBody() != null) {
                world.destroyBody(teleportBomb.getBody());
            }
            teleportBomb = null;
            needTeleport = false;
        }
    }


    public void deleteMarkedNpcs() {
        Iterator<GameActor> iterator = npcs.iterator();
        while (iterator.hasNext()) {
            while (iterator.hasNext()){
                GameActor actor = iterator.next();
                if (actor.isNeedDelete()){
                    if (actor instanceof Snowman) {
                        Snowman snowman = (Snowman) actor;
                        spawnSnowmanCrack(snowman);
                        deleteGameActor(snowman);
                    }
                    iterator.remove();
                }
            }
        }
    }

    public void deleteMarkedItemsOnField() {
        Iterator<ItemOnField> iterator = itemsOnField.iterator();
        while (iterator.hasNext()) {
            ItemOnField actor = iterator.next();
            if (actor.isNeedDelete()) {
                deleteGameActor(actor);
                iterator.remove();
            }
        }
    }

    public ItemOnField findPickedItem() {
        Iterator<ItemOnField> iterator = itemsOnField.iterator();
        while (iterator.hasNext()) {
            ItemOnField itemOnField = iterator.next();
            if (itemOnField.isPickedUp()) {
                return itemOnField;
            }
        }
        return null;
    }

    public void setPickedItem(ItemOnField itemOnField, boolean picked) {
        Iterator<ItemOnField> iterator = itemsOnField.iterator();
        while (iterator.hasNext()) {
            ItemOnField myItemOnField = iterator.next();
            if (myItemOnField.equals(itemOnField)) {
                if (picked) {
                    myItemOnField.setNeedDelete();
                } else {
                    myItemOnField.setPickedUp(false);
                }
            }
        }
    }

    public boolean isNeedTeleport() {
        return needTeleport;
    }

    public Vector2 getTeleportCoords() {
        return teleportCoords;
    }
}
