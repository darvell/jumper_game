package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import ru.darvell.game.jumpergame.utils.Assets;

public class Item extends Actor implements Json.Serializable{

    static int lastItemId = 0;
    private int itemId;

    private boolean tool = false;
    private Tools toolType;
    private StoneType stoneType;
    private boolean activated;
    private int stackCount = 0;


    private TextureRegion activatedTexture;
    private TextureRegion deActivatedTexture;
    private TextureRegion simpleInventoryTexture;

    private Assets assets;

    private Item() {
        itemId = ++lastItemId;
    }

    private Item(Assets assets, Tools tool) {
        this();
        this.assets = assets;
        setTool(true);
        setToolType(tool);
        setActivated(false);
        setVisible(false);
        initTextures();
        stackCount = canStack() ? 0 : 1;
    }

    public Item(Assets assets, Tools tool, boolean activated) {
        this(assets, tool);
        setActivated(activated);
    }

    public Item(Assets assets, StoneType stoneType) {
        this();
        this.assets = assets;
        setTool(false);
        this.stoneType = stoneType;
        setActivated(false);
        setVisible(false);
        initTextures();
        stackCount = 1;
    }

    private void copy(Item item){
        toolType = item.toolType;
        stoneType = item.stoneType;
        tool = item.tool;


        activated = item.activated;
        setWidth(item.getWidth());
        setHeight(item.getHeight());
        initTextures();
    }


    private Item(Item item) {
        this();
        this.assets = item.assets;
        copy(item);
    }

    public static Item newOne(Item item) {
        Item result = new Item(item);
        if (!item.isTool()) {
            result.setCount(1);
        } else {
            result.setCount(item.canStack() ? 0 : 1);
        }
        return result;
    }

    public Item(Assets assets, Item item){
        this();
        this.assets = assets;
        copy(item);
        setVisible(false);
        setCount(item.stackCount);
    }


    public TextureRegion getTexture() {
        if (isTool()) {
            return isActivated() ? activatedTexture : deActivatedTexture;
        } else {
            return activatedTexture;
        }
    }

    public TextureRegion getTextureForDialog() {
        if (simpleInventoryTexture != null) {
            return simpleInventoryTexture;
        } else {
            return getTexture();
        }
    }

    public void setDimens(float width) {
        setX(0);
        setY(0);

        setWidth(width);
        setHeight(width);
    }

    public boolean isTool() {
        return tool;
    }


    public void setTool(boolean tool) {
        this.tool = tool;
    }

    public Tools getToolType() {
        return toolType;
    }

    public void setToolType(Tools toolType) {
        this.toolType = toolType;
    }

    public boolean isActivated() {
        if (canStack()){
            return stackCount > 0;
        } else {
            return activated;
        }
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(getTexture(), getX(), getY());
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemId=" + itemId +
                ", tool=" + tool +
                ", toolType=" + toolType +
                ", stoneType=" + stoneType +
                ", activated=" + activated +
                ", stackCount=" + stackCount +
                '}';
    }

    public int getItemId() {
        return itemId;
    }

    public void initTextures() {
        if (isTool()) {
            switch (toolType) {
                case BIRDAMET:
                    activatedTexture = assets.getUITexture(Assets.UI_ITEM_GUN_F);
                    deActivatedTexture = assets.getUITexture(Assets.UI_ITEM_GUN_E);
                    break;
                case BIRDAMINS:
                    activatedTexture = assets.getUITexture(Assets.UI_ITEM_FLASK_F);
                    deActivatedTexture = assets.getUITexture(Assets.UI_ITEM_FLASK_E);
                    break;
                case BOTTLE:
                    activatedTexture = assets.getUITexture(Assets.UI_ITEM_BOTTLE_F);
                    deActivatedTexture = assets.getUITexture(Assets.UI_ITEM_BOTTLE_E);
                    break;
                case TELEPORT:
                    activatedTexture = assets.getUITexture(Assets.UI_ITEM_RADIO_F);
                    deActivatedTexture = assets.getUITexture(Assets.UI_ITEM_RADIO_E);
                    break;
                case PIKES:
                    activatedTexture = assets.getUITexture(Assets.UI_ITEM_PIKES_F);
                    deActivatedTexture = assets.getUITexture(Assets.UI_ITEM_PIKES_E);
                    break;
                case LIGHTER:
                    activatedTexture = assets.getUITexture(Assets.UI_ITEM_TUBE_F);
                    deActivatedTexture = assets.getUITexture(Assets.UI_ITEM_TUBE_E);
                    break;
                case BOOTS:
                    activatedTexture = assets.getUITexture(Assets.UI_ITEM_BOOTS_F);
                    deActivatedTexture = assets.getUITexture(Assets.UI_ITEM_BOOTS_E);
                    break;
                case TV:
                    activatedTexture = assets.getUITexture(Assets.UI_ITEM_TV_F);
                    deActivatedTexture = assets.getUITexture(Assets.UI_ITEM_TV_E);
                    break;
            }
        } else {
            switch (stoneType) {
                case BLUE:
                    activatedTexture = assets.getUITexture(Assets.STONE_BLUE);
                    simpleInventoryTexture = assets.getUITexture(Assets.UI_ITEM_STONE_BLUE);
                    break;
                case YELLOW:
                    activatedTexture = assets.getUITexture(Assets.STONE_YELLOW);
                    simpleInventoryTexture = assets.getUITexture(Assets.UI_ITEM_STONE_YELLOW);
                    break;
            }
        }
    }


    public int plusStackCount() {
        return stackCount++;
    }

    public void setCount(int count) {
        stackCount = count;
    }

    public int minusStackCount() {
        return stackCount--;
    }

    public void use(){
        if (canStack()){
            minusStackCount();
        }
    }

    public boolean canUse(){
        if (canStack()) {
            return stackCount > 0;
        }
        return false;
    }

    public boolean isCapableToStack(Item item) {
        if (item.isTool() && isTool() && (item.toolType == toolType)) {
            return true;
        }
        if (!item.isTool() && !isTool() && (item.stoneType == stoneType)) {
            return true;
        }
        return false;
    }

    public int getStackCount() {
        return stackCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Item)) return false;
        Item objItem = (Item) obj;
        if (objItem.getItemId() == this.itemId) {
            return true;
        }
        return false;
    }

    public StoneType getStoneType() {
        return stoneType;
    }

    public boolean canStack() {
        if (isTool()) {
            return  (toolType.equals(Tools.BOTTLE)
                    || toolType.equals(Tools.PIKES)
                    || toolType.equals(Tools.BIRDAMINS)
                    || toolType.equals(Tools.BIRDAMET)

            );
        } else {
            return true;
        }
    }


    @Override
    public void write(Json json) {
        json.writeValue("itemId", itemId);
        json.writeValue("toolType", toolType != null ? toolType.ordinal() : -1);
        json.writeValue("tool", tool);
        json.writeValue("stoneType", stoneType != null ? stoneType.ordinal() : -1);
        json.writeValue("stackCount", stackCount);
        json.writeValue("activated", activated);

    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        itemId = jsonData.get("itemId").asInt();
        toolType = jsonData.get("toolType").asInt() != -1 ? Tools.values()[jsonData.get("toolType").asInt()] : null;
        tool = jsonData.get("tool").asBoolean();
        stoneType = jsonData.get("stoneType").asInt() != -1 ? StoneType.values()[jsonData.get("stoneType").asInt()] : null;
        stackCount = jsonData.get("stackCount").asInt();
        activated = jsonData.get("activated").asBoolean();

    }
}

