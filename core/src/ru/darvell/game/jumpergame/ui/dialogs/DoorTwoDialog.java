package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.ui.InventoryCell;
import ru.darvell.game.jumpergame.utils.Assets;

public class DoorTwoDialog extends DoorOneDialog {

    public DoorTwoDialog(float screenHeight, Assets assets, final DialogListener listener) {
        super(screenHeight, assets, listener);
    }

    @Override
    protected void initDoor() {
        super.initDoor();
        door.setTextureRegion(assets.getUITexture(Assets.UI_DIALOG_DOOR_TWO));
        door.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                doDoorAction();
                return true;
            }
        });
    }

    protected void initDoorWholes() {
        doorWholes = new Array<InventoryCell>();
        genWhole(12f, 23f);
        genWhole(12f, 71f);
        genWhole(54f, 108f);
        genWhole(98f, 71f);
        genWhole(98f, 23f);
    }

    private void doDoorAction() {
        int stoneCount = 0;
        for (InventoryCell whole : doorWholes) {
            if (whole.getItem() != null){
                stoneCount++;
            }
        }
        if (stoneCount>0){
            myDialogListener.initChangeLevel(stoneCount);
        }
    }


}
