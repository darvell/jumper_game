package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

public class PikeUserData extends UserData {

    public float velocity;

    public PikeUserData() {
        userDataType = UserDataType.PIKE;
        width = Constants.PIKE_WIDTH;
        height = Constants.PIKE_HEIGHT;
        velocity = Constants.PIKE_VELOCITY;
    }
}
