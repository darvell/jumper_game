package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class MoveBlock extends Block{

    private float moveSpeed;
    private int direction = -1;
    private float actionTimeout = 0;

    public MoveBlock(Body body, float drawingHeight, TextureRegion texture, float moveSpeed) {
        super(body, drawingHeight, texture);
        this.moveSpeed = moveSpeed;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (getUserData() != null && isActive()){

            if (actionTimeout <= 0){
                getUserData().setAiOnPause(false);
            }
            if (getUserData().isCollideWithBlock()){
                getUserData().setAiOnPause(true);
                changeDirection();
                getUserData().setCollideWithBlock(false);
                actionTimeout = 1f;

            }
            body.setLinearVelocity(new Vector2(direction * moveSpeed, 0));
            actionTimeout -= delta;
        } else if (getUserData() != null && !isActive()) {
            body.setLinearVelocity(new Vector2(0, 0));
        }
    }

    private void changeDirection(){
        direction *= -1;
    }

}
