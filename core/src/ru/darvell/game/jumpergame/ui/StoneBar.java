package ru.darvell.game.jumpergame.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ru.darvell.game.jumpergame.utils.Assets;

public class StoneBar extends Actor {

    private TextureRegion emptyBar;
    private TextureRegion firstItem;
    private TextureRegion middleItem;
    private TextureRegion lastItem;
    private StoneBarType type;

    int stoneCount = 0;
    int maxStoneCount = 10;

    public StoneBar(Assets assets, StoneBarType type){
        this.type = type;
        emptyBar = assets.getUITexture(Assets.UI_EMPTY_STONE_BAR);
        if (type.equals(StoneBarType.RED)){
            firstItem = assets.getUITexture(Assets.UI_STONE_BAR_ITEM_RED_FIRST);
            middleItem = assets.getUITexture(Assets.UI_STONE_BAR_ITEM_RED_MIDDLE);
            lastItem = assets.getUITexture(Assets.UI_STONE_BAR_ITEM_RED_LAST);
        } else {
            firstItem = assets.getUITexture(Assets.UI_STONE_BAR_ITEM_BLUE_FIRST);
            middleItem = assets.getUITexture(Assets.UI_STONE_BAR_ITEM_BLUE_MIDDLE);
            lastItem = assets.getUITexture(Assets.UI_STONE_BAR_ITEM_BLUE_LAST);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(emptyBar, getX(), getY());

        if (stoneCount > 0) {
            float offset = 1;
            for (int i = 0; i < stoneCount; i++) {
                if (i == 0) {
                    batch.draw(firstItem, getX(), getY() + offset);
                } else if (i == 9) {
                    batch.draw(lastItem, getX(), getY() + offset);
                } else {
                    batch.draw(middleItem, getX(), getY() + offset);
                }
                offset += firstItem.getRegionHeight() + 1;
            }
        }
    }

    public boolean addStone(){
        if (stoneCount < maxStoneCount){
            stoneCount++;
            return true;
        } else {
            return false;
        }
    }

    public void minusStones(int minusCount) {
        if (minusCount >= stoneCount) {
            stoneCount = 0;
        } else {
            stoneCount -= minusCount;
        }

    }

    public int getStoneCount(){
        return stoneCount;
    }


    public enum StoneBarType {
        RED,
        BLUE
    }

}
