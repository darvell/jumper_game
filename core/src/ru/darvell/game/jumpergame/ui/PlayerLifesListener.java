package ru.darvell.game.jumpergame.ui;

public interface PlayerLifesListener {

    void updateLifeCount(int lifeCount);
}
