package ru.darvell.game.jumpergame.utils.dimens;

import ru.darvell.game.jumpergame.utils.DimensForDialogWithAction;

public class BigUIPouchDialog extends DimensForDialogWithAction {

    public BigUIPouchDialog() {
        PANEL_HORIZONTAL_OFFSET = 23f;
        PANEL_VERTICAL_OFFSET = 71f;

        CELL_HORIZONTAL_OFFSET = 1f;
        CELL_VERTICAL_OFFSET = 39f;

        DIALOG_CELL_DIMENS = 25f;
        DIALOG_CELL_SPACE_FOR_POUCH = 2f;

        BUCKET_HORIZONTAL_OFFSET = 20f;
        BUCKET_VERTICAL_OFFSET = -109f;

    }
}
