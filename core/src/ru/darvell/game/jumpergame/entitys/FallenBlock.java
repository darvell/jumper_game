package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.entitys.ai.FallenBlockState;
import ru.darvell.game.jumpergame.utils.Constants;

public class FallenBlock extends Block {

    private float shakeVelocity = 3f;
    private float fallVelocity = 15f;
    private float shakeDirection = -1f;
    private boolean returning = false;
    private float fallTimeOut = 0f;
    private boolean active = true;

    private Vector2 returnDirection;
    private float delta;

    public StateMachine<FallenBlock, FallenBlockState> stateMachine;

    public FallenBlock(Body body, float drawingHeight, TextureRegion texture) {
        super(body, drawingHeight, texture);
        stateMachine = new DefaultStateMachine<FallenBlock, FallenBlockState>(this, FallenBlockState.NOTHING);
        resetFallTimeOut();
        delta = 0;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (isActive()) {
            if (getUserData() != null) {
                this.delta = delta;
                stateMachine.update();
            }
        }
    }

    public boolean fallTimeOutIsOver(){
        return fallTimeOut <= 0;
    }

    public boolean shakeTimeOutIsOver(){
        return getUserData().getShakeTime() <= 0;
    }

    public void updateFallTimeout(){
        fallTimeOut -= delta;
    }

    public void updateShake(){
        body.setLinearVelocity(shakeVelocity * shakeDirection, 0);
        if (body.getPosition().x < originX - 0.08f) {
            shakeDirection = 1;
        } else if (body.getPosition().x > originX + 0.08f) {
            shakeDirection = -1;
        }
        getUserData().setShakeTime(getUserData().getShakeTime() - delta);
    }

    public void doFalling(){
        body.setLinearVelocity(0, -fallVelocity);
    }

    public boolean checkFallIsOver(){
        if (body.getPosition().y < originY - 5f) {
            body.setLinearVelocity(0, 0);
            getUserData().setHided(true);
            returnDirection = new Vector2(originX - body.getPosition().x, originY - body.getPosition().y);
            returnDirection.scl(0.5f);
            return true;
        }
        return false;
    }

    public boolean checkAndUpdateReturningIsOver(){
        if (body.getPosition().y < originY) {
            body.setLinearVelocity(returnDirection);
            return false;
        } else {
            body.setLinearVelocity(0, 0);
            getUserData().setHided(false);
            return true;
        }
    }

    public void resetFallTimeOut(){
        fallTimeOut = MathUtils.random(Constants.BLOCK_FALL_TIMEOUT - 2f, Constants.BLOCK_FALL_TIMEOUT);
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    public void initShake() {
        if (getUserData() != null) {
            getUserData().setShakeTime(0.1f);
            getUserData().setShaked(true);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (getUserData() != null && !getUserData().isHided()) {
            super.draw(batch, parentAlpha);
        }
    }

    public boolean isReturning() {
        return returning;
    }
}
