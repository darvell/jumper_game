package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.entitys.utils.PlayerPouch;
import ru.darvell.game.jumpergame.ui.InventoryCell;
import ru.darvell.game.jumpergame.ui.InventoryCell.InventoryCellListener;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;

public class MyDialog extends Group implements InventoryCellListener {

    protected final Assets assets;
    protected final UIActor background;
    protected final DragAndDrop dragAndDrop;
    protected final float screenHeight;
    protected final DialogListener myDialogListener;
    protected PlayerPouch playerPouch;

    public MyDialog(Assets assets, float screenHeight, DialogListener dialogListener) {
        this.screenHeight = screenHeight;
        this.assets = assets;
        this.myDialogListener = dialogListener;
        setX(0);
        setY(0);
        setHeight(screenHeight);

        background = new UIActor(assets.getUITexture(Assets.UI_GRAY_BACKGROUND));
        background.setHeight(getHeight());
        addActor(background);

        final MyDialog dialog = this;
        background.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                setVisible(false);
                myDialogListener.dialogClose(dialog);
                return true;
            }

        });

        dragAndDrop = new DragAndDrop();
    }

    public void setPouch(PlayerPouch pouch) {
        this.playerPouch = pouch;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    protected void fillCells(Array<Item> items, Array<InventoryCell> cells, float cellWidth) {
        for (InventoryCell cell : cells) {
            cell.setItem(null);
        }

        int i = 0;
        for (Item item : items) {
            cells.get(i).setItem(item);
            item.setDimens(cellWidth);
            addActor(item);
            i++;
        }
    }

    @Override
    public void longClick(Item item) {
        myDialogListener.longPress(item);
    }

    class MySource extends DragAndDrop.Source {

        final DragAndDrop.Payload payload = new DragAndDrop.Payload();
        public boolean plusWnenReset = false;

        public MySource(Actor actor) {
            super(actor);
        }

        @Override
        public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
            InventoryCell inventoryCell = ((InventoryCell) getActor());
            inventoryCell.setDragging(true);
            System.out.println("START DRAGGING");
            if (!inventoryCell.isEmpty()) {
                Item item = inventoryCell.getItem();
                item.setVisible(true);
                payload.setDragActor(item);
                return payload;
            }
            return null;
        }

        @Override
        public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload, DragAndDrop.Target target) {
            ((InventoryCell) getActor()).setDragging(false);
            if (payload.getDragActor() != null) {
                (payload.getDragActor()).setVisible(false);
            }
        }

    }
}
