package ru.darvell.game.jumpergame.entitys.utils;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.SaverListener;

public class PlayerPouch implements Json.Serializable{

    private Array<Item> items = new Array<Item>();
    private int pouchCurrentCapacity = 5;
    private int pouchMaxCapacity = 5;
    private SaverListener saverListener;

    public PlayerPouch(SaverListener saverListener) {
        this.saverListener = saverListener;
    }

    public PlayerPouch(PlayerPouch playerPouch, Assets assets) {
        for (Item item:playerPouch.items){
            items.add(new Item(assets, item));
        }
    }

    void initTestValues() {

    }

    public PlayerPouch() {
    }

    public void setSaverListener(SaverListener saverListener) {
        this.saverListener = saverListener;
    }

    public Array<Item> getItems() {
        return items;
    }

    public void setItems(Array<Item> items) {
        this.items = items;
    }

    public int getPouchCurrentCapacity() {
        return pouchCurrentCapacity;
    }

    public void setPouchCurrentCapacity(int pouchCurrentCapacity) {
        this.pouchCurrentCapacity = pouchCurrentCapacity;
    }

    public int getPouchMaxCapacity() {
        return pouchMaxCapacity;
    }

    public void setPouchMaxCapacity(int pouchMaxCapacity) {
        this.pouchMaxCapacity = pouchMaxCapacity;
    }

    public void save(){
        this.saverListener.needSave();
    }

    public Item addItem(Item item) {
        for (Item i: items){
            if (i.isCapableToStack(item)){
                i.plusStackCount();
                this.saverListener.needSave();
                return i;
            }
        }
        if (items.size < pouchCurrentCapacity) {
            items.add(item);
            this.saverListener.needSave();
            return item;
        } else {
            return null;
        }

    }

    public boolean removeItem(Item item) {
        boolean result = items.removeValue(item, false);
        saverListener.needSave();
        return result;
    }

    public boolean isHasFreeSpace() {
        return (pouchCurrentCapacity - items.size) > 0;
    }

    @Override
    public String toString() {
        return "PlayerPouch{" +
                "items=" + items +
                ", pouchCurrentCapacity=" + pouchCurrentCapacity +
                ", pouchMaxCapacity=" + pouchMaxCapacity +
                '}';
    }

    @Override
    public void write(Json json) {
        json.writeObjectStart("PlayerPouch");
        json.writeValue("items", items);
        json.writeObjectEnd();
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        for (JsonValue jsonValue : jsonData.get("PlayerPouch").get("items")){
            items.add(json.fromJson(Item.class, jsonValue.toString()));
        }
    }
}
