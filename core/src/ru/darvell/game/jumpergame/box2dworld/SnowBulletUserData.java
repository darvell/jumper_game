package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

public class SnowBulletUserData extends UserData {

    private float speed;

    public SnowBulletUserData(){
        userDataType = UserDataType.SNOWBULLET;
        width = Constants.SNOWBALL_DIMENS;
        height = width;
        speed = Constants.SNOW_BULLET_SPEED;
    }

    public float getSpeed() {
        return speed;
    }
}
