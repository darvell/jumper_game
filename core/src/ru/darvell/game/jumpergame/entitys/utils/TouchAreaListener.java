package ru.darvell.game.jumpergame.entitys.utils;

public interface TouchAreaListener {
    void touchDownArea(float x, float y);
    void touchUpArea(float x, float y);
}
