package ru.darvell.game.jumpergame.entitys.staticactors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BackgroundLayer extends Actor {

    Texture[] textures;

    public BackgroundLayer(float x, float y, Texture[] textures) {
        setX(x);
        setY(y);
        this.textures = textures;

//        texture = Assets.getTextureFromAtlas(Assets.BLOCK_TWO);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        float newY = getY();
        for (int i=0; i<textures.length; i++){
            if (textures[i] != null) {
                batch.draw(textures[i], getX(), newY, textures[i].getWidth(), textures[i].getHeight());
                newY += textures[i].getHeight();
            }
        }

//        batch.draw(texturePart1, getX(), getY(), getWidth(), getHeight());
//        batch.draw(texturePart2, getX(), getY() + getHeight(), getWidth(), getHeight());
    }
}
