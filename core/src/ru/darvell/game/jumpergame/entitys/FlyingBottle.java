package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.FlyingBottleUserData;
import ru.darvell.game.jumpergame.utils.Assets;

public class FlyingBottle extends GameActor {

    private TextureRegion textureRegion;
    private static int direction = 1;

    public FlyingBottle(Body body, Assets assets) {
        super(body);
        textureRegion = assets.getUITexture(Assets.UI_BOTTLE);
    }

    @Override
    protected FlyingBottleUserData getUserData() throws NullPointerException {
        return body != null ? (FlyingBottleUserData) body.getUserData() : null;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (body != null){
            batch.draw(textureRegion, getX(), getY(),
                    getWidth() / 2, getHeight() / 2,
                    getWidth(), getHeight()
                    , 1, 1,
                    body.getAngle() * 100);
        }
    }

    public void throwUp() {
        if (body != null){
            direction *= -1;
            throwUp(new Vector2(direction, 10f));
        }
    }

    public void throwUp(Vector2 power) {
        if (body != null){
            Vector2 v2Center = new Vector2(body.getWorldCenter());
            body.applyLinearImpulse(power, v2Center, true);
            body.applyAngularImpulse(1f, true);
        }
    }
}
