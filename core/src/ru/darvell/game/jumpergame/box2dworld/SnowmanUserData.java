package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

public class SnowmanUserData extends UserData{

    public SnowmanUserData(){
        userDataType = UserDataType.SNOWMAN;
        width = Constants.SNOWMAN_WIDTH;
        height = Constants.SNOWMAN_HEIGHT;
//        yOffsetForDrawing = Constants.SNOWMAN_Y_OFFSET;
    }
}
