package ru.darvell.game.jumpergame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import ru.darvell.game.jumpergame.JumperGame;
import ru.darvell.game.jumpergame.light.Light;
import ru.darvell.game.jumpergame.light.LightManager;
import ru.darvell.game.jumpergame.stages.GamePlayStage;
import ru.darvell.game.jumpergame.stages.UIStage;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;
import ru.darvell.game.jumpergame.utils.Levels;
import ru.darvell.game.jumpergame.utils.Saver;

public class GamePlayScreen implements Screen, ParentScreen {

    private final LightManager lightsManager;
    private final Light light;
    private GamePlayStage gamePlayStage;
    private JumperGame jumperGame;
    private UIStage uiStage;
    private float calculatedViewPortHeight;
    private Assets assets;
    private Levels currentLevel;
    private Saver saver;

    public GamePlayScreen(Assets assets, Levels level, Saver saver, JumperGame jumperGame) {
        this.assets = assets;
        this.jumperGame = jumperGame;
        this.currentLevel = level;
        this.saver = saver;
        light = new Light(0, 0, 50);
        lightsManager = new LightManager();
        lightsManager.addLight(light);
        calculatedViewPortHeight = calculateViewportHeight(Constants.VIEWPORT_RELEASE_WIDTH);

        prepareStage();
    }

    public void prepareStage() {

        gamePlayStage = new GamePlayStage(false, Constants.VIEWPORT_RELEASE_WIDTH, calculatedViewPortHeight, assets, this);
        uiStage = new UIStage(Constants.VIEWPORT_RELEASE_WIDTH, calculatedViewPortHeight, assets, this, gamePlayStage.getControllable());
        resetInput();


    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        gamePlayStage.act(delta);
        uiStage.act(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (currentLevel == Levels.LEVEL_TWO) {
            Batch spriteBatch = gamePlayStage.getBatch();
            light.update(gamePlayStage.getPlayerPosition());
            lightsManager.render(spriteBatch, Gdx.graphics.getDeltaTime(), getGamePlayStage().playerHasLight());
            spriteBatch.end();
        }

        gamePlayStage.draw();
        uiStage.draw();


//        batch.begin();
//        batch.draw(img, 0, 0);
//        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        calculatedViewPortHeight = calculateViewportHeight(Constants.VIEWPORT_RELEASE_WIDTH);
        gamePlayStage.getViewport().update(width, height);
        uiStage.getViewport().update(width, height);
        lightsManager.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }

    float calculateViewportHeight(float width) {
        int displayWidth = Gdx.graphics.getWidth();
        int displayHeight = Gdx.graphics.getHeight();
        return (displayHeight * width) / displayWidth;
    }

    @Override
    public UIStage getUIStage() {
        return uiStage;
    }

    @Override
    public GamePlayStage getGamePlayStage() {
        return gamePlayStage;
    }

    @Override
    public void reset() {
        gamePlayStage.dispose();
        prepareStage();
    }

    @Override
    public Levels gerCurrentLevel() {
        return currentLevel;
    }

    @Override
    public void changeLevel(Levels level) {
        jumperGame.changeLevel(level);
    }

    @Override
    public void changeInputToUIOnly() {
        InputMultiplexer inputMultiplexerAll = new InputMultiplexer();
        inputMultiplexerAll.addProcessor(uiStage);

        Gdx.input.setInputProcessor(inputMultiplexerAll);
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void resetInput() {
        InputMultiplexer inputMultiplexerAll = new InputMultiplexer();
        inputMultiplexerAll.addProcessor(uiStage);
        inputMultiplexerAll.addProcessor(gamePlayStage);
        inputMultiplexerAll.addProcessor(gamePlayStage.getInputProcessor());

        Gdx.input.setInputProcessor(inputMultiplexerAll);
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public Saver getSaver() {
        return saver;
    }


}
