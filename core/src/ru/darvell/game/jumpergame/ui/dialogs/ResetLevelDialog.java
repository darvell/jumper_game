package ru.darvell.game.jumpergame.ui.dialogs;

import ru.darvell.game.jumpergame.ui.ButtonClickListener;
import ru.darvell.game.jumpergame.ui.ButtonFabrick;
import ru.darvell.game.jumpergame.ui.ButtonType;
import ru.darvell.game.jumpergame.ui.MyButton;
import ru.darvell.game.jumpergame.utils.Assets;

import static ru.darvell.game.jumpergame.ui.ButtonType.RESET_DIALOG_NEW_GAME;
import static ru.darvell.game.jumpergame.ui.ButtonType.RESET_DIALOG_TO_MENU;

public class ResetLevelDialog extends MyDialog implements ButtonClickListener {

    private MyButton leftPaper;
    private MyButton rightPaper;
    private DialogListener dialogListener;

    public ResetLevelDialog(float screenHeight, Assets assets, ButtonFabrick buttonFabrick, DialogListener dialogListener) {
        super(assets, screenHeight, dialogListener);

        this.dialogListener = dialogListener;

        leftPaper = buttonFabrick.getButton(RESET_DIALOG_NEW_GAME, this);
        rightPaper = buttonFabrick.getButton(RESET_DIALOG_TO_MENU, this);

        addActor(leftPaper);
        addActor(rightPaper);
        leftPaper.toFront();

        leftPaper.setY(getHeight() / 2);
        rightPaper.setY(getHeight() / 2);

        leftPaper.setX(14f);
        rightPaper.setX(76f);
    }

    @Override
    public void onButtonClick(ButtonType buttonType) {
        switch (buttonType){
            case RESET_DIALOG_TO_MENU: dialogListener.returnToMainMenu();
                break;
            case RESET_DIALOG_NEW_GAME: dialogListener.doResetGame();
                break;
        }
    }

    @Override
    public void onButtonUp(ButtonType buttonType) {

    }

}
