package ru.darvell.game.jumpergame.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import ru.darvell.game.jumpergame.utils.Assets;

public class ProgressBar extends Group {

    private int maxProgress = 19;
    private int currProgress = 1;
    private float stepTime = 0;
    private float maxStepTime = 0.02f;

    private int direction = 1;

    private boolean active = false;

    private UIActor grid;
    private UIActor fill;

    public ProgressBar(Assets assets){
        fill = new UIActor(assets.getUITexture(Assets.UI_PROGRESS_BAR_FILL));
        fill.setX(1);
        fill.setY(1);
        grid = new UIActor(assets.getUITexture(Assets.UI_PROGRESS_BAR));
        addActor(fill);
        addActor(grid);
    }

    public void reset(){
        currProgress = 1;
        direction = 1;
        active = true;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        fill.setHeight(currProgress);
        super.draw(batch, parentAlpha);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (active) {
            stepTime += delta;
            if (stepTime >= maxStepTime) {
                stepTime = 0;
                currProgress += direction;
                if (currProgress == maxProgress) {
                    currProgress = 18;
                    direction = -1;
                } else if (currProgress == 0) {
                    currProgress = 1;
                    direction = 1;
                }
            }
        }
    }

    public int stop(){
        active = false;
        return currProgress;
    }
}
