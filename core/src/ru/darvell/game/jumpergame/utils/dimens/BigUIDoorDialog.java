package ru.darvell.game.jumpergame.utils.dimens;

public class BigUIDoorDialog extends BigUIPouchDialog{

    public BigUIDoorDialog() {

        PANEL_HORIZONTAL_OFFSET = 23f;
        PANEL_VERTICAL_OFFSET = 84f;

        CELL_HORIZONTAL_OFFSET = 1f;
        CELL_VERTICAL_OFFSET = 52f;

        DOOR_HORIZONTAL_OFFSET = 0;
        DOOR_VERTICAL_OFFSET = -113;
    }

}
