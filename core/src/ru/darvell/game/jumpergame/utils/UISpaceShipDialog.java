package ru.darvell.game.jumpergame.utils;

public class UISpaceShipDialog {

    public float DIALOG_PANEL_HORIZONTAL_OFFSET = 23f;
    public float DIALOG_PANEL_COLLECTION_VERTICAL_OFFSET = 96f;

    public float DIALOG_PANEL_TOOLS_VERTICAL_OFFSET = 34f;
    public float DIALOG_PANEL_INVENTORY_VERTICAL_OFFSET = -65f;

    public float DIALOG_CELL_DIMENS = 25f;
    public float DIALOG_CELL_START_OFFSET = 1f;
    public float DIALOG_CELL_SPACE = 2f;
    public float DIALOG_CELL_COLLECTION_VERTICAL_OFFSET = 64f;
    public float DIALOG_CELL_TOOLS_VERTICAL_OFFSET = 2f;
    public float DIALOG_CELL_TOOLS_VERTICAL_OFFSET_LINE_TWO = -30f;
    public float DIALOG_CELL_INVENTORY_VERTICAL_OFFSET = -97f;
}
