package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.darvell.game.jumpergame.entitys.utils.TouchAreaListener;

public class TouchArea extends SimpleActor {

    private float yOffset;

    public TouchArea(float width, float height, float yOffset, final TouchAreaListener listener) {
        this.yOffset = yOffset;
        setX(0);
        setY(yOffset);
        setWidth(width);
        setHeight(height);
        setVisible(false);

        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                listener.touchDownArea(x, y);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                listener.touchUpArea(x, y);
            }
        });
    }

    public void moveByHorizontal(float newYPos) {
        setY(newYPos + yOffset);
    }
}
