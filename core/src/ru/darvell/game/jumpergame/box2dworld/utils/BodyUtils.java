package ru.darvell.game.jumpergame.box2dworld.utils;

import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.UserData;
import ru.darvell.game.jumpergame.box2dworld.UserDataType;

public class BodyUtils {

    public static boolean bodyIsPlayer(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.PLAYER;
    }

    public static boolean bodyIsSoulBlock(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.BLOCK_SOUL ;
    }

    public static boolean bodyIsMoveBlock(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.BLOCK_MOVE ;
    }

    public static boolean bodyIsBlock(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && (userData.getUserDataType() == UserDataType.BLOCK || bodyIsMoveBlock(body) || bodyIsSoulBlock(body));
    }

    public static boolean bodyIsGoblin(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.GOBLIN;
    }

    public static boolean bodyIsFallSensor(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.FALL_SENSOR;
    }

    public static boolean bodyIsStormSensor(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.STORM_WAVE;
    }

    public static boolean bodyIsSideSensor(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.SIDE_SENSOR;
    }

    public static boolean bodyIsItemOnField(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.ITEM;
    }

    public static boolean bodyIsFlyingBottle(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.FLYING_BOTTLE;
    }

    public static boolean bodyIsTeleportBomb(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.TELEPORT_BOMB;
    }

    public static boolean bodyIsPike(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.PIKE;
    }

    public static boolean bodyIsSnowBall(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.SNOWBALL;
    }

    public static boolean bodyIsSnowman(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.SNOWMAN;
    }

    public static boolean bodyIsSnowBullet(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.SNOWBULLET;
    }

}
