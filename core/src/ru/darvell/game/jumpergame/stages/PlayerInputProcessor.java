package ru.darvell.game.jumpergame.stages;

import com.badlogic.gdx.InputProcessor;
import ru.darvell.game.jumpergame.entitys.Goblin;
import ru.darvell.game.jumpergame.entitys.Player;

import static com.badlogic.gdx.Input.Keys.*;

public class PlayerInputProcessor implements InputProcessor {

    public static final String LOG_TAG = "PlayerInputProcessor";
    private Player player;
    private Goblin goblin;

    public PlayerInputProcessor() {
    }

    public PlayerInputProcessor(Player player) {
        this.player = player;
    }

    public PlayerInputProcessor(Player player, Goblin goblin) {
        this.player = player;
        this.goblin = goblin;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case SPACE:
                player.initJumping();
//                GdxLog.d(LOG_TAG, "SPACE pressed");
                return true;
            case RIGHT:
                player.initMoveRight();
                return true;
            case LEFT:
                player.initMoveLeft();
                return true;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case RIGHT:
                player.stopMovingRight();
                return true;
            case LEFT:
                player.stopMovingLeft();
                return true;
            case L:
                if (goblin != null) {
                    goblin.initSuperJump();
                }
                return true;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
//        if (screenX < Gdx.graphics.getWidth() / 3) {
//            player.initMoveLeft();
//        } else if ((screenX > Gdx.graphics.getWidth() / 3) && (screenX < Gdx.graphics.getWidth() / 3 * 2)) {
//            player.initJumping();
//        } else {
//            player.initMoveRight();
//        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
//        if (screenX < Gdx.graphics.getWidth() / 3) {
//            player.stopMovingLeft();
//        } else if ((screenX > Gdx.graphics.getWidth() / 3) && (screenX < Gdx.graphics.getWidth() / 3 * 2)) {
////            player.jump();
//        } else {
//            player.stopMovingRight();
//        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
