package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.darvell.game.jumpergame.entitys.Tools;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;

public class MerchButton extends UIActor {

    private final MerchButton self;
    private TextureRegion activeTexture;
    private Tools tool;
    private boolean active;

    public MerchButton(Tools tool, Assets assets, final MerchButtonListener listener) {
        self = this;
        this.tool = tool;
        setTextureRegion(assets.merchButtonTextureByTool(tool, false));
        activeTexture = assets.merchButtonTextureByTool(tool, true);
        active = false;

        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                active = !active;
                listener.touchButton(self);
                return true;
            }
        });
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (isActive()) {
            batch.draw(activeTexture, getX(), getY());
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof MerchButton)) return false;
        MerchButton objButton = (MerchButton) obj;
        if (objButton.tool.equals(this.tool)) {
            return true;
        }
        return false;
    }

    public Tools getTool() {
        return tool;
    }
}
