package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.BlockUserData;
import ru.darvell.game.jumpergame.entitys.utils.blocks.ProtoBlock;
import ru.darvell.game.jumpergame.utils.Constants;

public class Block extends GameActor {

    public static final String LOG_TAG = "Block";
    protected TextureRegion texture;
    private float drawingHeight;

    protected float originY;
    protected float originX;

    private ProtoBlock protoBlock;


    public Block(Body body, float drawingHeight, TextureRegion texture) {
        super(body);
        this.texture = texture;
        this.drawingHeight = drawingHeight;
        updatePosition();
        originX = body.getPosition().x;
        originY = body.getPosition().y;
    }

    @Override
    protected BlockUserData getUserData() throws NullPointerException {
        return body != null ? (BlockUserData) body.getUserData() : null;
    }

    public Vector2 getPositionForGeneration() {
        if (body != null) {
            Vector2 position = body.getPosition();
            position.set(position.x, position.y + getUserData().getHeight() / 2);
            return position;
        }
        return null;
    }

    public ProtoBlock getProtoBlock() {
        return protoBlock;
    }

    public void setProtoBlock(ProtoBlock protoBlock) {
        this.protoBlock = protoBlock;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY() - (transformToScreen(drawingHeight) - getHeight()), getWidth(), transformToScreen(drawingHeight));
//        GdxLog.d(LOG_TAG, String.format(Locale.US, "Block Height is %f", getWidth()));
    }

    public float getUp() {
        return transformToScreen(originY + Constants.BLOCK_HEIGHT / 2);
    }

    public int getId() {
        return getUserData() != null ? getUserData().getBlockId() : -1;
    }

    @Override
    protected void updatePosition() {
        Vector2 vel = body.getLinearVelocity();
        vel.y = 0;
        body.setLinearVelocity(vel);
        super.updatePosition();
    }
}
