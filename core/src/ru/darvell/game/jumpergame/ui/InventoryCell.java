package ru.darvell.game.jumpergame.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ru.darvell.game.jumpergame.entitys.Item;

import static ru.darvell.game.jumpergame.ui.InventoryCellType.WHOLE;

public class InventoryCell extends Actor {

    private TextureRegion textureRegion;
    private Item item;
    private InventoryCellType cellType;
    private boolean dragging = false;
    private Label label;
    private Group group;

    private float longClickEstTime;
    private boolean touched;

    private InventoryCellListener cellListener;

    public InventoryCell(TextureRegion textureRegion, float x, float y, float width, InventoryCellType inventoryCellType,
                         BitmapFont bitmapFont, final InventoryCellListener cellListener) {
        this.textureRegion = textureRegion;
        this.cellType = inventoryCellType;
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(width);

        Skin skin = new Skin(Gdx.files.internal("data/style.json"));
        label = new Label("12", skin);
        label.setFontScale(0.3f, 0.3f);
        label.setHeight(label.getHeight() * 0.3f);

        this.cellListener = cellListener;

//        addListener(new ActorGestureListener(){
//            @Override
//            public boolean longPress(Actor actor, float x, float y) {
//                System.out.println("LONG PRESS");
//                if (!dragging) {
//                    if (item != null) {
//                        cellListener.longClick(item);
//                    }
//                }
//                return super.longPress(actor, x, y);
//            }
//
//        });

        addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("DOWN");
                longClickEstTime = 2f;
                touched = true;
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("UP");
                longClickEstTime = -1;
                touched = false;

                super.touchUp(event, x, y, pointer, button);
            }
        });
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (longClickEstTime >= 0){
            longClickEstTime -= delta;
        } else {
            if (touched) {
                if (item != null) {
                    cellListener.longClick(item);
                }
                touched = false;
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (item != null) {

            TextureRegion tmpTexture = (cellType == WHOLE) ? item.getTexture() :
                    item.getTextureForDialog();

            if (cellType == WHOLE) {
                batch.draw(tmpTexture, getX() + getWidth() / 2f - tmpTexture.getRegionWidth() / 2f,
                        getY() + (getHeight() / 2f - tmpTexture.getRegionHeight() / 2f)
                );
            } else {
                batch.draw(tmpTexture, getX(), getY());
                if (item != null && item.getStackCount() > 1) {
                    label.setText(item.getStackCount());
                    label.setX(getX() + 2f);
                    label.setY(getY() + 2f);
                    label.draw(batch, parentAlpha);
                }
            }
        } else {
            if (cellType != WHOLE) {
                batch.draw(textureRegion, getX(), getY());
            }
        }
    }

    public void setTextureRegion(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public boolean isEmpty() {
        return item == null;
    }

    public InventoryCellType getCellType() {
        return cellType;
    }

    public void setCellType(InventoryCellType cellType) {
        this.cellType = cellType;
    }

    public boolean isDragging() {
        return dragging;
    }

    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }

    public interface InventoryCellListener {
        void longClick(Item item);
    }
}
