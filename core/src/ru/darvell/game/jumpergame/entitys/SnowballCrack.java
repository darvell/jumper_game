package ru.darvell.game.jumpergame.entitys;

import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;

public class SnowballCrack extends UIActor {

    public SnowballCrack(Assets assets) {
        super(assets.getWorldAnimation(Assets.SNOWBALL_CRACK, 0.1f));
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (animation.isAnimationFinished(stateTime)) {
            remove();
        }
    }
}
