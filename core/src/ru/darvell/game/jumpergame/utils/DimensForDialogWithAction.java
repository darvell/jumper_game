package ru.darvell.game.jumpergame.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.utils.dimens.PanelData;


public class DimensForDialogWithAction {

    public float PANEL_HORIZONTAL_OFFSET;
    public float PANEL_VERTICAL_OFFSET;

    public float CELL_HORIZONTAL_OFFSET;
    public float CELL_VERTICAL_OFFSET;

    public float CELL_MERCH_HORIZONTAL_OFFSET;
    public float CELL_MERCH_VERTICAL_OFFSET;

    public float DIALOG_CELL_DIMENS;
    public float DIALOG_CELL_SPACE_FOR_POUCH;
    public float DIALOG_CELL_SPACE_FOR_MERCH;

    public float BUCKET_HORIZONTAL_OFFSET;
    public float BUCKET_VERTICAL_OFFSET;

    public float DOOR_HORIZONTAL_OFFSET;
    public float DOOR_VERTICAL_OFFSET;

    public Array<PanelData> panels = new Array<PanelData>();
}
