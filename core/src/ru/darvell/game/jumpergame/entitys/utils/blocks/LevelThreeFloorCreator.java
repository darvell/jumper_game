package ru.darvell.game.jumpergame.entitys.utils.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import ru.darvell.game.jumpergame.entitys.Block;
import ru.darvell.game.jumpergame.utils.Assets;

public class LevelThreeFloorCreator extends LevelFiveFloorCreator {

    public LevelThreeFloorCreator(World world, Assets assets) {
        super(world, assets);
    }

    @Override
    Block createBlock(ProtoBlock protoBlock, float floorPosY) {
        TextureRegion textureRegion = assets.getBlockTexture(protoBlock.getType());
        Vector2 position = new Vector2(protoBlock.getStartX(), floorPosY);
        int rnd = MathUtils.random(0,1);
//        int rnd = 2;
        Block block;
        if (rnd == 1) {
            block = blockFabrick.getFallBlock(position, textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), lastBlockId++, textureRegion);
        } else {
            block = blockFabrick.getShakedBlock(position, textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), lastBlockId++, shakeListener, textureRegion);
        }
        return block;
    }
}
