package ru.darvell.game.jumpergame.box2dworld;

public abstract class UserData {

    UserDataType userDataType;
    protected float width;
    protected float height;
    private boolean needDelete = false;
    protected float yOffsetForDrawing = 0f;

    public UserDataType getUserDataType(){
        return userDataType;
    }

    public float getHeight() {
        return height;
    }

    public float getWidth() {
        return width;
    }

    public boolean isNeedDelete() {
        return needDelete;
    }

    public void setNeedDelete(boolean needDelete) {
        this.needDelete = needDelete;
    }

    public float getyOffsetForDrawing() {
        return yOffsetForDrawing;
    }
}
