package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

public class FlyingBottleUserData extends UserData {

    public FlyingBottleUserData() {
        width = Constants.BOTTLE_WIDTH;
        height = Constants.BOTTLE_HEIGHT;
        userDataType = UserDataType.FLYING_BOTTLE;
    }
}
