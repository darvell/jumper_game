package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

public class SnowBallUserData extends UserData {

    public float velocity;

    public SnowBallUserData() {
        userDataType = UserDataType.SNOWBALL;
        velocity = Constants.SNOWBALL_VELOCITY;
        width = Constants.SNOWBALL_DIMENS;
        height = Constants.SNOWBALL_DIMENS;
    }
}
