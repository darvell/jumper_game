package ru.darvell.game.jumpergame.stages;

import com.badlogic.gdx.math.MathUtils;
import ru.darvell.game.jumpergame.entitys.Block;
import ru.darvell.game.jumpergame.entitys.Snowman;

public class LevelSixDecorator implements LevelDecorator {

    @Override
    public void doWork(GamePlayStage gamePlayStage) {
        gamePlayStage.player.setIceLevel(true);


//        Snowman snowman1 = gamePlayStage.npcMaster.spawnSnowman(gamePlayStage.blockMaster.getBlockById(2));
//        gamePlayStage.addActor(snowman1);
        for (Block block : gamePlayStage.blockMaster.getBlocksFromMobs()) {
            int rnd = MathUtils.random(1, 6);
            if (rnd == 3 || rnd == 6) {
                Snowman snowman = gamePlayStage.npcMaster.spawnSnowman(block);
                gamePlayStage.addActor(snowman);
            }
        }
    }
}
