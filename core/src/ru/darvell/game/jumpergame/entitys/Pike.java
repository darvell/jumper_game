package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.PikeUserData;
import ru.darvell.game.jumpergame.utils.Assets;

public class Pike extends GameActor {

    private TextureRegion textureRegion;
    private Vector2 velocityVector;
//    private float velosity = 30f;
//    private Vector2 position = new Vector2();
//    private Vector2 pointTo;
//    private int stepCount = 0;


    public Pike(Assets assets, Body body) {
        super(body);
        textureRegion = assets.getUITexture(Assets.UI_PIKE);
        setWidth(textureRegion.getRegionWidth());
        setHeight(textureRegion.getRegionHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(textureRegion, getX(), getY(),
                getWidth() / 2, getHeight() / 2,
                getWidth(), getHeight()
                , 1, 1,
                body.getAngle() * 100);
    }

    @Override
    protected PikeUserData getUserData() throws NullPointerException {
        return body != null ? (PikeUserData) body.getUserData() : null;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
//        if (velocityVector != null && stepCount > 0) {
//            position.add(velocityVector);
//            setPosition(position.x, position.y);
//            stepCount--;
//            setVisible(true);
//        } else {
//            setVisible(false);
//        }
    }


    public void throwUp(Vector2 direction) {
        direction.nor();
        if (body != null) {
            direction.x *= getUserData().velocity;
            direction.y *= getUserData().velocity;

            Vector2 tmpVector = new Vector2();
            tmpVector.set(body.getPosition().x, body.getPosition().y + 5f);
            tmpVector.sub(body.getPosition());

            float angle = tmpVector.angle(direction);

            body.setTransform(body.getPosition(), angle / 100);
            body.applyLinearImpulse(direction, body.getWorldCenter(), true);

        }
//        this.pointTo = pointTo;
//        if (velocityVector == null) {
//            velocityVector = new Vector2();
//        }
//        velocityVector.set(direction);
//        velocityVector.nor();
//        velocityVector.x *= velosity;
//        velocityVector.y *= velosity;
//        stepCount = (int) (direction.len() / velocityVector.len());
//
//
//        position.set(getX(), getY());
    }
}
