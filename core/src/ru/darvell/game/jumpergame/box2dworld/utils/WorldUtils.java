package ru.darvell.game.jumpergame.box2dworld.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import ru.darvell.game.jumpergame.box2dworld.*;
import ru.darvell.game.jumpergame.box2dworld.fixtures.FixtureUserData;
import ru.darvell.game.jumpergame.utils.Constants;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.*;
import static ru.darvell.game.jumpergame.box2dworld.fixtures.FixtureDataType.*;

public class WorldUtils {


    public static World createWorld() {
        return new World(Constants.WORLD_GRAVITY, true);
    }

    public static Body createDefaultBlock(World world, Vector2 position, float width) {
        Body body = createSimpleBody(world, position, width, Constants.BLOCK_HEIGHT, 0, KinematicBody);
        body.setUserData(new BlockUserData(width));
        return body;
    }

    public static Body createBlockWithSoul(World world, Vector2 position, float width){
        Body kinematicBody = createDefaultBlock(world, position, width);
        Body soulBody = createSimpleBody(world, position, width, Constants.BLOCK_HEIGHT, 0, DynamicBody);
        soulBody.setGravityScale(0);
        for (Fixture fixture :soulBody.getFixtureList()){
            fixture.setSensor(true);
        }
        soulBody.setUserData(new BlockUserData(width, UserDataType.BLOCK_SOUL));
        return kinematicBody;
    }

    public static Body createMovableBlock(World world, Vector2 position, float width){
        Body body = createSimpleBody(world, position, width, Constants.BLOCK_HEIGHT, 0, KinematicBody);
        body.setUserData(new BlockUserData(width, UserDataType.BLOCK_MOVE));
        return body;
    }

    public static Body createSimpleBody(World world, Vector2 position, float width, float height, float density, BodyDef.BodyType type){
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = type;
        bodyDef.position.set(position);
        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(width / 2, height/ 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = density;
        fixtureDef.shape = polygonShape;

        body.createFixture(fixtureDef);
        polygonShape.dispose();

        return body;
    }

    public static Body createSnowmanBody(World world, Vector2 position){
        Body body = createSimpleBody(world, position, Constants.SNOWMAN_WIDTH , Constants.SNOWMAN_HEIGHT,
                10f, DynamicBody);
        body.setUserData(new SnowmanUserData());
        body.setFixedRotation(true);
        return body;

    }

    public static Body createSimpleBodyRound(World world, Vector2 position, float width, float density, BodyDef.BodyType type){
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = type;
        bodyDef.position.set(position);
        Body body = world.createBody(bodyDef);

        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(width / 2);


        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = density;
        fixtureDef.shape = circleShape;

        body.createFixture(fixtureDef);
        circleShape.dispose();

        return body;
    }

    public static Body createSnowBulletBody(World world, Vector2 position){
        Body body = createSimpleBodyRound(world, position, Constants.SNOW_BULLET_DIMENS, Constants.SNOW_BULLET_DENSITY, DynamicBody);
        body.setFixedRotation(false);
        body.setUserData(new SnowBulletUserData());
        return body;
    }

    public static Body createBottleBody(World world, Vector2 position){
        Body body = createSimpleBody(world, position, Constants.BOTTLE_WIDTH, Constants.BOTTLE_HEIGHT, 1, DynamicBody);
        body.setUserData(new FlyingBottleUserData());
        body.setFixedRotation(false);

        return body;
    }

    public static Body createPikeBody(World world, Vector2 position){
        Body body = createSimpleBody(world, position, Constants.BOTTLE_WIDTH, Constants.BOTTLE_HEIGHT, 1, DynamicBody);
        body.setUserData(new PikeUserData());
        body.setFixedRotation(false);

        return body;
    }

    public static Body createTeleportBombBody(World world, Vector2 position){
        Body body = createSimpleBodyRound(world, position, Constants.TELEPORT_BOMB_DIMENS, 1, DynamicBody);
        body.setUserData(new TeleportBombUserData());
        body.setFixedRotation(false);
        return body;
    }

    public static Body createSnowBallBody(World world, Vector2 position){
        Body body = createSimpleBodyRound(world, position, Constants.SNOWBALL_DIMENS, 1, DynamicBody);
        body.setUserData(new SnowBallUserData());
        body.setFixedRotation(false);
        return body;
    }

    public static Body createItemOnField(World world, Vector2 position) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        Vector2 correctedPosition = new Vector2(position.x, position.y + Constants.STONE_HEIGHT);
        bodyDef.position.set(correctedPosition);
        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
//        float width = type == BLUE ? Constants.STONE_BLUE_WIDTH : Constants.STONE_HEIGHT;
        polygonShape.setAsBox(Constants.STONE_HEIGHT / 2, Constants.STONE_HEIGHT / 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 10f;
        fixtureDef.shape = polygonShape;
        fixtureDef.friction = 1000f;

        body.createFixture(fixtureDef);
        polygonShape.dispose();

        body.setUserData(new ItemOnFieldUserData(UserDataType.ITEM));
        return body;

    }

    public static Body createPlayerBody(World world, Vector2 position) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        position.set(position.x, position.y + Constants.PLAYER_HEIGHT);
        bodyDef.position.set(position);
        Body body = world.createBody(bodyDef);
        body.setGravityScale(2f);
        body.setBullet(true);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(Constants.PLAYER_WIDTH / 2, Constants.PLAYER_HEIGHT / 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = Constants.PLAYER_DENSITY;
        fixtureDef.shape = polygonShape;

        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setFriction(0);
        polygonShape.dispose();

        PolygonShape polygonShapeSensor = new PolygonShape();
        polygonShape.setAsBox(Constants.PLAYER_GROUND_SENSOR_WIDTH / 2, Constants.PLAYER_GROUND_SENSOR_HEIGHT / 2, new Vector2(0, -(Constants.PLAYER_HEIGHT / 2)), 0);

        FixtureDef groundLandingSensorFixtureDef = new FixtureDef();
        groundLandingSensorFixtureDef.density = 0;
        groundLandingSensorFixtureDef.shape = polygonShapeSensor;
        groundLandingSensorFixtureDef.isSensor = true;

        Fixture groundLandingSensorFixture = body.createFixture(groundLandingSensorFixtureDef);
        groundLandingSensorFixture.setUserData(new FixtureUserData(PLAYER_LANDING_SENSOR));
        polygonShapeSensor.dispose();

        PolygonShape polygonShapeGroundSensor = new PolygonShape();
        polygonShape.setAsBox(Constants.PLAYER_WIDTH / 2, Constants.PLAYER_GROUND_SENSOR_HEIGHT / 2, new Vector2(0, -(Constants.PLAYER_HEIGHT / 2)), 0);

        FixtureDef groundSensorFixtureDef = new FixtureDef();
        groundSensorFixtureDef.density = 0;
        groundSensorFixtureDef.shape = polygonShapeGroundSensor;
        groundSensorFixtureDef.isSensor = true;

        Fixture groundSensorFixture = body.createFixture(groundSensorFixtureDef);
        groundSensorFixture.setUserData(new FixtureUserData(PLAYER_GROUND_SENSOR));
        polygonShapeGroundSensor.dispose();


        body.setFixedRotation(true);
        body.setUserData(new PlayerUserData());
        return body;
    }

    public static Body createGoblinBody(World world, Vector2 position) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        position.y += Constants.GOBLIN_HEIGHT / 2;
        bodyDef.position.set(position);
        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(Constants.GOBLIN_WIDTH / 2, Constants.GOBLIN_HEIGHT / 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = Constants.GOBLIN_DENSITY;
        fixtureDef.shape = polygonShape;

        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setFriction(0);
        polygonShape.dispose();


        //Landing sensor

        PolygonShape polygonShapeLandingSensor = new PolygonShape();
        polygonShape.setAsBox(Constants.GOBLIN_WIDTH / 2, Constants.GOBLIN_GROUND_SENSOR_HEIGHT / 2, new Vector2(0, -(Constants.GOBLIN_HEIGHT / 2)), 0);

        FixtureDef groundLandingSensorFixtureDef = new FixtureDef();
        groundLandingSensorFixtureDef.density = 0;
        groundLandingSensorFixtureDef.shape = polygonShapeLandingSensor;
        groundLandingSensorFixtureDef.isSensor = true;

        Fixture groundLandingSensorFixture = body.createFixture(groundLandingSensorFixtureDef);
        groundLandingSensorFixture.setUserData(new FixtureUserData(GOBLIN_GROUND_LANDED_SENSOR));
        polygonShapeLandingSensor.dispose();

        //End ground LEFT sensor

        PolygonShape polygonShapeEndLeftSensor = new PolygonShape();
        polygonShape.setAsBox(Constants.GOBLIN_END_GROUND_SENSOR_WIDTH / 2, Constants.GOBLIN_GROUND_SENSOR_HEIGHT / 2,
                new Vector2(-(Constants.GOBLIN_WIDTH / 2) + (Constants.GOBLIN_END_GROUND_SENSOR_WIDTH / 2), -(Constants.GOBLIN_HEIGHT / 2)), 0);

        FixtureDef groundEndLeftSensorFixtureDef = new FixtureDef();
        groundEndLeftSensorFixtureDef.density = 0;
        groundEndLeftSensorFixtureDef.shape = polygonShapeEndLeftSensor;
        groundEndLeftSensorFixtureDef.isSensor = true;

        Fixture groundEndLeftSensorFixture = body.createFixture(groundEndLeftSensorFixtureDef);
        groundEndLeftSensorFixture.setUserData(new FixtureUserData(GOBLIN_LEFT_END_GROUND_SENSOR));
        polygonShapeEndLeftSensor.dispose();

        //End ground RIGHT sensor

        PolygonShape polygonShapeEndRightSensor = new PolygonShape();
        polygonShape.setAsBox(Constants.GOBLIN_END_GROUND_SENSOR_WIDTH / 2, Constants.GOBLIN_GROUND_SENSOR_HEIGHT / 2,
                new Vector2((Constants.GOBLIN_WIDTH / 2) - (Constants.GOBLIN_END_GROUND_SENSOR_WIDTH / 2), -(Constants.GOBLIN_HEIGHT / 2)), 0);

        FixtureDef groundEndRightSensorFixtureDef = new FixtureDef();
        groundEndRightSensorFixtureDef.density = 0;
        groundEndRightSensorFixtureDef.shape = polygonShapeEndRightSensor;
        groundEndRightSensorFixtureDef.isSensor = true;

        Fixture groundEndRightSensorFixture = body.createFixture(groundEndRightSensorFixtureDef);
        groundEndRightSensorFixture.setUserData(new FixtureUserData(GOBLIN_RIGHT_END_GROUND_SENSOR));
        polygonShapeEndRightSensor.dispose();

        body.setFixedRotation(true);
        body.setUserData(new GoblinUserData());
        return body;
    }

    public static Body createStormWave(World world, Vector2 position, boolean staticSensor) {
        BodyDef bodyDef = new BodyDef();

        bodyDef.type = staticSensor ? StaticBody : DynamicBody;
        bodyDef.position.set(position);
        Body body = world.createBody(bodyDef);
        body.setBullet(true);
        if (!staticSensor) {
            body.setGravityScale(0f);
        }

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(Constants.FALL_SENSOR_WIDTH / 2, Constants.FALL_SENSOR_HEIGHT / 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 0f;
        fixtureDef.shape = polygonShape;
        fixtureDef.isSensor = true;

        body.createFixture(fixtureDef);
        polygonShape.dispose();

        body.setUserData(staticSensor ? new FallSensorUserData(): new StormWaveUserData());
        return body;

    }

    public static Body[] createSideSensors(World world, Vector2 position, float originHeight) {
        Body[] result = new Body[2];

        position.x = 0 - Constants.SIDE_SENSOR_WIDTH / 2;

        for (int i = 0; i < 2; i++) {
            BodyDef bodyDef = new BodyDef();
            bodyDef.type = DynamicBody;

            bodyDef.position.set(position);
            Body body = world.createBody(bodyDef);
            body.setBullet(true);
            body.setGravityScale(0);

            body.setFixedRotation(true);
            PolygonShape polygonShape = new PolygonShape();
//            System.out.println((originHeight + (Constants.CAMERA_ACTIVE_VIEW_OFFSET / Constants.WORLD_TO_SCREEN) * 2));
            polygonShape.setAsBox(Constants.SIDE_SENSOR_WIDTH / 2, (originHeight + (Constants.CAMERA_ACTIVE_VIEW_OFFSET + 5 / Constants.WORLD_TO_SCREEN) * 2) / 2);

            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.density = 500f;
            fixtureDef.shape = polygonShape;
//            fixtureDef.isSensor = true;

            body.createFixture(fixtureDef);
            polygonShape.dispose();

            body.setUserData(new SideSensorUserData());
            result[i] = body;

            position.x = Constants.MAX_BOX2D_WORLD_WIDTH + Constants.SIDE_SENSOR_WIDTH / 2;
        }
        return result;

    }

}
