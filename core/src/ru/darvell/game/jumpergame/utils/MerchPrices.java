package ru.darvell.game.jumpergame.utils;

import ru.darvell.game.jumpergame.entitys.Tools;

import java.util.HashMap;
import java.util.Map;

import static ru.darvell.game.jumpergame.entitys.Tools.*;

public class MerchPrices {

    Map<Tools, Price> prices = new HashMap<Tools, Price>();

    public MerchPrices(){
        prices.put(BIRDAMINS, new Price(5,5));
        prices.put(PIKES, new Price(2,2));
        prices.put(BOTTLE, new Price(3,3));
        prices.put(BIRDAMET, new Price(5,5));

        prices.put(TELEPORT, new Price(1,1));
        prices.put(BOOTS, new Price(1,1));
        prices.put(LIGHTER, new Price(1,1));
        prices.put(TV, new Price(1,1));

    }

    public int getYellowCount(Tools tool){
        return prices.get(tool).yellow;
    }

    public int getBlueCount(Tools tool){
        return prices.get(tool).blue;
    }

    class Price {
        int yellow;
        int blue;

        public Price(int yellow, int blue) {
            this.yellow = yellow;
            this.blue = blue;
        }
    }
}
