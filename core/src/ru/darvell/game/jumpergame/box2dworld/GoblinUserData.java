package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

public class GoblinUserData extends UserData{

    private boolean landed;
    private float jumpUpVelocity;
    private float speed;
    private float jumpSpeed;
    private float freeRunCountMax;

    private boolean reachedEndOfPlatform = false;

    public GoblinUserData() {
        userDataType = UserDataType.GOBLIN;
        width = Constants.GOBLIN_WIDTH;
        height = Constants.GOBLIN_HEIGHT;
        landed = false;
        jumpUpVelocity = Constants.GOBLIN_JUMP_VELOCITY;
        speed = Constants.GOBLIN_SPEED;
        jumpSpeed = Constants.GOBLIN_JUMP_SPEED;
        freeRunCountMax = Constants.GOBLIN_FREE_RUN_TIME;
    }

    public float getFreeRunCountMax() {
        return freeRunCountMax;
    }

    public boolean isLanded() {
        return landed;
    }

    public void setLanded(boolean landed) {
        this.landed = landed;
    }

    public float getJumpUpVelocity() {
        return jumpUpVelocity;
    }

    public float getSpeed() {
        return speed;
    }

    public boolean isReachedEndOfPlatform() {
        return reachedEndOfPlatform;
    }

    public void setReachedEndOfPlatform(boolean reachedEndOfPlatform) {
        this.reachedEndOfPlatform = reachedEndOfPlatform;
    }

    public float getJumpSpeed() {
        return jumpSpeed;
    }
}

