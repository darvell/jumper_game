package ru.darvell.game.jumpergame.utils;

import com.badlogic.gdx.math.Vector2;

public class Constants {

    public static final float VIEWPORT_RELEASE_WIDTH = 135f;
    public static final float MAX_BOX2D_WORLD_WIDTH = 13.5f;

    public static final Vector2 WORLD_GRAVITY = new Vector2(0, -20);
    public static final float WORLD_TO_SCREEN = 10f;

    public static final float BLOCK_HEIGHT = 0.3f;
    public static final float BLOCK_MIN_Y_OFFSET = 6f;
    public static final float BLOCK_FALL_TIMEOUT = 6f;

    public static final float BLOCK_MIN_MOVE_SPEED = 2f;
    public static final float BLOCK_MAX_MOVE_SPEED = 4.9f;

    public static final float CAMERA_BLOCK_OFFSET = 30f;
    public static final float CAMERA_PLAYER_OFFSET = 100f;
    public static final float CAMERA_ACTIVE_VIEW_OFFSET = 100f;

    public static final float STONE_HEIGHT = 1.4f;
    public static final float STONE_BLUE_WIDTH = 1.5f;


    public static final float FALL_SENSOR_WIDTH = 50;
    public static final float FALL_SENSOR_HEIGHT = 20;

    public static final float SIDE_SENSOR_WIDTH = 2.0f;
    public static final float SIDE_SENSOR_HEIGHT = 50.f;

    public static final float PLAYER_HEIGHT = 2.6f;
    public static final float PLAYER_WIDTH = 1.1f;
    public static final float PLAYER_DRAWING_HEIGHT = 2.6f;
    public static final float PLAYER_DRAWING_WIDTH = 1.7f;
    public static final float PLAYER_GROUND_SENSOR_HEIGHT = 0.2f;
    public static final float PLAYER_GROUND_SENSOR_WIDTH = 0.8f;
    public static final float PLAYER_DENSITY = 0.9f;
    public static final float PLAYER_SPEED = 7.5f;
    public static final float PLAYER_MAX_VILOCITY = 7.5f;
    public static final float PLAYER_MAX_VILOCITY_BADDITIONAL = 5f;
    public static final float PLAYER_JUMP_VELOCITY = 15.5f;
    public static final float PLAYER_JUMP_VELOCITY_BADDITIONAL = 5f;
    public static final float PLAYER_EXTRA_JUMP_VELOCITY = 18.5f;
    public static final float PLAYER_EXTRA_JUMP_VELOCITY_BADDITIONAL = 6f;
    public static final float PLAYER_MAX_LANDING_VELOCITY = -25f;
    public static final Vector2 PLAYER_JUMPING_LINEAR_IMPULSE = new Vector2(0, 30f);
    public static final Vector2 PLAYER_EXTRA_JUMPING_LINEAR_IMPULSE = new Vector2(0, 30f);
    public static final int PLAYER_BLINKING_SKIP_FRAMES_COUNT = 8;
    public static final int PLAYER_BLINKING_COUNT = 20;
    public static final int PLAYER_MAX_LIFE_COUNT = 3;
    public static final float PLAYER_MAX_BIRDAMINS_TIME = 20f;

    public static final float GOBLIN_HEIGHT = 2f;
    public static final float GOBLIN_WIDTH = 1.0f;
    public static final float GOBLIN_DRAWING_HEIGHT = 2.5f;
    public static final float GOBLIN_DRAWING_WIDTH = 2.3f;
    public static final float GOBLIN_DENSITY = 5f;
    public static final float GOBLIN_SPEED = 2f;
    public static final float GOBLIN_JUMP_SPEED = 4f;
    public static final float GOBLIN_JUMP_VELOCITY = 11f;
    public static final float GOBLIN_GROUND_SENSOR_HEIGHT = 0.2f;
    public static final float GOBLIN_END_GROUND_SENSOR_WIDTH = 0.1f;
    public static final float GOBLIN_FREE_RUN_TIME = 10f;

    public static final float SNOWBALL_DIMENS = 1.4f;
    public static final float SNOWBALL_VELOCITY = 5f;

    public static final float SNOWMAN_HEIGHT = 3f;
    public static final float SNOWMAN_Y_OFFSET = 0.7f;
    public static final float SNOWMAN_WIDTH = 1.4f;
    public static final float SNOWMAN_FIRE_TIMEOUT_MAX = 4f;
    public static final float SNOWMAN_SNOWBAL_TIMEOUT_MAX = 6f;


    public static final float DVIG_DRAWING_WIDTH = 29f;
    public static final float DVIG_DRAWING_HEIGHT = 40f;
    public static final float DVIG_Y_OFFSET = -3f;


    public static final float BACKGROUND_WIDTH = 135f;
    public static final float BACKGROUND_HEIGHT = 7487f;

    public static final float BOTTLE_HEIGHT = 1.1f;
    public static final float BOTTLE_WIDTH = 0.8f;

    public static final float TELEPORT_BOMB_DIMENS = 1.3f;
    public static final float TELEPORT_BOMB_VELOCITY = 10f;

    public static final float PIKE_HEIGHT = 1.5f;
    public static final float PIKE_WIDTH = .7f;
    public static final float PIKE_VELOCITY = 30f;

    public static final float TOUCH_AREA_Y_OFFSET = 38f;

    public static final float SNOW_BULLET_DIMENS = 0.5f;
    public static final float SNOW_BULLET_SPEED = 5f;
    public static final float SNOW_BULLET_DENSITY = 2f;
    public static final float SNOW_BULLET_LIFE_TIME = 5f;


    public static MerchPrices merchPrices;
    static {
        merchPrices = new MerchPrices();
    }

}
