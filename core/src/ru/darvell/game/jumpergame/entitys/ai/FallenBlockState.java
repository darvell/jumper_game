package ru.darvell.game.jumpergame.entitys.ai;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.math.MathUtils;
import ru.darvell.game.jumpergame.entitys.FallenBlock;

public enum FallenBlockState implements State<FallenBlock> {

    NOTHING() {
        @Override
        public void update(FallenBlock block){
             if (block.fallTimeOutIsOver()){
                 int rnd = MathUtils.random(0, 5);
                 if (rnd == 1) {
                     block.stateMachine.changeState(SHAKING);
                     block.initShake();
                 } else {
                     block.resetFallTimeOut();
                 }
             } else {
                 block.updateFallTimeout();
             }
        }
    },
    SHAKING(){
        @Override
        public void update(FallenBlock block){
            if (block.shakeTimeOutIsOver()){
                block.stateMachine.changeState(FALLING);
            } else {
                block.updateShake();
            }
        }
    },
    FALLING(){
        @Override
        public void update(FallenBlock block){
            if (block.checkFallIsOver()){
                block.stateMachine.changeState(RETURNING);
            } else {
                block.doFalling();
            }
        }
    },
    RETURNING() {
        @Override
        public void update(FallenBlock block) {
            if (block.checkAndUpdateReturningIsOver()){
                block.stateMachine.changeState(NOTHING);
            }
        }
    };



    @Override
    public void enter(FallenBlock entity) {

    }

    @Override
    public void exit(FallenBlock entity) {

    }

    @Override
    public boolean onMessage(FallenBlock entity, Telegram telegram) {
        return false;
    }
}
