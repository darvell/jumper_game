package ru.darvell.game.jumpergame.entitys.utils;

import ru.darvell.game.jumpergame.entitys.Dvig;

public interface DvigClickListener {
    void dvigClicked(Dvig dvig);
}
