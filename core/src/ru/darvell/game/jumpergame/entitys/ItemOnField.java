package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.ItemOnFieldUserData;

public class ItemOnField extends GameActor {

    private Item item;

    public ItemOnField(Body body, Item item) {
        super(body);
        this.item = item;
    }

    @Override
    protected ItemOnFieldUserData getUserData() throws NullPointerException {
        if (body != null && body.getUserData() != null) {
            return (ItemOnFieldUserData) body.getUserData();
        }
        return null;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(item.getTexture(), getX(), getY(), 14f, 14f);
    }

    public boolean isPickedUp() {
        try {
            return getUserData().isPickedUp();
        } catch (Exception e) {
            return false;
        }
    }

    public void setPickedUp(boolean pickedUp) {
        try {
            getUserData().setPickedUp(pickedUp);
        } catch (Exception e) {
        }
    }

    public void setNeedDelete() {
        try {
            getUserData().setNeedDelete(true);
        } catch (Exception e) {

        }
    }

    public Item getItem() {
        return item;
    }
}
