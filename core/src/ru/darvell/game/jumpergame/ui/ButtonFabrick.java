package ru.darvell.game.jumpergame.ui;

import ru.darvell.game.jumpergame.utils.Assets;

public class ButtonFabrick {
    private Assets assets;

    public ButtonFabrick(Assets assets) {
        this.assets = assets;
    }

    public MyButton getButton(ButtonType type, ButtonClickListener clickListener) {
        switch (type) {
            case RESET_DIALOG_NEW_GAME:
                return new MyButton(
                        assets.getUITexture(Assets.UI_PAPER_ONE), type, clickListener
                );
            case RESET_DIALOG_TO_MENU:
                return new MyButton(
                        assets.getUITexture(Assets.UI_PAPER_TWO), type, clickListener
                );
            case CONTROL_UP:
                return new MyButton(
                        assets.getUITexture(Assets.UI_UP_ARROW), type, clickListener
                );
            case CONTROL_RIGHT:
                return new MyButton(
                        assets.getUITexture(Assets.UI_RIGT_ARROW), type, clickListener
                );
            case CONTROL_LEFT:
                return new MyButton(
                        assets.getUITexture(Assets.UI_LEFT_ARROW), type, clickListener
                );
            case MENU_NEW_GAME:
                return new MyButton(
                        assets.getUITexture(Assets.UI_BTN_START_NRML), assets.getUITexture(Assets.UI_BTN_START_PRSSD), type, clickListener
                );
            case MENU_LOAD_GAME:
                return new MyButton(
                        assets.getUITexture(Assets.UI_BTN_LOAD_NRML), assets.getUITexture(Assets.UI_BTN_LOAD_PRSSD), type, clickListener
                );
            case MENU_SETTINGS:
                return new MyButton(
                        assets.getUITexture(Assets.UI_BTN_SETTINGS_NRML), assets.getUITexture(Assets.UI_BTN_SETTINGS_PRSSD), type, clickListener
                );
            case BACKPACK:
                return new MyButton(
                        assets.getUITexture(Assets.UI_BTN_BACKPACK), assets.getUITexture(Assets.UI_BTN_BACKPACK), type, clickListener
                );
            default:
                return null;
        }
    }
}
