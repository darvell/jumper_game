package ru.darvell.game.jumpergame.stages;

import com.badlogic.gdx.math.MathUtils;
import ru.darvell.game.jumpergame.entitys.Block;
import ru.darvell.game.jumpergame.entitys.Goblin;

public class LevelOneDecorator implements LevelDecorator {
    @Override
    public void doWork(GamePlayStage gamePlayStage) {
        genGoblins(gamePlayStage);
        gamePlayStage.initSpaceShip();
        gamePlayStage.initShop();
    }

    public void genGoblins(GamePlayStage gamePlayStage){
        for (Block block : gamePlayStage.blockMaster.getBlocksFromMobs()) {
            int rnd = MathUtils.random(1, 6);
            if (rnd == 3) {
                Goblin goblin = gamePlayStage.npcMaster.spawnGoblin(block);
                gamePlayStage.addActor(goblin);
            }
        }
    }
}
