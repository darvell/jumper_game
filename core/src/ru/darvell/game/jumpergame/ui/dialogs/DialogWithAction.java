package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.ui.InventoryCell;
import ru.darvell.game.jumpergame.ui.TextPanel;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.DimensForDialogWithAction;
import ru.darvell.game.jumpergame.utils.dimens.BigUIPouchDialog;
import ru.darvell.game.jumpergame.utils.dimens.PanelData;
import ru.darvell.game.jumpergame.utils.dimens.SmallUIPouchDialog;

import static ru.darvell.game.jumpergame.ui.InventoryCellType.POUCH;

public class DialogWithAction extends MyDialog {

    private TextPanel textPanelInventory;
    protected Array<InventoryCell> pouchCells;
    protected DimensForDialogWithAction dimens;
    protected float centerY;

    public DialogWithAction(Assets assets, float screenHeight, DialogListener dialogListener) {
        super(assets, screenHeight, dialogListener);
        initDimens();
    }

    protected void initDimens(){
        if (screenHeight < 240f) {
            dimens = new SmallUIPouchDialog();
        } else {
            dimens = new BigUIPouchDialog();
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            initPanels();
            initInventoryCells();
        } else {
            if (playerPouch != null) {
                for (Item item : playerPouch.getItems()) {
                    item.remove();
                }
                for (InventoryCell cell : pouchCells) {
                    cell.remove();
                }
            }
            dragAndDrop.clear();
        }
        super.setVisible(visible);
    }

    protected void initPanels() {
        if (dimens.panels.size > 0) {

            for (PanelData panelData : dimens.panels) {
                textPanelInventory = new TextPanel(assets.getUITexture(Assets.UI_TEXT_PANEL_LONG),
                        panelData.getStartPosition().x, centerY + panelData.getStartPosition().y
                        );
                addActor(textPanelInventory);
            }

        } else {
            if (textPanelInventory == null) {
                textPanelInventory = new TextPanel(assets.getUITexture(Assets.UI_TEXT_PANEL_LONG),
                        dimens.PANEL_HORIZONTAL_OFFSET,
                        getHeight() / 2 + dimens.PANEL_VERTICAL_OFFSET);
                addActor(textPanelInventory);
            }
        }
    }

    protected void initInventoryCells(){
        float startX = dimens.CELL_HORIZONTAL_OFFSET;
        float startY = getHeight() / 2 + dimens.CELL_VERTICAL_OFFSET;
        initInventoryCells(startX, startY);
    }

    protected void initInventoryCells(float startX, float startY) {
        pouchCells = generateCells(startX, startY, dimens.DIALOG_CELL_SPACE_FOR_POUCH, playerPouch.getPouchCurrentCapacity(), 5);
        fillCells(playerPouch.getItems(), pouchCells, dimens.DIALOG_CELL_DIMENS);
    }

    protected Array<InventoryCell> generateCells(float startX, float startY, float space, int cellCount, int maxColCount) {

        Array<InventoryCell> result = new Array<InventoryCell>();
        int tmpColCount = 1;
        float currX = startX;

        for (int i = 0; i < cellCount; i++) {
            InventoryCell cell = new InventoryCell(assets.getUITexture(Assets.UI_EMPTY_CELL), currX,
                    startY,
                    dimens.DIALOG_CELL_DIMENS,
                    POUCH, assets.getFont(Assets.FONT_ZX_5_WHITE),
                    this);
            result.add(cell);
            addActor(cell);
            tmpColCount++;
            if (tmpColCount > maxColCount){
                currX = startX;
                tmpColCount = 1;
                startY -= (dimens.DIALOG_CELL_DIMENS + space);
            } else {
                currX += (dimens.DIALOG_CELL_DIMENS + space);
            }
        }
        return result;

    }
}
