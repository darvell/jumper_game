package ru.darvell.game.jumpergame.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ru.darvell.game.jumpergame.entitys.Item;

public class HandInventory extends Actor {

    private TextureRegion textureRegion;
    private Item item;
    private boolean readyToAction = false;
    private final HandInventory self;
    private Label label;

    public HandInventory(TextureRegion textureRegion, float x, float y, final HandInventoryType type, final HandInventoryListener listener) {
        self = this;
        this.textureRegion = textureRegion;
        setX(x);
        setY(y);
        setWidth(textureRegion.getRegionWidth());
        setHeight(textureRegion.getRegionHeight());
        addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                listener.onHandInventoryClick(type, self);
            }
        });

        Skin skin = new Skin(Gdx.files.internal("data/style.json"));
        label = new Label("12", skin);
        label.setFontScale(0.3f, 0.3f);
        label.setHeight(label.getHeight() * 0.3f);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(textureRegion, getX(), getY());
        if (item != null) {
            TextureRegion itemTexture = item.getTexture();
            batch.draw(itemTexture, getX(), getY(), getWidth(), getHeight());
            if (item.getStackCount() > 1) {
                label.setText(item.getStackCount());
                label.setX(getX() + 2f);
                label.setY(getY() + 2f);
                label.draw(batch, parentAlpha);
            }
        }
    }

    public void putItem(Item item){
        this.item = item;
    }

    public Item getItem(){
        return item;
    }

    public Item withdrawItem(){
        Item result = item;
        item = null;
        return result;
    }

    public boolean isReadyToAction() {
        return readyToAction;
    }

    public void setReadyToAction(boolean readyToAction) {
        this.readyToAction = readyToAction;
    }
}
