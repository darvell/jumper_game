package ru.darvell.game.jumpergame.utils;

public class UIMerchDialog {

    public float PANEL_HORIZONTAL_OFFSET;
    public float PANEL_VERTICAL_OFFSET;

    public float CELL_HORIZONTAL_OFFSET;
    public float CELL_VERTICAL_OFFSET;

    public float DIALOG_CELL_DIMENS;
    public float DIALOG_CELL_SPACE;

    public float BUCKET_HORIZONTAL_OFFSET;
    public float BUCKET_VERTICAL_OFFSET;

    public float DOOR_HORIZONTAL_OFFSET;
    public float DOOR_VERTICAL_OFFSET;
}
