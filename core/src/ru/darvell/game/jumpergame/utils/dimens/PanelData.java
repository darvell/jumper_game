package ru.darvell.game.jumpergame.utils.dimens;

import com.badlogic.gdx.math.Vector2;

public class PanelData {

    private Vector2 startPosition;
    private String text;

    public PanelData(Vector2 startPosition, String text) {
        this.startPosition = startPosition;
        this.text = text;
    }

    public Vector2 getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(Vector2 startPosition) {
        this.startPosition = startPosition;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
