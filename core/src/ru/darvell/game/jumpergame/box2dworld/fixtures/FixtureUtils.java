package ru.darvell.game.jumpergame.box2dworld.fixtures;

import com.badlogic.gdx.physics.box2d.Fixture;

public class FixtureUtils {

    public static boolean fixtureIsPlayerGroundSensor(Fixture fixture){
        FixtureUserData fixtureUserData = (FixtureUserData) fixture.getUserData();
        return fixtureUserData != null && fixtureUserData.getFixtureDataType() == FixtureDataType.PLAYER_GROUND_SENSOR;
    }

    public static boolean fixtureIsPlayerLandingSensor(Fixture fixture){
        FixtureUserData fixtureUserData = (FixtureUserData) fixture.getUserData();
        return fixtureUserData != null && fixtureUserData.getFixtureDataType() == FixtureDataType.PLAYER_LANDING_SENSOR;
    }

    public static boolean fixtureIsGoblinGroundLandingSensor(Fixture fixture){
        FixtureUserData fixtureUserData = (FixtureUserData) fixture.getUserData();
        return fixtureUserData != null && fixtureUserData.getFixtureDataType() == FixtureDataType.GOBLIN_GROUND_LANDED_SENSOR;
    }

    public static boolean fixtureIsGoblinRightEndGroundSensor(Fixture fixture){
        FixtureUserData fixtureUserData = (FixtureUserData) fixture.getUserData();
        return fixtureUserData != null && fixtureUserData.getFixtureDataType() == FixtureDataType.GOBLIN_RIGHT_END_GROUND_SENSOR;
    }

    public static boolean fixtureIsGoblinLeftEndGroundSensor(Fixture fixture){
        FixtureUserData fixtureUserData = (FixtureUserData) fixture.getUserData();
        return fixtureUserData != null && fixtureUserData.getFixtureDataType() == FixtureDataType.GOBLIN_LEFT_END_GROUND_SENSOR;
    }
}
