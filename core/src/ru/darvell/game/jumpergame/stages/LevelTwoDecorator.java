package ru.darvell.game.jumpergame.stages;

public class LevelTwoDecorator extends LevelOneDecorator {

    @Override
    public void doWork(GamePlayStage gamePlayStage) {
        genGoblins(gamePlayStage);
    }
}
