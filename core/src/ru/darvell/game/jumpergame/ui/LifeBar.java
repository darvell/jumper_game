package ru.darvell.game.jumpergame.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import ru.darvell.game.jumpergame.utils.Assets;

public class LifeBar extends Group implements PlayerLifesListener{

    private TextureRegion panelTexture;
    private TextureRegion heartTexture;

    int lifesCount = 3;

    public LifeBar(float screenHeight, Assets assets) {
        super();
        panelTexture = assets.getUITexture(Assets.UI_LIFEBAR);
        heartTexture = assets.getUITexture(Assets.UI_LIFEBAR_HEART);
        setHeight(panelTexture.getRegionHeight());
        setWidth(panelTexture.getRegionWidth());
        setX(5f);
        setY(screenHeight - panelTexture.getRegionHeight() - 5f);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(panelTexture, getX(), getY(), getWidth(), getHeight());
        if (lifesCount > 0) {
            float nextX = getX() + 15;
            for (int i = 0; i < lifesCount; i++) {
                batch.draw(heartTexture, nextX, getY() + getHeight() / 2 - heartTexture.getRegionHeight()
                        , heartTexture.getRegionWidth(), heartTexture.getRegionHeight());
                nextX += heartTexture.getRegionWidth() + 1f;
            }
        }
    }

    @Override
    public void updateLifeCount(int lifeCount){
        this.lifesCount = lifeCount;
    }
}
