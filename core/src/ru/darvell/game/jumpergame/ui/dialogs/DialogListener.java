package ru.darvell.game.jumpergame.ui.dialogs;

import ru.darvell.game.jumpergame.entitys.Item;

public interface DialogListener {
    void dialogClose(MyDialog myDialog);
    void returnToMainMenu();
    void doResetGame();
    void initChangeLevel(int stoneCount);
    void initReturnToLevelOne();
    void clickItem(Item item);
    void removeItem(Item item);
    void longPress(Item item);
    void itemBeingActivated(Item item);
}
