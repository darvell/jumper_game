package ru.darvell.game.jumpergame.entitys.utils.blocks;

import com.badlogic.gdx.physics.box2d.World;
import ru.darvell.game.jumpergame.utils.Assets;

import static ru.darvell.game.jumpergame.entitys.utils.blocks.BlockTypes.*;

public class LevelTwoFloorCreator extends LevelOneFloorCreator{

    public LevelTwoFloorCreator(World world, Assets assets) {
        super(world, assets);
    }

    @Override
    void initPatterns() {
        generatorPatterns.add(new FloorPattern(1, new ProtoBlock(BLOCK_TWO, 0.1f), new ProtoBlock(BLOCK_TWO, 5.3f), new ProtoBlock(BLOCK_TWO, 10.2f)));
        generatorPatterns.add(new FloorPattern(2, new ProtoBlock(BLOCK_TWO, 2.6f), new ProtoBlock(BLOCK_TWO, 8f)));
        generatorPatterns.add(new FloorPattern(3, new ProtoBlock(BLOCK_TWO, 0.1f), new ProtoBlock(BLOCK_TWO, 10.2f)));

        generatorPatterns.add(new FloorPattern(4, new ProtoBlock(BLOCK_THREE, 1.5f)));
        generatorPatterns.add(new FloorPattern(5, new ProtoBlock(BLOCK_THREE, 4.3f)));
        generatorPatterns.add(new FloorPattern(6, new ProtoBlock(BLOCK_THREE, 6.3f)));

        generatorPatterns.add(new FloorPattern(7, new ProtoBlock(BLOCK_THREE, 0.1f), new ProtoBlock(BLOCK_TWO, 9f)));
        generatorPatterns.add(new FloorPattern(8, new ProtoBlock(BLOCK_TWO, 2.5f), new ProtoBlock(BLOCK_THREE, 7.5f)));

        generatorPatterns.add(new FloorPattern(9, new ProtoBlock(BLOCK_FOUR, 2.5f)));

//        generatorPatterns.add(new FloorPattern(new ProtoBlock(BLOCK_TWO, 0.1f), new ProtoBlock(BLOCK_FOUR, 5f)));
//        generatorPatterns.add(new FloorPattern(new ProtoBlock(BLOCK_FOUR, 0.1f), new ProtoBlock(BLOCK_TWO, 10.2f)));
    }


}
