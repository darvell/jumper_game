package ru.darvell.game.jumpergame.entitys.utils.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import ru.darvell.game.jumpergame.entitys.Block;
import ru.darvell.game.jumpergame.utils.Assets;

import static ru.darvell.game.jumpergame.entitys.utils.blocks.BlockTypes.*;

public class LevelFiveFloorCreator extends FloorCreator {

    public LevelFiveFloorCreator(World world, Assets assets) {
        super(world, assets);
    }

    @Override
    void initPatterns() {
        generatorPatterns.add(new FloorPattern(1, new ProtoBlock(BLOCK_ONE, 0.5f), new ProtoBlock(BLOCK_ONE, 11f)));
        generatorPatterns.add(new FloorPattern(2, new ProtoBlock(BLOCK_ONE, 4f), new ProtoBlock(BLOCK_ONE, 11f)));
        generatorPatterns.add(new FloorPattern(3, new ProtoBlock(BLOCK_ONE, 0.6f), new ProtoBlock(BLOCK_ONE, 6f), new ProtoBlock(BLOCK_ONE, 11f)));

        generatorPatterns.add(new FloorPattern(4, new ProtoBlock(BLOCK_ONE, 0.2f), new ProtoBlock(BLOCK_TWO, 5f)));
        generatorPatterns.add(new FloorPattern(5, new ProtoBlock(BLOCK_ONE, 3f), new ProtoBlock(BLOCK_TWO, 7f)));
        generatorPatterns.add(new FloorPattern(6, new ProtoBlock(BLOCK_ONE, 2f), new ProtoBlock(BLOCK_TWO, 8.5f)));
        generatorPatterns.add(new FloorPattern(7, new ProtoBlock(BLOCK_ONE, 0.2f), new ProtoBlock(BLOCK_TWO, 9.5f)));
////
        generatorPatterns.add(new FloorPattern(8, new ProtoBlock(BLOCK_TWO, 0.2f), new ProtoBlock(BLOCK_ONE, 9f)));
        generatorPatterns.add(new FloorPattern(9, new ProtoBlock(BLOCK_TWO, 4.5f), new ProtoBlock(BLOCK_ONE, 11f)));
        generatorPatterns.add(new FloorPattern(10, new ProtoBlock(BLOCK_TWO, 2f), new ProtoBlock(BLOCK_ONE, 8.5f)));
//
        generatorPatterns.add(new FloorPattern(11, new ProtoBlock(BLOCK_TWO, 0.2f), new ProtoBlock(BLOCK_TWO, 6f)));
        generatorPatterns.add(new FloorPattern(12, new ProtoBlock(BLOCK_TWO, 0.2f), new ProtoBlock(BLOCK_TWO, 9.5f)));
        generatorPatterns.add(new FloorPattern(13, new ProtoBlock(BLOCK_TWO, 3f), new ProtoBlock(BLOCK_TWO, 9.5f)));

        generatorPatterns.add(new FloorPattern(14, new ProtoBlock(BLOCK_TWO, 0.2f), new ProtoBlock(BLOCK_ONE, 6.3f), new ProtoBlock(BLOCK_ONE, 10.5f)));
        generatorPatterns.add(new FloorPattern(15, new ProtoBlock(BLOCK_ONE, 0.2f), new ProtoBlock(BLOCK_ONE, 4.2f), new ProtoBlock(BLOCK_TWO, 8.5f)));

        generatorPatterns.add(new FloorPattern(16, new ProtoBlock(BLOCK_THREE, 0.2f), new ProtoBlock(BLOCK_ONE, 7.9f), new ProtoBlock(BLOCK_ONE, 11.5f)));
        generatorPatterns.add(new FloorPattern(17, new ProtoBlock(BLOCK_ONE, 0.2f), new ProtoBlock(BLOCK_THREE, 4f), new ProtoBlock(BLOCK_ONE, 11.5f)));
        generatorPatterns.add(new FloorPattern(18, new ProtoBlock(BLOCK_ONE, 0.2f), new ProtoBlock(BLOCK_ONE, 4.1f), new ProtoBlock(BLOCK_THREE, 7.8f)));
    }

    @Override
    Block createBlock(ProtoBlock protoBlock, float floorPosY) {
        TextureRegion textureRegion = assets.getBlockTexture(protoBlock.getType());
        Vector2 position = new Vector2(protoBlock.getStartX(), floorPosY);
        Block block = blockFabrick.getShakedBlock(position, textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), lastBlockId++, shakeListener, textureRegion);
        return block;
    }


}
