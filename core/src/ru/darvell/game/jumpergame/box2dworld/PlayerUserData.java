package ru.darvell.game.jumpergame.box2dworld;

import com.badlogic.gdx.math.Vector2;
import ru.darvell.game.jumpergame.entitys.ItemOnField;
import ru.darvell.game.jumpergame.utils.Constants;

public class PlayerUserData extends UserData {

    private boolean landed;
    private Vector2 jumpingLinearImpulse;
    private Vector2 extraJumpingLinearImpulse;
    private float jumpVelocity;
    private float extraJumpVelocity;

    private float speed;
    private boolean extraJump = false;

    private boolean fellDown = false;
    private boolean badTouched = false;
    private boolean deepFall = false;
    private boolean needLandingAnimation = false;
    private boolean beenJumped = false;

    private int blockId = -1;
    private int lastBlockId = -1;
    private float friction = 0f;

    private boolean touchYellowStone = false;
    private boolean touchBlueStone = false;

    private ItemOnField itemOnField;

    public PlayerUserData() {
        super();
        userDataType = UserDataType.PLAYER;
        landed = true;
        jumpingLinearImpulse = Constants.PLAYER_JUMPING_LINEAR_IMPULSE;
        extraJumpingLinearImpulse = Constants.PLAYER_EXTRA_JUMPING_LINEAR_IMPULSE;
        speed = Constants.PLAYER_SPEED;
//        height = Constants.PLAYER_HEIGHT + Constants.PLAYER_WIDTH / 2;
        height = Constants.PLAYER_HEIGHT;
        width = Constants.PLAYER_WIDTH;
        jumpVelocity = Constants.PLAYER_JUMP_VELOCITY;
        extraJumpVelocity = Constants.PLAYER_EXTRA_JUMP_VELOCITY;

    }
//03-06 13:35:10.851 12578-13419/ru.darvell.game.jumpergame A/libc: Fatal signal 11 (SIGSEGV), code 1, fault addr 0xa000003ee6666e in tid 13419 (GLThread 43624)
    public int getBlockId() {
        return blockId;
    }

    private void setBlockId(int blockId) {
        this.blockId = blockId;

    }

    public boolean hasExtraJump() {
        return extraJump;
    }

    public void setExtraJump(boolean extraJump) {
        this.extraJump = extraJump;
    }

    public boolean isLanded() {
        return landed;
    }

    public void setLanded(int blockId) {
        this.landed = true;
        this.extraJump = true;
        this.beenJumped = false;
        setBlockId(blockId);
        lastBlockId = getBlockId();
    }

    public void setNotLanded(){
        this.landed = false;
        setBlockId(-1);
    }

    public ItemOnField getItemOnField() {
        return itemOnField;
    }

    public void setItemOnField(ItemOnField itemOnField) {
        this.itemOnField = itemOnField;
    }

    public Vector2 getJumpingLinearImpulse() {
        return jumpingLinearImpulse;
    }

    public Vector2 getExtraJumpingLinearImpulse() {
        return extraJumpingLinearImpulse;
    }

    public float getJumpVelocity() {
        return jumpVelocity;
    }

    public float getExtraJumpVelocity() {
        return extraJumpVelocity;
    }

    public float getSpeed() {
        return speed;
    }

    public boolean isFellDown() {
        return fellDown;
    }

    public void setFellDown(boolean fellDown) {
        this.fellDown = fellDown;
    }

    public boolean isBadTouched() {
        return badTouched;
    }

    public void setBadTouched(boolean badTouched) {
        this.badTouched = badTouched;
    }

    public boolean isDeepFall() {
        return deepFall;
    }

    public void setDeepFall(boolean deepFall) {
        this.deepFall = deepFall;
    }

    public int getLastBlockId() {
        return lastBlockId;
    }

    public boolean isNeedLandingAnimation() {
        return needLandingAnimation;
    }

    public void setNeedLandingAnimation(boolean needLandingAnimation) {
        this.needLandingAnimation = needLandingAnimation;
    }

    public float getFriction() {
        return friction;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }

    public boolean isBeenJumped() {
        return beenJumped;
    }

    public void setBeenJumped(boolean beenJumped) {
        this.beenJumped = beenJumped;
    }

    public boolean isTouchYellowStone() {
        return touchYellowStone;
    }

    public void setTouchYellowStone(boolean touchYellowStone) {
        this.touchYellowStone = touchYellowStone;
    }

    public boolean isTouchBlueStone() {
        return touchBlueStone;
    }

    public void setTouchBlueStone(boolean touchBlueStone) {
        this.touchBlueStone = touchBlueStone;
    }
}
