package ru.darvell.game.jumpergame.stages;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.entitys.interfaces.Controllable;
import ru.darvell.game.jumpergame.entitys.utils.PlayerPouch;
import ru.darvell.game.jumpergame.entitys.utils.SpaceShipInventory;
import ru.darvell.game.jumpergame.screens.ParentScreen;
import ru.darvell.game.jumpergame.ui.*;
import ru.darvell.game.jumpergame.ui.dialogs.*;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.GameState;
import ru.darvell.game.jumpergame.utils.Levels;

import static ru.darvell.game.jumpergame.entitys.Tools.*;
import static ru.darvell.game.jumpergame.ui.ButtonType.*;
import static ru.darvell.game.jumpergame.ui.HandInventoryType.ACTION;
import static ru.darvell.game.jumpergame.ui.HandInventoryType.HOLD;

public class UIStage extends Stage implements ButtonClickListener, DialogListener, HandInventoryListener {

    private float viewPortWidth;
    private float viewPortHeight;

    private Assets assets;
    private LifeBar lifeBar;
    private MyButton backpackBtn;
    private ResetLevelDialog resetLevelDialog;
    private DoorOneDialog doorOneDialog;
    private DoorTwoDialog doorTwoDialog;
    private SpaceShipDialog spaceShipDialog;
    private PouchDialog pouchDialog;
    private MerchantDialog merchantDialog;

    private ItemInfo itemInfo;

    private ButtonFabrick buttonFabrick;
    private ParentScreen parentScreen;
    private Controllable controllable;

    private HandInventory handInventoryUsable;
    private HandInventory handInventoryHold;

    private float yControllButtonOffset = 0f;


    public UIStage(float width, float height, Assets assets, ParentScreen parentScreen, Controllable controllable) {
        super(new FillViewport(width, height));
        this.assets = assets;
        viewPortWidth = width;
        viewPortHeight = height;

        buttonFabrick = new ButtonFabrick(assets);
        this.parentScreen = parentScreen;

        this.controllable = controllable;

        lifeBar = new LifeBar(height, assets);
        addActor(lifeBar);

        initControlButtons();

        resetLevelDialog = new ResetLevelDialog(height, assets, buttonFabrick, this);
        addActor(resetLevelDialog);
        resetLevelDialog.setVisible(false);

        doorOneDialog = new DoorOneDialog(height, assets, this);
        addActor(doorOneDialog);
        doorOneDialog.setVisible(false);

        doorTwoDialog = new DoorTwoDialog(height, assets, this);
        addActor(doorTwoDialog);
        doorTwoDialog.setVisible(false);

        spaceShipDialog = new SpaceShipDialog(assets, height, this);
        addActor(spaceShipDialog);
        spaceShipDialog.setVisible(false);

        pouchDialog = new PouchDialog(assets, height, this);
        addActor(pouchDialog);
        pouchDialog.setVisible(false);

        merchantDialog = new MerchantDialog(assets, height, this);
        addActor(merchantDialog);
        merchantDialog.setVisible(false);


        initBackpackButton();
        initHandInventory();

        itemInfo = new ItemInfo(assets);
        addActor(itemInfo);

        itemInfo.setX(1);
        itemInfo.setY(viewPortHeight / 2 - itemInfo.getHeight() / 2);
        itemInfo.setVisible(false);
    }

    private void initControlButtons() {
        MyButton buttonLeft = buttonFabrick.getButton(CONTROL_LEFT, this);
        buttonLeft.setX(6f);
        buttonLeft.setY(10f);
        addActor(buttonLeft);

        MyButton buttonRight = buttonFabrick.getButton(CONTROL_RIGHT, this);
        buttonRight.setX(46);
        buttonRight.setY(10f);
        addActor(buttonRight);

        MyButton buttonUp = buttonFabrick.getButton(CONTROL_UP, this);
        buttonUp.setX(92f);
        buttonUp.setY(10f);
        addActor(buttonUp);

        yControllButtonOffset = buttonUp.getY() + buttonUp.getHeight() + 5f;
        System.out.println(yControllButtonOffset);
    }

    private void initBackpackButton() {
        backpackBtn = buttonFabrick.getButton(BACKPACK, this);
        backpackBtn.setX(52);
        backpackBtn.setY(viewPortHeight - backpackBtn.getWidth() - 2f);
        addActor(backpackBtn);
    }

    private void initHandInventory() {
        handInventoryUsable = new HandInventory(assets.getUITexture(Assets.UI_BTN_USE_ITEM), 80,
                viewPortHeight - backpackBtn.getWidth() - 2f, ACTION, this);
        handInventoryHold = new HandInventory(assets.getUITexture(Assets.UI_BTN_USE_ITEM), 108,
                viewPortHeight - backpackBtn.getWidth() - 2f, HOLD, this);
        addActor(handInventoryUsable);
        addActor(handInventoryHold);

//        handInventoryUsable.addListener(new ClickListener() {
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                if (!handInventoryUsable.isReadyToAction()) {
//                    if (handInventoryUsable.getItem() != null) {
//                        parentScreen.getGamePlayStage().initActionWithItem(handInventoryUsable.getItem());
//                        handInventoryUsable.setReadyToAction(true);
//                    }
//                } else {
//                    handInventoryUsable.setReadyToAction(false);
//                }
//                super.clicked(event, x, y);
//            }
//        });
    }

    PlayerLifesListener getLifesListener() {
        return lifeBar;
    }

    void showResetDialog() {
        openDialog(resetLevelDialog);
    }

    void openDoorOneDialog() {
        openDialog(doorOneDialog);
    }

    void openDoorTwoDialog() {
        openDialog(doorTwoDialog);
    }

    void openMerchantDialog(){openDialog(merchantDialog);}

    void openSpaceShipDialog(SpaceShipInventory spaceShipInventory, PlayerPouch playerPouch) {

        parentScreen.getGamePlayStage().changeState(GameState.PAUSE);
        spaceShipDialog.setSpaceShipInventory(spaceShipInventory);
        spaceShipDialog.setPouch(playerPouch);
        spaceShipDialog.setVisible(true);
        backpackBtn.setVisible(false);
        handInventoryHold.setVisible(false);
        handInventoryUsable.setVisible(false);

    }


    private void openPouchDialog() {
        openDialog(pouchDialog);
    }

    private void openDialog(MyDialog myDialog) {
        parentScreen.getGamePlayStage().changeState(GameState.PAUSE);

        myDialog.setPouch(parentScreen.getGamePlayStage().getPlayerPouch());
        parentScreen.changeInputToUIOnly();
        backpackBtn.setVisible(false);
        handInventoryHold.setVisible(false);
        handInventoryUsable.setVisible(false);
        myDialog.setVisible(true);

    }

    @Override
    public void onButtonClick(ButtonType buttonType) {
        switch (buttonType) {
            case CONTROL_RIGHT:
                controllable.initMoveRight();
                break;
            case CONTROL_LEFT:
                controllable.initMoveLeft();
                break;
            case CONTROL_UP:
                controllable.initJumping();
                break;
            case BACKPACK:
//                openMerchantDialog();
                openPouchDialog();
//                openDoorOneDialog();
//                openDoorTwoDialog();
                break;
        }
    }

    @Override
    public void onButtonUp(ButtonType buttonType) {
        switch (buttonType) {
            case CONTROL_RIGHT:
                controllable.stopMovingRight();
                break;
            case CONTROL_LEFT:
                controllable.stopMovingLeft();
                break;
            case CONTROL_UP:
                break;
        }
    }

    @Override
    public void dialogClose(MyDialog dialog) {
        if (!(dialog instanceof ResetLevelDialog)) {
            parentScreen.getGamePlayStage().changeState(GameState.PLAYING);
            parentScreen.resetInput();
            backpackBtn.setVisible(true);
            handInventoryHold.setVisible(true);
            handInventoryUsable.setVisible(true);
            dialog.setVisible(false);
        }
    }

    @Override
    public void returnToMainMenu() {

    }

    @Override
    public void doResetGame() {
        resetLevelDialog.setVisible(false);
        parentScreen.reset();
//        parentScreen.changeLevel(Levels.LEVEL_FIVE);
    }

    @Override
    public void initChangeLevel(int stoneCount) {
        switch (stoneCount) {
            case 1:
                parentScreen.changeLevel(Levels.LEVEL_TWO);
                break;
            case 2:
                parentScreen.changeLevel(Levels.LEVEL_THREE);
                break;
            case 3:
                parentScreen.changeLevel(Levels.LEVEL_FOUR);
                break;
            case 4:
                parentScreen.changeLevel(Levels.LEVEL_FIVE);
                break;
            case 5:
                parentScreen.changeLevel(Levels.LEVEL_SIX);
                break;
        }
    }

    @Override
    public void initReturnToLevelOne() {
        parentScreen.changeLevel(Levels.LEVEL_ONE);
    }

    @Override
    public void clickItem(Item item) {
        if (item.isTool()) {
            if (item.getToolType().equals(BOOTS)
                    || item.getToolType().equals(LIGHTER)) {

                if (handInventoryHold.getItem() != null) {
                    parentScreen.getGamePlayStage().removeHoldItemAction(handInventoryHold.getItem());
                    if (handInventoryHold.getItem().equals(item)){
                        handInventoryHold.putItem(null);
                    } else {
                        putItemToHoldHand(item);
                    }
                } else {
                    putItemToHoldHand(item);
                }

            } else if (item.getToolType().equals(BIRDAMINS)) {
                parentScreen.getGamePlayStage().initActionWithItem(item);

            } else if (item.getToolType().equals(PIKES)
                    || item.getToolType().equals(BOTTLE)
                    || item.getToolType().equals(BIRDAMET)) {

                if (handInventoryUsable.getItem() == null){
                    handInventoryUsable.putItem(item);
                    parentScreen.getGamePlayStage().initActionWithItem(handInventoryUsable.getItem());
                } else {
                    parentScreen.getGamePlayStage().deactivateActionWithItem(handInventoryUsable.getItem());
                    if (handInventoryUsable.getItem().equals(item)){
                        handInventoryUsable.putItem(null);
                    } else {
                        handInventoryUsable.putItem(item);
                        parentScreen.getGamePlayStage().initActionWithItem(handInventoryUsable.getItem());
                    }
                }
            }
        }
    }

    @Override
    public void itemBeingActivated(Item item) {
        if (handInventoryHold.getItem() != null && handInventoryHold.getItem().equals(item)){
            parentScreen.getGamePlayStage().initHoldItemAction(item);
        }
        if (handInventoryUsable.getItem() != null && handInventoryUsable.getItem().equals(item)){
            parentScreen.getGamePlayStage().initActionWithItem(item);
        }
    }

    private void putItemToHoldHand(Item item) {
        handInventoryHold.putItem(item);
        parentScreen.getGamePlayStage().initHoldItemAction(item);
    }

    @Override
    public void removeItem(Item item) {
        if (handInventoryHold.getItem()!=null && handInventoryHold.getItem().equals(item)) {
            handInventoryHold.putItem(null);
            parentScreen.getGamePlayStage().removeHoldItemAction(item);
        } else if (handInventoryUsable.getItem()!=null && handInventoryUsable.getItem().equals(item)) {
            handInventoryUsable.putItem(null);
            parentScreen.getGamePlayStage().removeHoldItemAction(item);
        }
    }

    @Override
    public void longPress(Item item) {
        parentScreen.getGamePlayStage().changeState(GameState.PAUSE);
        parentScreen.changeInputToUIOnly();
        itemInfo.setVisible(true);
    }



    @Override
    public boolean keyDown(int keyCode) {
        switch (keyCode) {
            case Input.Keys.BACK:
                itemInfo.setVisible(false);
                return closeOpenDialogs();
        }
        return false;
    }

    private boolean closeOpenDialogs() {
        int closeCount = 0;
        closeCount += checkAndCloseDialogForButton(pouchDialog) ? 1 : 0;
        closeCount += checkAndCloseDialogForButton(doorOneDialog) ? 1 : 0;
        closeCount += checkAndCloseDialogForButton(doorTwoDialog) ? 1 : 0;
        closeCount += checkAndCloseDialogForButton(spaceShipDialog) ? 1 : 0;
        return closeCount > 0;
    }

    private boolean checkAndCloseDialogForButton(MyDialog myDialog) {
        if (myDialog.isVisible()) {
            dialogClose(myDialog);
            return true;
        }
        return false;
    }

    @Override
    public void onHandInventoryClick(HandInventoryType inventoryType, HandInventory handInventory) {
//        if (handInventory.getItem() != null && inventoryType.equals(ACTION)) {
//            if (!handInventory.isReadyToAction()) {
//                parentScreen.getGamePlayStage().initActionWithItem(item);
//            }
//        }
    }

    public float getYControllButtonOffset(){
        return yControllButtonOffset;
    }

    public HandInventory getHandInventoryUsable() {
        return handInventoryUsable;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
//        if (handInventoryUsable.getItem() != null && handInventoryUsable.getItem().getStackCount() == 0) {
//            handInventoryUsable.putItem(null);
//        }
    }


}
