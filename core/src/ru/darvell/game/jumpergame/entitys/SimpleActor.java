package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.math.Vector2;
import ru.darvell.game.jumpergame.ui.UIActor;

public abstract class SimpleActor extends UIActor {

    static int lastId = 0;
    protected int id;

    public SimpleActor() {
        this.id = lastId++;
    }

    public Vector2 getCenter() {
        return new Vector2(getX() + getWidth() / 2, getY() + getHeight() / 2);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        SimpleActor objActor = (SimpleActor) obj;
        if (objActor.getId() == this.id) {
            return true;
        }
        return false;
    }

    public Vector2 getPosition(){
        return new Vector2(getX(), getY());
    }

    public int getId() {
        return id;
    }
}
