package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.GoblinUserData;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;

public class Goblin extends GameActor {

    private Vector2 velocity = new Vector2();
    private int moveDirection = -1;
    private boolean moving = false;

    private Animation moveRight;
    private Animation moveLeft;
    private Animation jumpRight;
    private Animation jumpLeft;
    private Animation inAirRight;
    private Animation inAirLeft;
    private Animation landingRight;
    private Animation landingLeft;

    private float freeRunCount;
    private float stateTime;

    private boolean superJumpInit = false;
    private boolean superJumpAnimFinish = true;
    private boolean doLandingAnimation = false;
    private boolean lastLandingState = false;

    public Goblin(Body body, Assets assets) {
        super(body);
        moveRight = assets.getWorldAnimation(Assets.GOBLIN_MOVE_RIGHT, 0.1f);
        moveLeft = assets.getWorldAnimation(Assets.GOBLIN_MOVE_LEFT, 0.1f);
        jumpRight = assets.getWorldAnimation(Assets.GOBLIN_JUMP_RIGHT, 0.1f);
        jumpLeft = assets.getWorldAnimation(Assets.GOBLIN_JUMP_LEFT, 0.1f);
        inAirRight = assets.getWorldAnimation(Assets.GOBLIN_JUMP_IN_AIR_RIGHT, 0.1f);
        inAirLeft = assets.getWorldAnimation(Assets.GOBLIN_JUMP_IN_AIR_LEFT, 0.1f);
        landingRight = assets.getWorldAnimation(Assets.GOBLIN_LANDING_RIGHT, 0.1f);
        landingLeft = assets.getWorldAnimation(Assets.GOBLIN_LANDING_LEFT, 0.1f);
        freeRunCount = getUserData().getFreeRunCountMax();
    }

    @Override
    protected GoblinUserData getUserData() throws NullPointerException {
        return (GoblinUserData) body.getUserData();
    }

    public void initSuperJump() {
        superJumpInit = true;
        superJumpAnimFinish = false;
        stateTime = 0f;
        stopMoving();
    }

    @Override
    public void act(float delta) {
        if (isActive()) {
            super.act(delta);
            try {
                Vector2 tmpVel = body.getLinearVelocity();
                velocity.y = tmpVel.y;
                body.setLinearVelocity(velocity);

                if (freeRunCount < 0) {
                    initSuperJump();
                    freeRunCount = getUserData().getFreeRunCountMax();
                } else {
                    freeRunCount -= delta;
                }

                if (getUserData().isLanded() && moving) {
                    velocity.x = moveDirection * getUserData().getSpeed();
                }

                if (getUserData().isReachedEndOfPlatform() && moving) {
                    changeDirection();
                    initMoving();
                    getUserData().setReachedEndOfPlatform(false);
                }

                if (getUserData().isReachedEndOfPlatform() && !getUserData().isLanded()) {
                    getUserData().setReachedEndOfPlatform(false);
                }

                if (superJumpInit) {
                    if (superJumpAnimFinish) {
                        superJump();
                        superJumpInit = false;
                        stateTime = 0;
                        getUserData().setReachedEndOfPlatform(false);
                    }
                }

                if (lastLandingState != getUserData().isLanded()) {
                    if (getUserData().isLanded()) {
                        doLandingAnimation = true;
                        stateTime = 0;
                    }
                }

                lastLandingState = getUserData().isLanded();

            } catch (NullPointerException e) {

            }
        }
    }



    public void jump() {
        try {
            if (getUserData().isLanded()) {
                velocity.y = getUserData().getJumpUpVelocity();
                velocity.x = getUserData().getJumpSpeed() * moveDirection;
                body.setLinearVelocity(velocity);
                getUserData().setLanded(false);
            }
        } catch (NullPointerException e) {

        }
    }

    private void superJump() {
        try {
            if (getUserData().isLanded()) {
                velocity.y = getUserData().getJumpUpVelocity();
                body.setLinearVelocity(velocity);
                getUserData().setLanded(false);
            }
        } catch (NullPointerException e) {

        }
    }

    public void initMoving() {
        try {
            moving = true;
            velocity.x = moveDirection * getUserData().getSpeed();
        } catch (NullPointerException e) {

        }
    }

    public void stopMoving() {
        try {
            moving = false;
            velocity.x = 0f;
        } catch (NullPointerException e) {

        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        stateTime += Gdx.graphics.getDeltaTime();
        TextureRegion textureToDraw;

        if (superJumpInit) {
            if (moveDirection > 0) {
                textureToDraw = (TextureRegion) jumpRight.getKeyFrame(stateTime, false);
                superJumpAnimFinish = jumpRight.isAnimationFinished(stateTime);
            } else {
                textureToDraw = (TextureRegion) jumpLeft.getKeyFrame(stateTime, false);
                superJumpAnimFinish = jumpLeft.isAnimationFinished(stateTime);
            }
        }else if (doLandingAnimation) {
            if (moveDirection > 0) {
                textureToDraw = (TextureRegion) landingRight.getKeyFrame(stateTime, false);
                doLandingAnimation = !landingRight.isAnimationFinished(stateTime);
                if (!doLandingAnimation){
                    initMoving();
                }
            } else {
                textureToDraw = (TextureRegion) landingLeft.getKeyFrame(stateTime, false);
                doLandingAnimation = !landingLeft.isAnimationFinished(stateTime);
                if (!doLandingAnimation){
                    initMoving();
                }
            }
        } else if (getUserData() != null && !getUserData().isLanded()) {
            if (moveDirection > 0) {
                textureToDraw = (TextureRegion) inAirRight.getKeyFrame(stateTime, false);
            } else {
                textureToDraw = (TextureRegion) inAirLeft.getKeyFrame(stateTime, false);
            }
        } else if (moving) {
            if (moveDirection > 0) {
                textureToDraw = (TextureRegion) moveRight.getKeyFrame(stateTime, true);
            } else {
                textureToDraw = (TextureRegion) moveLeft.getKeyFrame(stateTime, true);
            }
        } else {
            textureToDraw = (TextureRegion) moveLeft.getKeyFrame(stateTime, true);
        }
        batch.draw(textureToDraw, getX(), getY(), getWidth(), getHeight());
    }

    private void changeDirection() {
        moveDirection = moveDirection == 1 ? -1 : 1;
    }

    @Override
    protected void updatePosition() {
        float rightOffset = 0;
        if (moveDirection > 0) {
            rightOffset = .1f;
        }
        setX(transformToScreen(body.getPosition().x - Constants.GOBLIN_DRAWING_WIDTH / 2 + rightOffset));
        setY(transformToScreen(body.getPosition().y - Constants.GOBLIN_HEIGHT / 2));
        setWidth(transformToScreen(Constants.GOBLIN_DRAWING_WIDTH));
        setHeight(transformToScreen(Constants.GOBLIN_DRAWING_HEIGHT));
    }
}
