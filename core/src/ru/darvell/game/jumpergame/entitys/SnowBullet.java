package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.SnowBulletUserData;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;

public class SnowBullet extends GameActor {

    private TextureRegion textureRegion;
    private float lifeTime;
    private int direction;

    public SnowBullet(Assets assets, Body body, int direction) {
        super(body);
        textureRegion = assets.getUITexture(Assets.SNOW_BULLET);
        this.direction = direction;

    }

    @Override
    protected SnowBulletUserData getUserData() throws NullPointerException {
        return body != null ? (SnowBulletUserData) body.getUserData() : null;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        lifeTime += delta;
        if (getUserData() != null) {
            if (!isNeedDelete()) {
                if (!isNeedDelete() && lifeTime > Constants.SNOW_BULLET_LIFE_TIME) {
                    setNeedDelete();
                }
                getBody().setLinearVelocity(new Vector2(getUserData().getSpeed() * direction, 0));
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(textureRegion, getX(), getY());
    }
}
