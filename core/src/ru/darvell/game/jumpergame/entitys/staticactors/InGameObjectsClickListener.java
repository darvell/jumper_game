package ru.darvell.game.jumpergame.entitys.staticactors;

public interface InGameObjectsClickListener {

    void doorClicked(Door.DoorType doorType);
    void spaceShipCliked();
    void shopCliked();
}
