package ru.darvell.game.jumpergame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.darvell.game.jumpergame.JumperGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		//		config.width = 720;
//		config.height = 1280;
//		config.width = 360;
//		config.height = 640;

//        config.width = 768;
//        config.height = 1024;

		//Note 9
//		config.width = 360;
//        config.height = 740;

		// 16:9
		config.width = 360;
		config.height = 640;

		// 4:3
//		config.width = 540;
//      	config.height = 720;
		new LwjglApplication(new JumperGame(), config);
	}
}
