package ru.darvell.game.jumpergame.entitys.utils;

import com.badlogic.gdx.scenes.scene2d.Stage;
import ru.darvell.game.jumpergame.entitys.staticactors.BackgroundLayer;
import ru.darvell.game.jumpergame.utils.Assets;

public class BackgroundMaster {

    private Stage stage;
    private Assets assets;

    private BackgroundLayer firstLayer;
    private BackgroundLayer secondLayer;
    private BackgroundLayer thirdLayer;

    public BackgroundMaster(Stage stage, Assets assets) {
        this.stage = stage;
        this.assets = assets;

        firstLayer = new BackgroundLayer(0,0, assets.getBackground(Assets.FIRST_LAYER_BACKGROUND));
        secondLayer = new BackgroundLayer(0,0, assets.getBackground(Assets.SECOND_LAYER_BACKGROUND));
        thirdLayer = new BackgroundLayer(0,0, assets.getBackground(Assets.THIRD_LAYER_BACKGROUND));

        stage.addActor(firstLayer);
        stage.addActor(secondLayer);
        stage.addActor(thirdLayer);
    }

    public void moveBackgroundDown(float mult){
        thirdLayer.setY(thirdLayer.getY() - 0.025f * mult);
        secondLayer.setY(secondLayer.getY() - 0.05f * mult);
    }

    public void moveBackgroundUp(float mult){
        thirdLayer.setY(thirdLayer.getY() + 0.025f * mult);
        secondLayer.setY(secondLayer.getY() + 0.05f * mult);
    }

    public float getLayerOffset(){
        return secondLayer.getY();
    }

    public void toBack(){
        thirdLayer.toBack();
        secondLayer.toBack();
        firstLayer.toBack();

    }
}
