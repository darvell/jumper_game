package ru.darvell.game.jumpergame.entitys.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import ru.darvell.game.jumpergame.box2dworld.utils.WorldUtils;
import ru.darvell.game.jumpergame.utils.Constants;

public class SensorsMaster {

    private Body fallSensor;
    private Body[] sideSensors;

    public SensorsMaster(float cameraPosition, World world, float vieportHeight) {
//        Vector2 fallSensorStartPosition = new Vector2();
//        fallSensorStartPosition.set(Constants.MAX_BOX2D_WORLD_WIDTH / 2, (cameraFooter / Constants.WORLD_TO_SCREEN) - Constants.FALL_SENSOR_HEIGHT / 2);

//        fallSensor = WorldUtils.createStormWave(world, fallSensorStartPosition, true);
        sideSensors = WorldUtils.createSideSensors(world, new Vector2(0, cameraPosition / Constants.WORLD_TO_SCREEN)
                , vieportHeight / Constants.WORLD_TO_SCREEN);
        moveSensors(cameraPosition);
    }


    public void moveSensors(float cameraY){
//        fallSensor.setTransform(fallSensor.getPosition().x, (cameraFooter / Constants.WORLD_TO_SCREEN) - Constants.FALL_SENSOR_HEIGHT / 2, 0);
        sideSensors[0].setTransform(0 - Constants.SIDE_SENSOR_WIDTH / 2, (cameraY / Constants.WORLD_TO_SCREEN), 0);
        sideSensors[1].setTransform(Constants.MAX_BOX2D_WORLD_WIDTH + Constants.SIDE_SENSOR_WIDTH / 2, (cameraY / Constants.WORLD_TO_SCREEN), 0);
        sideSensors[0].setLinearVelocity(new Vector2(100f, 0));
        sideSensors[1].setLinearVelocity(new Vector2(-100f, 0));
    }
}
