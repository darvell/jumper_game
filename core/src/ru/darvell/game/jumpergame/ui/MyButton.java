package ru.darvell.game.jumpergame.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class MyButton extends Actor {

    private TextureRegion textureRegion;
    private TextureRegion texturePressed;

    private boolean pressed = false;

    public MyButton(final ButtonType type, final ButtonClickListener clickListener){
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                pressed = true;
                clickListener.onButtonClick(type);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                pressed = false;
                clickListener.onButtonUp(type);
            }
        });
    }

    public MyButton(TextureRegion textureRegion, final ButtonType type, final ButtonClickListener clickListener) {
        this(type, clickListener);
        this.textureRegion = textureRegion;

        setWidth(textureRegion.getRegionWidth());
        setHeight(textureRegion.getRegionHeight());


    }

    public MyButton(TextureRegion textureNormall, TextureRegion texturePressed, final ButtonType type, final ButtonClickListener clickListener) {
        this(type, clickListener);
        this.textureRegion = textureNormall;
        this.texturePressed = texturePressed;

        setWidth(textureNormall.getRegionWidth());
        setHeight(textureNormall.getRegionHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (pressed && texturePressed != null) {
            batch.draw(texturePressed, getX(), getY());
        } else {
            batch.draw(textureRegion, getX(), getY());
        }
    }
}

