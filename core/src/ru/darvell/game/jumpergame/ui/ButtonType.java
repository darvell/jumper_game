package ru.darvell.game.jumpergame.ui;

public enum ButtonType {

    RESET_DIALOG_NEW_GAME,
    RESET_DIALOG_TO_MENU,
    CONTROL_LEFT,
    CONTROL_RIGHT,
    CONTROL_UP,
    MENU_NEW_GAME,
    MENU_LOAD_GAME,
    MENU_SETTINGS,
    BACKPACK

}
