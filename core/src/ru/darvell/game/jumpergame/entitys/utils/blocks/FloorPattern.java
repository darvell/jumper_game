package ru.darvell.game.jumpergame.entitys.utils.blocks;


import com.badlogic.gdx.utils.Array;

public class FloorPattern {

    private ProtoBlock[] blocks;
    private int id = -1;
    private Array<Integer> notAfterIds;

    public FloorPattern(ProtoBlock... blocks) {
        this.blocks = blocks;
    }

    public FloorPattern(int id, ProtoBlock... blocks) {
        this.blocks = blocks;
        this.id = id;
    }

    public ProtoBlock[] getBlocks() {
        return blocks;
    }

    public void setBlocks(ProtoBlock[] blocks) {
        this.blocks = blocks;
    }

    public boolean notCorrectFloor(int id) {
        if (this.id == id) return true;
        if (id != -1 && notAfterIds != null){
            for (Integer myId : notAfterIds){
                if (myId == id){
                    return true;
                }
            }
        }
        return false;
    }

    public int getId() {
        return id;
    }

    public void addNotAfter(int id){
        if (notAfterIds == null){
            notAfterIds = new Array<Integer>();
        }
        notAfterIds.add(id);
    }
}

