package ru.darvell.game.jumpergame.entitys.staticactors;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.darvell.game.jumpergame.entitys.SimpleActor;
import ru.darvell.game.jumpergame.utils.Assets;

public class Shop extends SimpleActor {

    public Shop(Assets assets, final InGameObjectsClickListener listener) {
        setTextureRegion(assets.getUITexture(Assets.UI_SHOP));
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                listener.shopCliked();
                return true;
            }
        });
    }
}
