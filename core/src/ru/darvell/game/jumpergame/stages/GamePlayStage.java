package ru.darvell.game.jumpergame.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import ru.darvell.game.jumpergame.box2dworld.utils.GameContactListener;
import ru.darvell.game.jumpergame.box2dworld.utils.WorldUtils;
import ru.darvell.game.jumpergame.entitys.*;
import ru.darvell.game.jumpergame.entitys.interfaces.Controllable;
import ru.darvell.game.jumpergame.entitys.interfaces.ShakeListener;
import ru.darvell.game.jumpergame.entitys.staticactors.*;
import ru.darvell.game.jumpergame.entitys.utils.*;
import ru.darvell.game.jumpergame.entitys.utils.blocks.BlockTypes;
import ru.darvell.game.jumpergame.entitys.utils.blocks.WorldBlockMaster;
import ru.darvell.game.jumpergame.screens.ParentScreen;
import ru.darvell.game.jumpergame.ui.ProgressBar;
import ru.darvell.game.jumpergame.utils.*;

import java.util.Locale;

public class GamePlayStage extends Stage implements ShakeListener, InGameObjectsClickListener, DvigClickListener, TouchAreaListener, SaverListener {

    public static final String LOG_TAG = "GamePlayStage";
    private final float TIME_STEP = 1 / 300f;
    WorldBlockMaster blockMaster;
    NpcMaster npcMaster;
    Player player;
    Dvig dvig;
    private GameState gameState = GameState.PAUSE;
    private SpaceShip spaceShip;
    private SpaceShipInventory spaceShipInventory;
    private PlayerPouch playerPouch;
    private StormWave stormWave;
    private boolean stormWaveIsAlive = false;
    private float viewPortWidth;
    private float viewPortHeight;
    private float minCameraPosition;
    private float maxCameraPosition;
    private boolean debug;

    private World world;
    private boolean worldStop = false;

    private Box2DDebugRenderer box2DDebugRenderer;
    private GameContactListener gameContactListener;
    private float accumulator = 0f;
    private float lastCameraPostion;
    private BackgroundMaster backgroundMaster;
    private SensorsMaster sensorsMaster;
    private Assets assets;
    private OrthographicCamera camera;
    private PlayerInputProcessor inputProcessor;
    private ParentScreen parentScreen;
    private BitmapFont font;
    private BitmapFont font2;
    private float frameRate = 0f;
    private boolean waitingUseItem = false;
    private boolean canThrowPike = false;
    private boolean canThrowBottle = false;
    private boolean canThrowTeleport = false;
    private TouchArea touchArea;
    private ProgressBar progressBar;
    private float timeOut = 2f;

    public GamePlayStage(boolean debug, float width, float height, Assets assets, ParentScreen parentScreen) {
        super(new FillViewport(width, height));

        this.parentScreen = parentScreen;
        viewPortWidth = width;
        viewPortHeight = height;
        this.assets = assets;

        this.debug = debug;
        if (debug) {
            box2DDebugRenderer = new Box2DDebugRenderer();
        }


        calculateCameraPositions();

        buildLevelByDefault();

        inputProcessor = new PlayerInputProcessor(player);


        font = new BitmapFont();
        font2 = new BitmapFont();
        Item item = new Item(assets, StoneType.BLUE);
        Item item1 = new Item(assets, StoneType.YELLOW);
//        Item item2 = new Item(assets, StoneType.BLUE);
        Item item3 = new Item(assets, StoneType.YELLOW);
//        Item item = new Item(assets, Tools.BOTTLE, true);
        addActor(npcMaster.spawnItemOnField(blockMaster.getBlockById(3), item));
        addActor(npcMaster.spawnItemOnField(blockMaster.getBlockById(4), item1));
//        addActor(npcMaster.spawnItemOnField(blockMaster.getBlockById(5), item2));
        addActor(npcMaster.spawnItemOnField(blockMaster.getBlockById(6), item3));

//        Pike pike = npcMaster.initPike();
//        addActor(pike);
//        dvig = npcMaster.spawnDvig(blockMaster.getBlockById(2), this, parentScreen.gerCurrentLevel().equals(LEVEL_SIX));
//        addActor(dvig);
//        pike.toFront();

        initProgressBar();
        initTouchArea();
        worldStop = false;
        changeState(GameState.PLAYING);

        needSave();

    }

    private void processItems() {
        ItemOnField itemOnField = npcMaster.findPickedItem();
        if (itemOnField != null) {
            if (playerPouch.addItem(itemOnField.getItem()) != null) {
                npcMaster.setPickedItem(itemOnField, true);
            } else {
                npcMaster.setPickedItem(itemOnField, false);
            }
        }
    }

    private void calculateCameraPositions() {
        GdxLog.d(LOG_TAG, String.format(Locale.US, "Viewport width: %f, height: %f", getViewport().getWorldWidth(), getViewport().getWorldHeight()));
        minCameraPosition = viewPortHeight * 0.35f - Constants.CAMERA_BLOCK_OFFSET;
        GdxLog.d(LOG_TAG, String.valueOf(minCameraPosition));
        float backSize = parentScreen.gerCurrentLevel() == Levels.LEVEL_ONE ? 1024 * 3 : 14975;

        float offsetBackground = (backSize - minCameraPosition - viewPortHeight / 2) / 0.5f * 0.05f;
        GdxLog.d(LOG_TAG, String.valueOf(offsetBackground));
        maxCameraPosition = backSize - offsetBackground - viewPortHeight / 2;
        GdxLog.d(LOG_TAG, String.valueOf(maxCameraPosition));
    }

    private void buildLevelByDefault() {

        initCamera();
        getViewport().setCamera(camera);
        initWorld();

        initGameMasters();

        initDoors();
        initPlayer();


        addLevelDetails();

        lastCameraPostion = camera.position.y;

        blockMaster.allBlockToFront();
        stormWave.toFront();
        backgroundMaster.toBack();


    }

    private void addLevelDetails() {
        LevelDecorator levelDecorator;

        switch (parentScreen.gerCurrentLevel()) {
            case LEVEL_ONE:
                levelDecorator = new LevelOneDecorator();
                break;
            case LEVEL_TWO:
                levelDecorator = new LevelTwoDecorator();
                break;
            case LEVEL_SIX:
                levelDecorator = new LevelSixDecorator();
                break;
            default:
                levelDecorator = new DefaultLevelDecorator();
        }
        levelDecorator.doWork(this);
    }

    private void initPlayer() {
        float startYOffset = 0f;
//        player = new Player(WorldUtils.createPlayerBody(world, new Vector2(2f, startYOffset + 2f)), assets);
        player = new Player(WorldUtils.createPlayerBody(world, blockMaster.getFirstBlock().getPositionForGeneration()), assets);
        addActor(player);
        initPlayerPouch();
    }

    private void initWorld() {
        world = WorldUtils.createWorld();
        gameContactListener = new GameContactListener();
        world.setContactListener(gameContactListener);
    }

    private void initGameMasters() {
        npcMaster = new NpcMaster(world, assets, this);
        backgroundMaster = new BackgroundMaster(this, assets);
        sensorsMaster = new SensorsMaster(camera.position.y, world, viewPortHeight);
        blockMaster = new WorldBlockMaster(world, assets, this, parentScreen.gerCurrentLevel(), this, maxCameraPosition);
        initVawe();
    }

    private void initVawe() {
        Vector2 fallSensorStartPosition = new Vector2();
        fallSensorStartPosition.set(Constants.MAX_BOX2D_WORLD_WIDTH / 2, (camera.position.y / Constants.WORLD_TO_SCREEN) - Constants.FALL_SENSOR_HEIGHT / 2 - 25f);
//        fallSensorStartPosition.set(Constants.MAX_BOX2D_WORLD_WIDTH / 2, camera.position.y / Constants.WORLD_TO_SCREEN);

        Body body = WorldUtils.createStormWave(world, fallSensorStartPosition, false);

        stormWave = new StormWave(body,
                assets, viewPortHeight, viewPortWidth);
        addActor(stormWave);
    }

    private void initDoors() {
        if (parentScreen.gerCurrentLevel() != Levels.LEVEL_ONE) {
            Block firstBlock = blockMaster.getFirstBlock();
            Door door = DoorFabrick.getDoor(Door.DoorType.DOOR_START, 50f, firstBlock.getUp(), this, assets);
            addActor(door);
            Block lastBlock = blockMaster.getLastBlock();
            Door doorLast = DoorFabrick.getDoor(Door.DoorType.DOOR_START, lastBlock.getX() + lastBlock.getWidth() / 2, lastBlock.getUp(), this, assets);
            addActor(doorLast);
//            door.toBack();
        }

        if (parentScreen.gerCurrentLevel() == Levels.LEVEL_ONE) {
            Block lastBlock = blockMaster.getLastBlock();
            Door doorLast = DoorFabrick.getDoor(Door.DoorType.DOOR_END, lastBlock.getX() + lastBlock.getWidth() / 2, lastBlock.getUp(), this, assets);
//        Door doorLast = DoorFabrick.getDoor(Door.DoorType.DOOR_END, lastBlock.getX() + lastBlock.getWidth() * .8f, lastBlock.getUp(), this, assets);
            addActor(doorLast);
        }
    }

    void initSpaceShip() {
        Block firstBlock = blockMaster.getFirstBlock();
        spaceShip = new SpaceShip(assets, firstBlock.getWidth() / 2, firstBlock.getUp(), this);
        addActor(spaceShip);
        spaceShip.toBack();

//        spaceShipInventory = new SpaceShipInventory();

        SpaceShipInventory spaceShipTmp = (SpaceShipInventory) parentScreen.getSaver().loadData().get(Saver.SPACE_SHIP);
        if (spaceShipTmp != null) {
            spaceShipInventory = new SpaceShipInventory(spaceShipTmp, assets);
            spaceShipInventory.setSaverListener(this);
        } else {
            spaceShipInventory = new SpaceShipInventory(this);
            spaceShipInventory.initTestState(assets);
            needSave();
        }
    }

    void initShop() {
        int i = 3;
        while (blockMaster.getBlockById(i).getProtoBlock().getType().equals(BlockTypes.BLOCK_ONE)
                || blockMaster.getBlockById(i).getProtoBlock().getType().equals(BlockTypes.BLOCK_TWO)
        ) {
            i++;
        }
        Block block = blockMaster.getBlockById(i);
        Shop shop = new Shop(assets, this);
        shop.setX(block.getX() + block.getWidth() / 2 - shop.getWidth() / 2);
        shop.setY(block.getY() + block.getHeight() / 2);
        addActor(shop);
        shop.toBack();
    }

    private void initPlayerPouch() {

        PlayerPouch pouchTmp = (PlayerPouch) parentScreen.getSaver().loadData().get(Saver.POUCH);
        if (pouchTmp != null) {
            playerPouch = new PlayerPouch(pouchTmp, assets);
            playerPouch.setSaverListener(this);
        } else {
            playerPouch = new PlayerPouch(this);
        }
//        Item item = new Item(assets, StoneType.BLUE);
//        item.setCount(20);
//
//        Item item1 = new Item(assets, StoneType.YELLOW);
//        item1.setCount(20);
//
//        Item item2 = new Item(assets, Tools.BOOTS, false);
//
//
//        Item item1 = new Item(assets, Tools.BIRDAMINS, false);
//        Item item3 = new Item(assets, Tools.PIKES, false);
//        item2.setCount(0);
//        Item item3 = new Item(assets, Tools.BOTTLE, false);
//        item3.setCount(0);
//        Item item4 = new Item(assets, Tools.BIRDAMET, false);
//        playerPouch.addItem(item);
//        playerPouch.addItem(item1);
//        playerPouch.addItem(item2);
//        playerPouch.addItem(item3);
//        playerPouch.addItem(item4);
    }

    public PlayerPouch getPlayerPouch() {
        return playerPouch;
    }

    public void initTouchArea() {
        float offset = Constants.TOUCH_AREA_Y_OFFSET;
        touchArea = new TouchArea(viewPortWidth, viewPortHeight - offset
                , offset, this);
        touchArea.setX(0);
        moveTouchAreaByCamera();
        addActor(touchArea);
//        touchArea.setVisible(true);
//        touchArea.setDebug(true);
    }

    private void initProgressBar() {
        progressBar = new ProgressBar(assets);
        progressBar.setVisible(false);
        addActor(progressBar);
    }

    private void initCamera() {
        camera = new OrthographicCamera(viewPortWidth, viewPortHeight);
        camera.position.set(viewPortWidth / 2, minCameraPosition, 0f);
        if (debug) {
            camera.position.set(5f, 0, 0f);
            camera.zoom = 0.1f;
        }
        camera.update();
//        camera.zoom = 10f;
    }

    @Override
    public void draw() {
        super.draw();
        if (box2DDebugRenderer != null) {
            box2DDebugRenderer.render(world, camera.combined);
        }
        getBatch().begin();
        font.draw(getBatch(), (int) frameRate + " fps", 50, camera.position.y + viewPortHeight / 2 - 5);
        getBatch().end();

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (timeOut >= 0) {
            timeOut -= delta;
        }
        if (gameState == GameState.PLAYING && timeOut < 0) {
            npcMaster.reactivateNpc(camera.position.y + viewPortHeight / 2, camera.position.y - viewPortHeight / 2);
            blockMaster.reactivateBlocks(camera.position.y + viewPortHeight / 2, camera.position.y - viewPortHeight / 2);

            accumulator += delta;
            float mult = 1;
            if (!debug) {
                if (camera.position.y >= minCameraPosition && camera.position.y < maxCameraPosition) {
                    float playerPosition = player.getY();
                    float cameraBottom = camera.position.y - viewPortHeight / 2;
                    if (playerPosition > minCameraPosition) {
                        if (cameraBottom + Constants.CAMERA_PLAYER_OFFSET + 5f < playerPosition) {
                            if ((playerPosition - (cameraBottom + viewPortHeight)) > viewPortHeight / 6) {
                                mult = 15;
                            }
                            camera.position.y += 0.5f * mult;
                        } else if (cameraBottom + Constants.CAMERA_PLAYER_OFFSET - 5f > playerPosition) {
                            if (cameraBottom > playerPosition) {
                                mult = 15;
                            }
                            camera.position.y -= 0.5f * mult;
                        }
                    } else {
                        if (camera.position.y > minCameraPosition) {
                            if (cameraBottom > playerPosition) {
                                mult = 15;
                            }
                            camera.position.y -= 0.5f * mult;
                        }
                    }

                } else if (camera.position.y < minCameraPosition) {
                    camera.position.y = minCameraPosition;
                }
                camera.update();
//                sensorsMaster.moveSensors(camera.position.y);
                sensorsMaster.moveSensors(player.getY());
                moveTouchAreaByCamera();
            } else {
                camera.position.y = player.getBody().getPosition().y;
                camera.update();
                sensorsMaster.moveSensors(player.getY());
            }

            if (lastCameraPostion != camera.position.y) {
                if (lastCameraPostion < camera.position.y) {
                    backgroundMaster.moveBackgroundDown(mult);
                } else {
                    backgroundMaster.moveBackgroundUp(mult);
                }
            }

            lastCameraPostion = camera.position.y;

            if (!worldStop) {
                while (accumulator >= delta) {
                    world.step(TIME_STEP, 8, 3);
                    accumulator -= TIME_STEP;
                }
            }

            npcMaster.update();
            touchArea.toFront();

            processItems();
            moveProgressBar();
            checkAndDoTeleport();

            if (!worldStop) {
                npcMaster.doDeleteOperations();
            }

            parentScreen.getUIStage().getLifesListener().updateLifeCount(player.getLifes());

            if (!player.isAlive() && player.bamAnimationIsFinish()) {
                changeState(GameState.PAUSE);
                parentScreen.getUIStage().showResetDialog();
            }

            frameRate = Gdx.graphics.getFramesPerSecond();

            if (player.isDeepFall()) {
                int blockId = player.getLastBlockStanding();
                Block block = blockMaster.getBlockById(blockId);
                if (block instanceof FallenBlock && ((FallenBlock) block).isReturning()) {
                    block = blockMaster.getNotFallenBlock(blockId);
                }
                player.moveToBlock(block);
                player.resetDeepFall();
                stormWave.initFalling();
            }

//            checkWaveActivation();
        }
    }

    @Override
    public boolean keyDown(int keyCode) {
        switch (keyCode) {
            case Input.Keys.BACK:
                parentScreen.getSaver().saveData(playerPouch, spaceShipInventory, parentScreen.gerCurrentLevel());
                Gdx.app.exit();
                return true;
            case Input.Keys.I:
                addActor(npcMaster.throwTeleportBomb(player, 2, 1));
                return true;
            case Input.Keys.O:
                addActor(npcMaster.throwPike(player, new Vector2(2, 2)));
                return true;
            case Input.Keys.U:
//                addActor(npcMaster.spawnSnowBall(new Vector2(dvig.getCenter().x / 10, dvig.getCenter().y / 10)));
                activateTouchArea();
                return true;
            case Input.Keys.Y:
//                npcMaster.spawnSnowBallCrack();
//                activateTouchArea();
                return true;
        }
        return false;
    }

    @Override
    public void dispose() {
        world.dispose();
        super.dispose();
    }

    private void checkAndDoTeleport() {
        if (npcMaster.isNeedTeleport()) {
            player.moveToPosition(npcMaster.getTeleportCoords());
            player.getBody().applyLinearImpulse(new Vector2(0, -.3f), player.getBody().getWorldCenter(), true);
        }
    }

    private void moveTouchAreaByCamera() {
        touchArea.moveByHorizontal(camera.position.y - viewPortHeight / 2);
    }

    private void checkWaveActivation() {
        if (blockMaster.getLastBlock().getId() == player.getBlockStanding()) {
            stormWave.setActive(false);
        }
        if (!stormWaveIsAlive && player.getLastBlockStanding() > 2) {
            stormWaveIsAlive = true;
            stormWave.setActive(true);
        }
    }

    public void changeState(GameState gameState) {
        this.gameState = gameState;
    }

    public PlayerInputProcessor getInputProcessor() {
        return inputProcessor;
    }

    public Controllable getControllable() {
        return player;
    }

    @Override
    public void blockShaked(int blockId) {
        if (player.getBlockStanding() == blockId) {
            player.hit();
        }
    }

    @Override
    public void doorClicked(Door.DoorType doorType) {
        switch (doorType) {
            case DOOR_START:
                parentScreen.getUIStage().openDoorOneDialog();
                break;
            case DOOR_END:
                parentScreen.getUIStage().openDoorTwoDialog();
                break;
        }
    }

    public Vector2 getPlayerPosition() {
        return new Vector2(player.getX(), player.getY());
    }

    public boolean playerHasLight() {
        return player.isHasLight();
    }

    @Override
    public void spaceShipCliked() {
        parentScreen.getUIStage().openSpaceShipDialog(spaceShipInventory, playerPouch);
    }

    @Override
    public void shopCliked() {
        parentScreen.getUIStage().openMerchantDialog();
    }

    public void initActionWithItem(Item item) {
        if (item.isActivated()) {
            switch (item.getToolType()) {
                case BIRDAMINS:
                    player.eatBirdamins();
                    break;
                case BOTTLE:
                    activateTouchArea();
                    canThrowBottle = true;
                    break;
                case PIKES:
                    activateTouchArea();
                    canThrowPike = true;
                    break;
                case BIRDAMET:
                    activateTouchArea();
                    canThrowTeleport = true;
                    break;
            }
        }

    }

    public void deactivateActionWithItem(Item item) {
        switch (item.getToolType()) {
            case BOTTLE:
                touchArea.setVisible(false);
                canThrowBottle = false;
                break;
            case PIKES:
                touchArea.setVisible(false);
                canThrowPike = false;
                break;
            case BIRDAMET:
                canThrowTeleport = false;
                touchArea.setVisible(false);
                break;
        }
    }

    public void initHoldItemAction(Item item) {
        if (item.isActivated()) {
            switch (item.getToolType()) {
                case BOOTS:
                    player.setInBoots(true);
                    break;
                case LIGHTER:
                    player.setHasLight(true);
                    break;
            }
        }
    }

    public void removeHoldItemAction(Item item) {
        switch (item.getToolType()) {
            case BOOTS:
                player.setInBoots(false);
                break;
            case LIGHTER:
                player.setHasLight(false);
                break;
        }
    }

    @Override
    public void dvigClicked(Dvig dvig) {
//        if (canThrowPike) {
//            npcMaster.throwPike(player, dvig);
//        }
//        addActor(npcMaster.throwBottle(player));
    }

    public void activateTouchArea() {
        touchArea.toFront();
        touchArea.setVisible(true);
    }

    @Override
    public void touchDownArea(float x, float y) {
        Item item = parentScreen.getUIStage().getHandInventoryUsable().getItem();
        if (item != null && item.canUse()) {

            if (canThrowBottle) {
                throwBottle(x, y);
            } else if (canThrowTeleport) {
                showProgressBar();
            } else if (canThrowPike) {
                throwPike(x, y);
            }
            item.use();
            if (!item.canUse()) {
                deactivateActionWithItem(item);
            }
        } else {
            touchArea.setVisible(false);
            canThrowPike = false;
            canThrowBottle = false;
            canThrowTeleport = false;
        }
    }

    @Override
    public void touchUpArea(float x, float y) {
        if (progressBar.isVisible()) {
            int mult = progressBar.stop();
            hideProgressBar();
            float globalYPos = y + touchArea.getY();
            int direction = globalYPos < player.getY() ? -1 : 1;
            TeleportBomb teleportBomb = npcMaster.throwTeleportBomb(player, mult, direction);
            if (teleportBomb != null) {
                addActor(teleportBomb);
            }
        }
    }

    private void throwPike(float x, float y) {
        float globalYPos = y + touchArea.getY();
        Vector2 v2TouchOrig = new Vector2(x / Constants.WORLD_TO_SCREEN, globalYPos / Constants.WORLD_TO_SCREEN);
        addActor(npcMaster.throwPike(player, v2TouchOrig));
    }

    private void throwBottle(float x, float y) {
        float globalYPos = y + touchArea.getY();
        float minYThrowPower = 8f;
        Vector2 v2TouchOrig = new Vector2(x / Constants.WORLD_TO_SCREEN, globalYPos / Constants.WORLD_TO_SCREEN);
        Vector2 v2Direction = v2TouchOrig.sub(player.getOriginCenter());
        float distance = v2Direction.len();
        v2Direction.nor();
        float yPower = globalYPos <= player.getY() ? minYThrowPower : Math.abs(v2Direction.y * 17f);
        yPower = yPower < minYThrowPower ? minYThrowPower : yPower;
        addActor(npcMaster.throwBottle(player, new Vector2(v2Direction.x * distance * 0.8f, yPower)));
    }

    private void showProgressBar() {
        progressBar.setVisible(true);
        progressBar.setPosition(player.getX() - 5f, player.getY());
        progressBar.reset();
    }

    private void hideProgressBar() {
        progressBar.setVisible(false);
    }

    private void moveProgressBar() {
        progressBar.setPosition(player.getX() - 5f, player.getY());
    }


    @Override
    public void needSave() {
        parentScreen.getSaver().saveData(playerPouch, spaceShipInventory, parentScreen.gerCurrentLevel());
    }
}
