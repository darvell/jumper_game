package ru.darvell.game.jumpergame.entitys.staticactors;

import ru.darvell.game.jumpergame.utils.Assets;

public class DoorFabrick {

    public static Door getDoor(Door.DoorType doorType, float x, float y, InGameObjectsClickListener inGameObjectsClickListener, Assets assets){
        Door door = null;
        switch (doorType){
            case DOOR_END: door = new Door(doorType, assets.getUITexture(Assets.UI_DOOR_TWO), inGameObjectsClickListener);
            break;
            case DOOR_START: door = new Door(doorType, assets.getUITexture(Assets.UI_DOOR_ONE), inGameObjectsClickListener);
            break;
        }
        door.setX(x - door.getWidth() / 2);
        door.setY(y);
        return door;

    }

}
