package ru.darvell.game.jumpergame.entitys.utils.blocks;

public enum BlockTypes {

    BLOCK_ONE,
    BLOCK_TWO,
    BLOCK_THREE,
    BLOCK_FOUR,
    BLOCK_MAX
}
