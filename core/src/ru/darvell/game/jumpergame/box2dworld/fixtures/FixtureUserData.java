package ru.darvell.game.jumpergame.box2dworld.fixtures;

public class FixtureUserData {

    FixtureDataType fixtureDataType;

    public FixtureUserData(FixtureDataType fixtureDataType) {
        this.fixtureDataType = fixtureDataType;
    }

    public FixtureDataType getFixtureDataType() {
        return fixtureDataType;
    }
}
