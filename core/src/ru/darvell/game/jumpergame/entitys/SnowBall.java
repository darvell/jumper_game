package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.game.jumpergame.box2dworld.SnowBallUserData;
import ru.darvell.game.jumpergame.utils.Assets;

public class SnowBall extends GameActor {

    private Animation walkAnimationRight;
    private Animation walkAnimationLeft;
    private int direction = -1;
    private float stateTime;

    public SnowBall(Body body, Assets assets) {
        super(body);

        walkAnimationRight = assets.getWorldAnimation(Assets.SNOWBALL_MOVE_RIGHT, 0.1f);

        walkAnimationLeft = assets.getWorldAnimation(Assets.SNOWBALL_MOVE_LEFT, 0.1f);

    }

    @Override
    protected SnowBallUserData getUserData() throws NullPointerException {
        return body != null ? (SnowBallUserData) body.getUserData() : null;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        stateTime += delta;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (body != null) {
            TextureRegion textureRegion;
            if (body.getLinearVelocity().x < 0) {
                textureRegion = (TextureRegion) walkAnimationRight.getKeyFrame(stateTime, true);
            } else {
                textureRegion = (TextureRegion) walkAnimationLeft.getKeyFrame(stateTime, true);
            }
            batch.draw(textureRegion, getX(), getY(), getWidth(), getHeight());
        }

    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public void throwBall() {
        if (body != null) {
            body.applyLinearImpulse(new Vector2(getUserData().velocity * direction, 0f), body.getWorldCenter(), true);
        }
    }
}
