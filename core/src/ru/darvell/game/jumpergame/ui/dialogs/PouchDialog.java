package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.ui.InventoryCell;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;

public class PouchDialog extends DialogWithAction {

    private UIActor bucket;

    public PouchDialog(Assets assets, float screenHeight, DialogListener dialogListener) {
        super(assets, screenHeight, dialogListener);

    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible) {
            initBucket();
            setTouchableCells();
            initDragAndDrop();

        }
    }

    private void setTouchableCells(){
        for (final InventoryCell inventoryCell: pouchCells) {
            inventoryCell.addListener(new ClickListener(){
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    if (!inventoryCell.isDragging()) {
                        Item item = inventoryCell.getItem();
                        if (item != null) {
                            myDialogListener.clickItem(item);
                        }
                        super.touchUp(event, x, y, pointer, button);
                    }
                }
            });
        }
    }

    private void initBucket() {
        if (bucket == null) {
            bucket = new UIActor(assets.getUITexture(Assets.UI_DIALOG_BUSKET));
            bucket.setX(dimens.BUCKET_HORIZONTAL_OFFSET);
            bucket.setY(getHeight() / 2f + dimens.BUCKET_VERTICAL_OFFSET);
            addActor(bucket);
        }
    }

    private void initDragAndDrop() {
        for (InventoryCell inventoryCell : pouchCells) {
            dragAndDrop.addSource(new MySource(inventoryCell));
        }
        dragAndDrop.addTarget(new MyTarget(bucket));
    }

    class MyTarget extends DragAndDrop.Target {

        public MyTarget(UIActor actor) {
            super(actor);
        }



        @Override
        public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            return true;
        }

        @Override
        public void reset(DragAndDrop.Source source, DragAndDrop.Payload payload) {
            super.reset(source, payload);
            if (((MySource)source).plusWnenReset) {
                ((Item)source.getActor()).plusStackCount();
            }
        }

        @Override
        public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            System.out.println("drop");
            InventoryCell sourceInventoryCell = ((InventoryCell) source.getActor());
            myDialogListener.removeItem(sourceInventoryCell.getItem());
            playerPouch.removeItem(sourceInventoryCell.getItem());
            sourceInventoryCell.setItem(null);
        }
    }

}
