package ru.darvell.game.jumpergame.screens;

public interface UiScreenInt {
    void changeToMenu();
    void changeToSettings();
}
