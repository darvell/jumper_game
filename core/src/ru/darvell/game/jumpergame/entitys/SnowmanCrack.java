package ru.darvell.game.jumpergame.entitys;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;

public class SnowmanCrack extends UIActor {

    private Animation right;
    private Animation left;

    public SnowmanCrack(Assets assets, int direction){
        if (direction == 1){
            right = assets.getForAllAnimation(Assets.SNOWMAN_FAIL_RIGHT, 0.2f);
        } else {
            left = assets.getForAllAnimation(Assets.SNOWMAN_FAIL_LEFT, 0.2f);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        checkAndDestroy(((right != null)?right:left).isAnimationFinished(stateTime));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (right != null){
            batch.draw((TextureRegion) right.getKeyFrame(stateTime, false), getX() - 5, getY());
        } else {
            batch.draw((TextureRegion) left.getKeyFrame(stateTime, false), getX() - 10, getY());
        }
    }

    private void checkAndDestroy(boolean b){
        if (b) {
            remove();
        }
    }
}
