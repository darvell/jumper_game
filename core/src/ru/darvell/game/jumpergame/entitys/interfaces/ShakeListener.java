package ru.darvell.game.jumpergame.entitys.interfaces;

public interface ShakeListener {

    void blockShaked(int blockId);
}
