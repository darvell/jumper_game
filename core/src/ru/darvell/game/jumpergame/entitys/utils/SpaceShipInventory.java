package ru.darvell.game.jumpergame.entitys.utils;


import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.entitys.Tools;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.SaverListener;

public class SpaceShipInventory implements Json.Serializable{
    private Array<Boolean> collectionItems = new Array<Boolean>();
    private Array<Item> items = new Array<Item>();
    private SaverListener saverListener;

    public SpaceShipInventory() {
        collectionItems.add(Boolean.TRUE);
        collectionItems.add(Boolean.FALSE);
        collectionItems.add(Boolean.TRUE);
        collectionItems.add(Boolean.TRUE);
        collectionItems.add(Boolean.TRUE);
    }

    public SpaceShipInventory(SaverListener saverListener) {
        this();
        this.saverListener = saverListener;

    }

    public SpaceShipInventory(SpaceShipInventory SpaceShipInventory, Assets assets) {
        this();
        for (Item item:SpaceShipInventory.items){
            items.add(new Item(assets, item));
        }
    }

    public void setSaverListener(SaverListener saverListener) {
        this.saverListener = saverListener;
    }

    public Array<Boolean> getCollectionItems() {
        return collectionItems;
    }

    public void initTestState(Assets assets){
        for (Tools tool: Tools.values()){
            items.add(new Item(assets, tool, false));
        }
    }

    public Array<Item> getItems() {
        return items;
    }

    public boolean removeItem(Item item){
        boolean result = items.removeValue(item, false);
        saverListener.needSave();
        return result;
    }

    public void addItem(Item item) {
        items.add(item);
        saverListener.needSave();
    }

    @Override
    public void write(Json json) {
        json.writeObjectStart("SpaceShip");
        json.writeValue("items", items);
        json.writeObjectEnd();
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        for (JsonValue jsonValue : jsonData.get("SpaceShip").get("items")){
            items.add(json.fromJson(Item.class, jsonValue.toString()));
//            System.out.println("add : " + items.get(items.size-1));
        }
    }
}
