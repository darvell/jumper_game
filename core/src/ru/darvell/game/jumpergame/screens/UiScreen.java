package ru.darvell.game.jumpergame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import ru.darvell.game.jumpergame.JumperGame;
import ru.darvell.game.jumpergame.stages.GamePlayStage;
import ru.darvell.game.jumpergame.stages.MainMenuStage;
import ru.darvell.game.jumpergame.stages.NonGameStage;
import ru.darvell.game.jumpergame.stages.UIStage;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;
import ru.darvell.game.jumpergame.utils.Levels;
import ru.darvell.game.jumpergame.utils.Saver;

public class UiScreen implements Screen, ParentScreen {

    private Assets assets;
    private JumperGame jumperGame;
    private NonGameStage menuStage;
    private NonGameStage currStage;
    private Saver saver;

    private float calculatedViewPortHeight;

    public UiScreen(Assets assets, JumperGame jumperGame, Saver saver) {
        this.assets = assets;
        this.jumperGame = jumperGame;
        this.saver = saver;
        calculatedViewPortHeight = calculateViewportHeight(Constants.VIEWPORT_RELEASE_WIDTH);
        menuStage = new MainMenuStage(Constants.VIEWPORT_RELEASE_WIDTH, calculatedViewPortHeight, assets, this);
        currStage = menuStage;
        Gdx.input.setInputProcessor(menuStage);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        currStage.act(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        currStage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public UIStage getUIStage() {
        return null;
    }

    @Override
    public GamePlayStage getGamePlayStage() {
        return null;
    }

    @Override
    public void reset() {

    }

    @Override
    public Levels gerCurrentLevel() {
        return null;
    }

    @Override
    public void changeLevel(Levels level) {
        jumperGame.changeLevel(level);
    }

    @Override
    public void changeInputToUIOnly() {

    }

    @Override
    public void resetInput() {

    }

    @Override
    public Saver getSaver() {
        return saver;
    }

    float calculateViewportHeight(float width){
        int displayWidth = Gdx.graphics.getWidth();
        int displayHeight = Gdx.graphics.getHeight();
        return (displayHeight * width) / displayWidth;
    }
}
