package ru.darvell.game.jumpergame.entitys.staticactors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class Door extends Actor {

    private TextureRegion textureRegion;

    public Door(final DoorType doorType, TextureRegion textureRegion, final InGameObjectsClickListener inGameObjectsClickListener){
        this.textureRegion = textureRegion;
        setWidth(textureRegion.getRegionWidth());
        setHeight(textureRegion.getRegionHeight());

        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                inGameObjectsClickListener.doorClicked(doorType);
                return true;
            }
        });
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(textureRegion, getX(), getY());
    }

    public enum DoorType {
        DOOR_START,
        DOOR_END
    }
}
