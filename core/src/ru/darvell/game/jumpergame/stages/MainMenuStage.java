package ru.darvell.game.jumpergame.stages;

import ru.darvell.game.jumpergame.screens.ParentScreen;
import ru.darvell.game.jumpergame.ui.*;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Levels;
import ru.darvell.game.jumpergame.utils.Saver;

import static ru.darvell.game.jumpergame.ui.ButtonType.*;

public class MainMenuStage extends NonGameStage implements ButtonClickListener {

    private UIActor title;
    private MyButton btnStartNewGame;
    private MyButton btnLoadGame;
    private MyButton btnSettings;

    public MainMenuStage(float width, float height, Assets assets, ParentScreen parentScreen) {
        super(width, height, assets, parentScreen);
        initTitle();
        initButtons();
    }

    @Override
    void setBackgroundTexture(UIActor background) {
        background.setTextureRegion(assets.getUITexture(Assets.UI_BACKGROUND_LIGHT));
    }

    private void initTitle() {
        title = new UIActor(assets.getUITexture(Assets.UI_TITLE));
        title.setX(7f);
        title.setY(viewPortHeight / 2);
        addActor(title);

    }

    private void initButtons() {
        ButtonFabrick buttonFabrick = new ButtonFabrick(assets);
        btnStartNewGame = buttonFabrick.getButton(MENU_NEW_GAME, this);
        btnStartNewGame.setX(7);
        btnStartNewGame.setY(viewPortHeight / 2 - 46);
        addActor(btnStartNewGame);

        btnLoadGame = buttonFabrick.getButton(MENU_LOAD_GAME, this);
        btnLoadGame.setX(7);
        btnLoadGame.setY(btnStartNewGame.getY() - 17);
        addActor(btnLoadGame);

        btnSettings = buttonFabrick.getButton(MENU_SETTINGS, this);
        btnSettings.setX(7);
        btnSettings.setY(btnLoadGame.getY() - 17);
        addActor(btnSettings);

    }

    @Override
    public void act() {
        super.act();

    }

    @Override
    public void onButtonClick(ButtonType buttonType) {
//        parentScreen.changeLevel(Levels.LEVEL_TWO);
    }

    @Override
    public void onButtonUp(ButtonType buttonType) {
        switch (buttonType) {
            case MENU_NEW_GAME:
                parentScreen.getSaver().resetData();
                parentScreen.changeLevel(Levels.LEVEL_ONE);
                break;
            case MENU_LOAD_GAME:
                Levels level = (Levels) parentScreen.getSaver().loadData().get(Saver.LEVEL);
                parentScreen.changeLevel(level);
                break;
        }

    }
}
