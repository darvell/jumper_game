package ru.darvell.game.jumpergame.box2dworld;

public enum UserDataType {
    BLOCK,
    BLOCK_SOUL,
    BLOCK_MOVE,
    PLAYER,
    GOBLIN,
    STORM_WAVE,
    SIDE_SENSOR,
    FALL_SENSOR,
    ITEM,
    FLYING_BOTTLE,
    TELEPORT_BOMB,
    PIKE,
    SNOWBALL, SNOWMAN,
    SNOWBULLET
}
