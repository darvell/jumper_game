package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.ui.InventoryCell;
import ru.darvell.game.jumpergame.ui.InventoryCellType;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.GdxLog;
import ru.darvell.game.jumpergame.utils.dimens.BigUIDoorDialog;
import ru.darvell.game.jumpergame.utils.dimens.SmallUIPouchDialog;

import static ru.darvell.game.jumpergame.ui.InventoryCellType.WHOLE;

public class DoorOneDialog extends DialogWithAction {

    public static final String LOG_TAG = "DoorOneDialog";

    protected UIActor door;
    protected Array<InventoryCell> doorWholes;

    public DoorOneDialog(float screenHeight, Assets assets, final DialogListener listener) {
        super(assets, screenHeight, listener);
    }

    @Override
    protected void initDimens() {
        if (screenHeight < 240f) {
            dimens = new SmallUIPouchDialog();
        } else {
            dimens = new BigUIDoorDialog();
        }
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (isVisible()) {
            initDoor();
            initDoorWholes();
            initDragAndDrop();
        } else {
            dragAndDrop.clear();
            if (playerPouch != null) {
                for (Item item : playerPouch.getItems()) {
                    item.remove();
                }

                for (InventoryCell doorWhole : doorWholes) {
                    if (doorWhole.getItem() != null) {
                        GdxLog.d(LOG_TAG, doorWhole.getItem().toString());
                        playerPouch.addItem(doorWhole.getItem());
                        doorWhole.setItem(null);
                    }
                    doorWhole.remove();
                }
            }
        }
    }

    protected void initDoor() {
        if (door == null) {
            door = new UIActor(assets.getUITexture(Assets.UI_DIALOG_DOOR_ONE));
            door.setX(dimens.DOOR_HORIZONTAL_OFFSET);
            door.setY(screenHeight / 2f + dimens.DOOR_VERTICAL_OFFSET);
            addActor(door);
            door.addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    doDoorAction();
                    return true;
                }
            });
        }
    }

    protected void initDoorWholes() {
        doorWholes = new Array<InventoryCell>();
        genWhole(77f, 69f);

    }

    protected InventoryCell genWhole(float x, float y) {
        InventoryCell inventoryCell = new InventoryCell(assets.getUITexture(Assets.UI_EMPTY_CELL),
                x, y,
                dimens.DIALOG_CELL_DIMENS,
                InventoryCellType.WHOLE,
                assets.getFont(Assets.FONT_ZX_5_WHITE), this);
        doorWholes.add(inventoryCell);
        addActor(inventoryCell);
        return inventoryCell;
    }

    private void initDragAndDrop() {
        for (InventoryCell inventoryCell : pouchCells) {
            dragAndDrop.addSource(new MySource(inventoryCell));
            dragAndDrop.addTarget(new MyTarget(inventoryCell));
        }

        for (InventoryCell inventoryCell : doorWholes) {
            dragAndDrop.addTarget(new MyTarget(inventoryCell));
            dragAndDrop.addSource(new MySource(inventoryCell));
        }
    }

    private void doDoorAction() {
        int stoneCount = 0;
        for (InventoryCell whole : doorWholes) {
            if (whole.getItem() != null) {
                stoneCount++;
            }
        }
        if (stoneCount > 0) {
            myDialogListener.initReturnToLevelOne();
        }
    }

    class MyTarget extends DragAndDrop.Target {

        public MyTarget(InventoryCell inventoryCell) {
            super(inventoryCell);
        }

        @Override
        public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            return true;
        }

        @Override
        public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            InventoryCell sourceInventoryCell = ((InventoryCell) source.getActor());
            InventoryCell targetInventoryCell = ((InventoryCell) getActor());
            if (!((Item) payload.getDragActor()).isTool()) {

                    if (targetInventoryCell.getCellType() == WHOLE) {
                        if (targetInventoryCell.isEmpty()) {
                            if (sourceInventoryCell.getItem().getStackCount() > 1) {
                                Item newItem = Item.newOne(sourceInventoryCell.getItem());
                                sourceInventoryCell.getItem().minusStackCount();
                                targetInventoryCell.setItem(newItem);
                            } else {
                                playerPouch.removeItem((Item) payload.getDragActor());
                                sourceInventoryCell.setItem(null);
                                targetInventoryCell.setItem((Item) payload.getDragActor());
                            }
                        }
                    } else {
                        if (targetInventoryCell.getItem() == null) {
                            if (playerPouch.addItem((Item) payload.getDragActor()) != null){
                                sourceInventoryCell.setItem(null);
                                fillCells(playerPouch.getItems(), pouchCells, dimens.DIALOG_CELL_DIMENS);
                            }
                        } else {
                            if (targetInventoryCell.getItem().isCapableToStack((Item) payload.getDragActor())) {
                                sourceInventoryCell.setItem(null);
                                playerPouch.addItem((Item) payload.getDragActor());
                            }
                        }
                    }
            }
        }

        @Override
        public void reset(DragAndDrop.Source source, DragAndDrop.Payload payload) {
            super.reset(source, payload);
        }
    }
}
