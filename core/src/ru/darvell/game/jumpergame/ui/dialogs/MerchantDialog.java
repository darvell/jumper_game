package ru.darvell.game.jumpergame.ui.dialogs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.entitys.Item;
import ru.darvell.game.jumpergame.entitys.StoneType;
import ru.darvell.game.jumpergame.entitys.Tools;
import ru.darvell.game.jumpergame.ui.InventoryCell;
import ru.darvell.game.jumpergame.ui.StoneBar;
import ru.darvell.game.jumpergame.ui.UIActor;
import ru.darvell.game.jumpergame.utils.Assets;
import ru.darvell.game.jumpergame.utils.Constants;
import ru.darvell.game.jumpergame.utils.dimens.BigUIMerchDialog;


interface MerchButtonListener {
    void touchButton(MerchButton merchButton);

}

public class MerchantDialog extends DialogWithAction implements MerchButtonListener {

    private float centerX;

    private StoneBar redStoneBar;
    private StoneBar blueStoneBar;
    private UIActor merchAnimation;
    private TextureRegion merchAnimationBorder;
    private Array<MerchButton> merchButtons = new Array<MerchButton>();
    private Tools activeTool;
    private UIActor itemBorder;
    private Label label;

    private float showLabelEstTime = -1;
    private float showLabelMaxTime = 2f;

    public MerchantDialog(Assets assets, float screenHeight, DialogListener dialogListener) {
        super(assets, screenHeight, dialogListener);
        centerY = screenHeight / 2;
        centerX = Constants.VIEWPORT_RELEASE_WIDTH / 2;
        merchAnimationBorder = assets.getUITexture(Assets.MERCH_ANIM_BORDER);
        itemBorder = new UIActor(assets.getUITexture(Assets.UI_MERCH_ITEM_BORDER));
        itemBorder.setVisible(false);
        addActor(itemBorder);
        initMerchAnimation();
        initStoneBars();
        initLabel();

    }

    private void initLabel(){
        Skin skin = new Skin(Gdx.files.internal("data/style.json"));
        label = new Label("Активировано", skin);

        label.setFontScale(0.3f, 0.3f);
        label.setHeight(label.getPrefHeight());
        addActor(label);
        label.setVisible(false);


    }

    private void initMerchAnimation() {
        merchAnimation = new UIActor(assets.getForAllAnimation(Assets.MERCH_ANIMATION, 0.2f));
        merchAnimation.setX(centerX - 36);
        merchAnimation.setY(centerY + 41);
        addActor(merchAnimation);
    }

    private void initStoneBars() {
        redStoneBar = new StoneBar(assets, StoneBar.StoneBarType.RED);
        redStoneBar.setX(centerX - 57);
        redStoneBar.setY(centerY + 40);
        addActor(redStoneBar);

        blueStoneBar = new StoneBar(assets, StoneBar.StoneBarType.BLUE);
        blueStoneBar.setX(centerX + 47);
        blueStoneBar.setY(centerY + 40);
        addActor(blueStoneBar);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        merchAnimation.act(delta);
        if (label.isVisible()){
            showLabelEstTime -= delta;
            if (showLabelEstTime <= 0){
                label.setVisible(false);
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(merchAnimationBorder, centerX - 35, centerY + 40);
    }

    @Override
    protected void initDimens() {
        dimens = new BigUIMerchDialog();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible) {
            initMerchantCells();
            initDragAndDrop();
            resetButtons();
            itemBorder.toFront();
            itemBorder.setVisible(false);
            activeTool = null;
            refreshButtons();
        }
    }

    protected void initInventoryCells() {
        float startX = dimens.CELL_HORIZONTAL_OFFSET;
        float startY = centerY + dimens.CELL_VERTICAL_OFFSET;
        initInventoryCells(startX, startY);
    }

    protected void initMerchantCells() {
        if (merchButtons.size == 0) {
            float curX = dimens.CELL_MERCH_HORIZONTAL_OFFSET;
            float curY = centerY + dimens.CELL_MERCH_VERTICAL_OFFSET;

            int toolNumber = 0;

            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 4; j++) {
                    MerchButton button = new MerchButton(Tools.values()[toolNumber], assets, this);
                    button.setX(curX);
                    button.setY(curY);
                    addActor(button);
                    merchButtons.add(button);
                    curX += dimens.DIALOG_CELL_SPACE_FOR_MERCH + button.getWidth();
                    toolNumber++;
                }
                curX = dimens.CELL_MERCH_HORIZONTAL_OFFSET;
                curY -= dimens.DIALOG_CELL_SPACE_FOR_MERCH + merchButtons.get(0).getHeight();
            }
        }
    }

    private void initDragAndDrop() {
        for (InventoryCell inventoryCell : pouchCells) {
            dragAndDrop.addSource(new MySource(inventoryCell));
        }
        dragAndDrop.addTarget(new MyTarget(merchAnimation));
    }

    @Override
    public void touchButton(MerchButton merchButton) {
        if (activeTool == null || !activeTool.equals(merchButton.getTool())) {
            itemBorder.setX(merchButton.getX() - 2);
            itemBorder.setY(merchButton.getY() - 2);
            itemBorder.setVisible(true);
            activeTool = merchButton.getTool();
            doAction();
        } else if (activeTool.equals(merchButton.getTool())){
            deactivateButton(merchButton);
        }
    }


    public void deactivateButton(MerchButton merchButton) {
        activeTool = null;
        itemBorder.setVisible(false);
    }

    private void refreshButtons() {
        for (Item item: playerPouch.getItems()){
            if (item.isTool() && item.isActivated()){
                reactivateMerchButton(item.getToolType(), true);
            }
        }
    }

    private void reactivateMerchButton(Tools tool, boolean activate) {
        for (MerchButton merchButton: merchButtons) {
            if (merchButton.getTool().equals(tool)) {
                merchButton.setActive(activate);
            }
        }
    }

    private void resetButtons() {
        for (MerchButton merchButton : merchButtons) {
            merchButton.setActive(false);
        }
    }

    private void doAction() {
        if (activeTool != null) {
            boolean nextTry = true;
            while (nextTry) {
                nextTry = false;
                if ((redStoneBar.getStoneCount() >= Constants.merchPrices.getYellowCount(activeTool))
                        && (blueStoneBar.getStoneCount() >= Constants.merchPrices.getBlueCount(activeTool))) {
                    for (Item item : playerPouch.getItems()) {
                        if (item.isTool() && item.getToolType().equals(activeTool)) {
                            if (!item.canStack() && !item.isActivated()) {
                                item.setActivated(true);
                                processPurchase(activeTool);
                                myDialogListener.itemBeingActivated(item);
                            } else if (item.canStack()) {
                                item.plusStackCount();
                                nextTry = true;
                                processPurchase(activeTool);
                                myDialogListener.itemBeingActivated(item);
                            }
                            showSuccessLabel();
                            playerPouch.save();
                        }
                    }
                    refreshButtons();
                }
            }
        }
    }

    private void processPurchase(Tools tool) {
        redStoneBar.minusStones(Constants.merchPrices.getYellowCount(tool));
        blueStoneBar.minusStones(Constants.merchPrices.getBlueCount(tool));
    }

    private void showSuccessLabel(){
        label.setVisible(true);
        showLabelEstTime = showLabelMaxTime;
        label.setX(merchAnimation.getX()+6f);
        label.setY(merchAnimation.getY()+3f);
    }

    class MyTarget extends DragAndDrop.Target {

        public MyTarget(UIActor actor) {
            super(actor);
        }

        @Override
        public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            return true;
        }

        @Override
        public void reset(DragAndDrop.Source source, DragAndDrop.Payload payload) {
            super.reset(source, payload);
            if (((MySource) source).plusWnenReset) {
                ((Item) source.getActor()).plusStackCount();
            }
        }

        @Override
        public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            InventoryCell inventoryCellSource = (InventoryCell) source.getActor();
            if (getActor() instanceof UIActor) {
                Item item = inventoryCellSource.getItem();
                if (!item.isTool()) {
                    item.minusStackCount();
                    if (item.getStackCount() == 0) {
                        inventoryCellSource.setItem(null);
                        playerPouch.removeItem(item);
                    }
                    if (item.getStoneType().equals(StoneType.BLUE)) {
                        blueStoneBar.addStone();
                        doAction();
                    } else {
                        redStoneBar.addStone();
                        doAction();
                    }
                }
            }
//            InventoryCell sourceInventoryCell = ((InventoryCell) source.getActor());
//            myDialogListener.removeItem(sourceInventoryCell.getItem());
//            playerPouch.removeItem(sourceInventoryCell.getItem());
//            sourceInventoryCell.setItem(null);
        }
    }


}
