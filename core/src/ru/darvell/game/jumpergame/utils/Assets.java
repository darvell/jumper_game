package ru.darvell.game.jumpergame.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import ru.darvell.game.jumpergame.entitys.Tools;
import ru.darvell.game.jumpergame.entitys.utils.blocks.BlockTypes;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static ru.darvell.game.jumpergame.utils.Levels.*;

public class Assets {

    public static final String WORLD_ATLAS = "atlas.atlas";
    public static final String HERO_ATLAS = "data/hero/hero.atlas";
    public static final String UI_ATLAS = "data/ui/ui.atlas";
    public static final String UI_RU_ATLAS = "data/ui/localed/ru_RU/ui.atlas";
    public static final String UI_EN_ATLAS = "data/ui/localed/en_US/ui.atlas";

    public static final String FIRST_LAYER_BACKGROUND = "first_layer_background_%d.png";
    public static final String SECOND_LAYER_BACKGROUND = "second_layer_background_%d.png";
    public static final String THIRD_LAYER_BACKGROUND = "third_layer_background_%d.png";
    public static final String BLOCK_ONE = "block_one";
    public static final String BLOCK_TWO = "block_two";
    public static final String BLOCK_THREE = "block_three";
    public static final String BLOCK_FOUR = "block_four";
    public static final String BLOCK_MAX = "block_max";
    public static final String HERO_STAY_RIGHT = "hero_stay_right";
    public static final String HERO_STAY_LEFT = "hero_stay_left";
    public static final String HERO_RUN_RIGHT = "hero_run_right";
    public static final String HERO_RUN_LEFT = "hero_run_left";
    public static final String HERO_JUMP_RIGHT = "hero_jump_right";
    public static final String HERO_JUMP_LEFT = "hero_jump_left";
    public static final String HERO_JUMP_IN_AIR_RIGHT = "hero_jump_inair_right";
    public static final String HERO_JUMP_IN_AIR_LEFT = "hero_jump_inair_left";
    public static final String HERO_LANDING_RIGHT = "hero_landing_right";
    public static final String HERO_LANDING_LEFT = "hero_landing_left";
    public static final String HERO_BAM_RIGHT = "hero_bam_right";
    public static final String HERO_BAM_LEFT = "hero_bam_left";
    public static final String DVIG = "dvig";

    public static final String STONE_YELLOW = "stone_yellow";
    public static final String STONE_BLUE = "stone_blue";
    public static final String VAWE = "vawe";
    public static final String WAVE_COLOR = "wave_color";


    public static final String GOBLIN_MOVE_RIGHT = "goblin_move_right";
    public static final String GOBLIN_MOVE_LEFT = "goblin_move_left";
    public static final String GOBLIN_JUMP_RIGHT = "goblin_jump_right";
    public static final String GOBLIN_JUMP_LEFT = "goblin_jump_left";
    public static final String GOBLIN_JUMP_IN_AIR_RIGHT = "goblin_inair_right";
    public static final String GOBLIN_JUMP_IN_AIR_LEFT = "goblin_inair_left";
    public static final String GOBLIN_LANDING_RIGHT = "goblin_landing_right";
    public static final String GOBLIN_LANDING_LEFT = "goblin_landing_left";

    public static final String SNOWBALL_MOVE_LEFT = "snowball_move_left";
    public static final String SNOWBALL_MOVE_RIGHT = "snowball_move_right";
    public static final String SNOWBALL_CRACK = "snowball_crack";

    public static final String SNOWMAN_STAY_RIGHT = "snowman_stay_right";
    public static final String SNOWMAN_STAY_LEFT = "snowman_stay_left";
    public static final String SNOWMAN_FIRE_RIGHT = "snowman_fire_right";
    public static final String SNOWMAN_FIRE_LEFT = "snowman_fire_left";
    public static final String SNOWMAN_FAIL_RIGHT = "snowman_fail_right";
    public static final String SNOWMAN_FAIL_LEFT = "snowman_fail_left";

    public static final String SNOW_BULLET = "snow_bullet";

    public static final String UI_LIFEBAR = "lifebar";
    public static final String UI_LIFEBAR_HEART = "lifebar_heart";
    public static final String UI_BIG_PLANK = "big_plank";
    public static final String UI_PAPER_ONE = "paper_one";
    public static final String UI_PAPER_TWO = "paper_two";
    public static final String UI_DOOR_ONE = "door_one";
    public static final String UI_DOOR_TWO = "door_two";
    public static final String UI_UP_ARROW = "up_arrow";
    public static final String UI_LEFT_ARROW = "left_arrow";
    public static final String UI_RIGT_ARROW = "right_arrow";
    public static final String UI_DIALOG_DOOR_ONE = "dialog_door_one";
    public static final String UI_DIALOG_DOOR_TWO = "dialog_door_two";
    public static final String UI_FIRST_SCREEN_BACKGROUND = "first_screen_background";
    public static final String UI_BACKGROUND_LIGHT = "background_light";
    public static final String UI_BACKGROUND_DARK = "background_dark";
    public static final String UI_TITLE = "title";
    public static final String UI_BTN_START_NRML = "btn_start_nrml";
    public static final String UI_BTN_START_PRSSD = "btn_start_prssd";
    public static final String UI_BTN_LOAD_NRML = "btn_load_nrml";
    public static final String UI_BTN_LOAD_PRSSD = "btn_load_prssd";
    public static final String UI_BTN_SETTINGS_NRML = "btn_settings_nrml";
    public static final String UI_BTN_SETTINGS_PRSSD = "btn_settings_prssd";
    public static final String UI_GRAY_BACKGROUND = "gray_back";
    public static final String UI_BTN_BACKPACK = "btn_backpack";
    public static final String UI_BTN_USE_ITEM = "btn_use_item";

    public static final String UI_SHOP = "shop";
    public static final String UI_MERCH_ITEM_BORDER = "merch_item_border";

    public static final String SPACESHIP = "spaceship";
    public static final String UI_TEXT_PANEL_LONG = "dialog_text_panel_long";
    public static final String UI_EMPTY_CELL = "dialog_empty_cell";
    public static final String UI_COL_ITEM1 = "col_item1";
    public static final String UI_COL_ITEM2 = "col_item2";
    public static final String UI_COL_ITEM3 = "col_item3";
    public static final String UI_COL_ITEM4 = "col_item4";
    public static final String UI_COL_ITEM5 = "col_item5";


    public static final String UI_ITEM_GUN_E = "item_gun_e";
    public static final String UI_ITEM_GUN_F = "item_gun_f";
    public static final String UI_ITEM_FLASK_E = "item_flask_e";
    public static final String UI_ITEM_FLASK_F = "item_flask_f";
    public static final String UI_ITEM_BOTTLE_E = "item_bottle_e";
    public static final String UI_ITEM_BOTTLE_F = "item_bottle_f";
    public static final String UI_ITEM_RADIO_E = "item_radio_e";
    public static final String UI_ITEM_RADIO_F = "item_radio_f";
    public static final String UI_ITEM_PIKES_E = "item_pikes_e";
    public static final String UI_ITEM_PIKES_F = "item_pikes_f";
    public static final String UI_ITEM_TUBE_E = "item_tube_e";
    public static final String UI_ITEM_TUBE_F = "item_tube_f";
    public static final String UI_ITEM_BOOTS_E = "item_boots_e";
    public static final String UI_ITEM_BOOTS_F = "item_boots_f";
    public static final String UI_ITEM_TV_E = "item_tv_e";
    public static final String UI_ITEM_TV_F = "item_tv_f";
    public static final String UI_ITEM_STONE_BLUE = "item_stone_blue";
    public static final String UI_ITEM_STONE_YELLOW = "item_stone_yellow";



    public static final String MERCH_BTN_GUN = "merch_btn_gun";
    public static final String MERCH_BTN_FLASK = "merch_btn_flask";
    public static final String MERCH_BTN_BOTTLE = "merch_btn_bottle";
    public static final String MERCH_BTN_RADIO = "merch_btn_radio";
    public static final String MERCH_BTN_PIKES = "merch_btn_pikes";
    public static final String MERCH_BTN_TUBE = "merch_btn_tube";
    public static final String MERCH_BTN_BOOTS = "merch_btn_boots";
    public static final String MERCH_BTN_TV = "merch_btn_tv";

    public static final String UI_DIALOG_BUSKET = "dialog_busket";
    public static final String UI_PIKE = "pike";
    public static final String UI_BOTTLE = "bottle";
    public static final String UI_TELEPORT_BOMB = "teleport_bomb";

    public static final String UI_PROGRESS_BAR = "progress_bar";
    public static final String UI_PROGRESS_BAR_FILL = "progress_bar_fill";


    public static final String UI_EMPTY_STONE_BAR = "empty_stone_bar";
    public static final String UI_STONE_BAR_ITEM_RED_FIRST = "stone_bar_item_red_first";
    public static final String UI_STONE_BAR_ITEM_RED_MIDDLE = "stone_bar_item_red_middle";
    public static final String UI_STONE_BAR_ITEM_RED_LAST = "stone_bar_item_red_last";

    public static final String UI_STONE_BAR_ITEM_BLUE_FIRST = "stone_bar_item_blue_first";
    public static final String UI_STONE_BAR_ITEM_BLUE_MIDDLE = "stone_bar_item_blue_middle";
    public static final String UI_STONE_BAR_ITEM_BLUE_LAST = "stone_bar_item_blue_last";

    public static final String UI_INFO_PANEL_CORNER = "info_panel_corner";
    public static final String UI_INFO_PANEL_BORDER = "info_panel_border";
    public static final String UI_INFO_PANEL_BORDER_LAND = "info_panel_border_land";
    public static final String UI_INFO_PANEL_BODY = "info_panel_body";
    public static final String UI_INFO_PANEL_CLOSE_BTN = "info_panel_close_btn";

    public static final String MERCH_ANIMATION = "merch_anim";
    public static final String MERCH_ANIM_BORDER = "merch_anim_border";


    public static final String FONT_ZX_5_WHITE = "data/fonts/zx_5_white.fnt";

    private AssetManager manager;
    private Map<Levels, String> levelResourcesPaths;
    private Levels currentLevel;

    public Assets() {
        manager = new AssetManager();
        levelResourcesPaths = new HashMap<Levels, String>();
        levelResourcesPaths.put(LEVEL_ONE, "data/level_one/");
        levelResourcesPaths.put(LEVEL_TWO, "data/level_two/");
        levelResourcesPaths.put(LEVEL_THREE, "data/level_three/");
        levelResourcesPaths.put(LEVEL_FOUR, "data/level_four/");
        levelResourcesPaths.put(LEVEL_FIVE, "data/level_five/");
        levelResourcesPaths.put(LEVEL_SIX, "data/level_six/");
        setCurrentLevel(LEVEL_ONE);
    }

    public boolean setCurrentLevel(Levels level) {
        if (levelResourcesPaths.containsKey(level)) {
            this.currentLevel = level;
            return true;
        }
        return false;
    }

    public void load() {

        manager.load(HERO_ATLAS, TextureAtlas.class);
        manager.load(UI_ATLAS, TextureAtlas.class);
        manager.load(UI_RU_ATLAS, TextureAtlas.class);
        manager.load(UI_EN_ATLAS, TextureAtlas.class);
        manager.load(levelResourcesPaths.get(currentLevel) + WORLD_ATLAS, TextureAtlas.class);
        manager.load(FONT_ZX_5_WHITE, BitmapFont.class);


        for (int i = 14; i >= 0; i--) {
            if (Gdx.files.internal(levelResourcesPaths.get(currentLevel) + String.format(Locale.US, FIRST_LAYER_BACKGROUND, i)).exists()) {
                manager.load(levelResourcesPaths.get(currentLevel) + String.format(Locale.US, FIRST_LAYER_BACKGROUND, i), Texture.class);
            }
            if (Gdx.files.internal(levelResourcesPaths.get(currentLevel) + String.format(Locale.US, SECOND_LAYER_BACKGROUND, i)).exists()) {
                manager.load(levelResourcesPaths.get(currentLevel) + String.format(Locale.US, SECOND_LAYER_BACKGROUND, i), Texture.class);
            }
            if (Gdx.files.internal(levelResourcesPaths.get(currentLevel) + String.format(Locale.US, THIRD_LAYER_BACKGROUND, i)).exists()) {
                manager.load(levelResourcesPaths.get(currentLevel) + String.format(Locale.US, THIRD_LAYER_BACKGROUND, i), Texture.class);
            }
        }
    }

    public void finishLoading() {
        manager.finishLoading();
    }

    public void dispose() {
        manager.dispose();
        manager.clear();
    }

    private TextureAtlas getWorldAtlas(String atlasName) {
        return manager.get(levelResourcesPaths.get(currentLevel) + atlasName, TextureAtlas.class);
    }

    private TextureAtlas getHeroAtlas(String atlasName) {
        return manager.get(atlasName, TextureAtlas.class);
    }

    private TextureAtlas getUiAtlas(String atlasName) {
        return manager.get(atlasName, TextureAtlas.class);
    }

    private TextureAtlas getUiLocaledAtlas() {
        if (java.util.Locale.getDefault().toString().equals("ru_RU")) {
            return manager.get(UI_RU_ATLAS, TextureAtlas.class);
        } else {
            return manager.get(UI_EN_ATLAS, TextureAtlas.class);
        }
    }

    public Animation getForAllAnimation(String animationType, float rate) {
        Array<TextureAtlas.AtlasRegion> atlasRegions = getHeroAtlas(HERO_ATLAS).findRegions(animationType);
        Animation animation = new Animation(rate, atlasRegions);
        System.currentTimeMillis()
        return animation;
    }

    public Animation getWorldAnimation(String animationType, float rate) {
        Array<TextureAtlas.AtlasRegion> atlasRegions = getWorldAtlas(WORLD_ATLAS).findRegions(animationType);
        Animation animation = new Animation(rate, atlasRegions);
        return animation;
    }

    public Animation getWorldAnimation(String animationType, float rate, Animation.PlayMode playMode) {
        Array<TextureAtlas.AtlasRegion> atlasRegions = getWorldAtlas(WORLD_ATLAS).findRegions(animationType);
        Animation animation = new Animation(rate, atlasRegions, playMode);
        return animation;
    }

    public TextureRegion getUITexture(String textureName) {
        TextureRegion result = getUiAtlas(UI_ATLAS).findRegion(textureName);

        if (result == null) {
            result = getUiLocaledAtlas().findRegion(textureName);
        }
        return result;
    }

    public TextureRegion getWorldTextureFromAtlas(String name) {
        return getWorldAtlas(WORLD_ATLAS).findRegion(name);
    }

    public Texture getTexure(String name) {
        return manager.get(levelResourcesPaths.get(currentLevel) + name, Texture.class);
    }

    //TODO Поправить потом индексы
    public Texture[] getBackground(String name) {
        Texture[] textures = new Texture[15];
        int j = 0;
        for (int i = 14; i >= 0; i--) {
            if (manager.isLoaded(levelResourcesPaths.get(currentLevel) + String.format(Locale.US, name, i), Texture.class)) {
                textures[j] = manager.get(levelResourcesPaths.get(currentLevel) + String.format(Locale.US, name, i), Texture.class);
                j++;
            }
        }
        return textures;
    }

    public TextureRegion getBlockTexture(BlockTypes blockTypes) {
        Animation animation;
        switch (blockTypes) {
            case BLOCK_ONE:
                animation = getWorldAnimation(BLOCK_ONE, 0.1f);
                break;
            case BLOCK_TWO:
                animation = getWorldAnimation(BLOCK_TWO, 0.1f);
                break;
            case BLOCK_THREE:
                animation = getWorldAnimation(BLOCK_THREE, 0.1f);
                break;
            case BLOCK_FOUR:
                animation = getWorldAnimation(BLOCK_FOUR, 0.1f);
                break;
            case BLOCK_MAX:
                animation = getWorldAnimation(BLOCK_MAX, 0.1f);
                break;
            default:
                animation = null;
        }

        Object[] frames = animation.getKeyFrames();
        int rndIndex = MathUtils.random(0, frames.length - 1);
        return (TextureRegion) frames[rndIndex];
    }

    public int getLoadedAssets() {
        return manager.getLoadedAssets();
    }

    public BitmapFont getFont(String fontName){
        return manager.get(fontName, BitmapFont.class);
    }

    public TextureRegion merchButtonTextureByTool(Tools tool, boolean active) {
        switch (tool){
            case BOTTLE:
                return (active) ? getUITexture(UI_ITEM_BOTTLE_F) : getUITexture(MERCH_BTN_BOTTLE);
            case BIRDAMET:
                return (active) ? getUITexture(UI_ITEM_GUN_F) : getUITexture(MERCH_BTN_GUN);
            case TV:
                return (active) ? getUITexture(UI_ITEM_TV_F) : getUITexture(MERCH_BTN_TV);
            case TELEPORT:
                return (active) ? getUITexture(UI_ITEM_RADIO_F) : getUITexture(MERCH_BTN_RADIO);
            case BIRDAMINS:
                return (active) ? getUITexture(UI_ITEM_FLASK_F) : getUITexture(MERCH_BTN_FLASK);
            case PIKES:
                return (active) ? getUITexture(UI_ITEM_PIKES_F) : getUITexture(MERCH_BTN_PIKES);
            case LIGHTER:
                return (active) ? getUITexture(UI_ITEM_TUBE_F) : getUITexture(MERCH_BTN_TUBE);
            case BOOTS:
                return (active) ? getUITexture(UI_ITEM_BOOTS_F) : getUITexture(MERCH_BTN_BOOTS);
            default: return (active) ? getUITexture(UI_ITEM_BOTTLE_F) : getUITexture(MERCH_BTN_BOTTLE);
        }
    }
}
