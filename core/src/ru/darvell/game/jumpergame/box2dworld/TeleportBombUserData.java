package ru.darvell.game.jumpergame.box2dworld;

import ru.darvell.game.jumpergame.utils.Constants;

public class TeleportBombUserData extends UserData {

    public float velocity;
    public boolean activated = false;

    public TeleportBombUserData() {
        userDataType = UserDataType.TELEPORT_BOMB;
        velocity = Constants.TELEPORT_BOMB_VELOCITY;
        width = Constants.TELEPORT_BOMB_DIMENS;
        height = Constants.TELEPORT_BOMB_DIMENS;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
}
