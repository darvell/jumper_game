package ru.darvell.game.jumpergame.stages;

public interface LevelDecorator {

    void doWork(GamePlayStage gamePlayStage);

}
